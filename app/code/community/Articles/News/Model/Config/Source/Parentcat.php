<?php

class Articles_News_Model_Config_Source_Parentcat {

    public function toOptionArray() {
        return array(
            array('value' => 1, 'label' => Mage::helper('articlesnews')->__('Show list products')),
            array('value' => 2, 'label' => Mage::helper('articlesnews')->__('Show list categories')),
        );
    }

}

?>
