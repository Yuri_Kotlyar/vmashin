<?php

class Articles_News_Model_News extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('articlesnews/news');
    }

    public function getUrl() {
        $urlOption = Mage::helper('articlesnews')->getUrloption();
        $url = "";
        if ($urlOption == 2) {
            $catUrl = Mage::app()->getRequest()->getPathInfo();
            if (substr($catUrl, 0, 1) == '/') {
                $catUrl = substr($catUrl, 1);
            }
            if (substr($catUrl, -1) == '/') {
                $catUrl = substr($catUrl, 0, -1);
            }
            $urlSuffix = '/' . $this->getIdentifier() . Mage::helper('articlesnews')->getNewsitemUrlSuffix();
            $catUrl = str_replace(Mage::helper('articlesnews')->getNewsitemUrlSuffix(), '', $catUrl);
            $url = Mage::getUrl() . $catUrl . $urlSuffix;
        } else {
            $url = Mage::getUrl(Mage::helper('articlesnews')->getRoute()) . $this->getIdentifier() . Mage::helper('articlesnews')->getNewsitemUrlSuffix();
        }
        return $url;
    }

    public function getCommentsCount() {
        $model = Mage::getModel('articlesnews/comment')->getCollection()
                ->addFieldToFilter('news_id', $this->getId())
                ->count();
        return $model;
    }

    public function getUrlPrint() {
        $urlOption = Mage::helper('articlesnews')->getUrloption();
        $url = "";
        if ($urlOption == 2) {
            $catUrl = str_replace(Mage::helper('articlesnews')->getNewsitemUrlSuffix(), '', Mage::registry('current_offi_news_cat_url'));
            $catUrl = str_replace(Mage::getUrl(Mage::helper('articlesnews')->getRoute()), '', $catUrl);
            if ($catUrl) {
                $catUrl = $catUrl . '/';
            }
            $url = Mage::getUrl(Mage::helper('articlesnews')->getRoute() . '/print') . $catUrl . $this->getIdentifier() . Mage::helper('articlesnews')->getNewsitemUrlSuffix();
        } else {
            $url = Mage::getUrl(Mage::helper('articlesnews')->getRoute()) . $this->getIdentifier() . Mage::helper('articlesnews')->getNewsitemUrlSuffix() . '?mode=print';
        }
        return $url;
    }

    /**
     * Reset all model data
     *
     * @return Articles_News_Model_News
     */
    public function reset() {
        $this->setData(array());
        $this->setOrigData();
        $this->_attributes = null;
        return $this;
    }

    public function getTimePublic() {
        if ($this->getPublicateFromDate() == NULL) {
            return Mage::helper('articlesnews')->formatDate($this->getCreatedTime());
        } else {
            return Mage::helper('articlesnews')->formatDate($this->getPublicateFromDate());
        }
    }

}
