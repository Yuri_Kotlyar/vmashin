<?php

class Articles_News_Model_Mysql4_Comment extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('articlesnews/comment', 'comment_id');
    }
}
