<?php

class Articles_News_Block_Tags extends Articles_News_Block_Abstract {

    protected function _prepareLayout() {
        if ($head = $this->getLayout()->getBlock('head')) {
            // show breadcrumbs

            $moduleName = $this->getRequest()->getModuleName();
            $showBreadcrumbs = (int) Mage::getStoreConfig('articlesnews/news/showbreadcrumbs');
            if ($showBreadcrumbs && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))) {
                $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('articlesnews')->__('Home'),
                    'title' => Mage::helper('articlesnews')->__('Go to Home Page'),
                    'link' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)));
                $breadcrumbs->addCrumb('articlesnews', array(
                    'label' => Mage::helper('articlesnews')->__(Mage::getStoreConfig('articlesnews/news/title')),
                    'title' => Mage::helper('articlesnews')->__('Return to %s', Mage::helper('articlesnews')->__('News')),
                    'link' => Mage::getUrl(Mage::helper('articlesnews')->getRoute())
                ));

                $breadcrumbs->addCrumb('tag', array(
                    'label' => Mage::helper('articlesnews')->__("Tag"),
                    'title' => Mage::helper('articlesnews')->__('Tag'),
                ));

                $breadcrumbs->addCrumb('tagitem', array(
                    'label' => Mage::helper('articlesnews')->__("Tag item"),
                    'title' => Mage::helper('articlesnews')->__('Tag item'),
                ));
            }

            if ($moduleName == 'articlesnews') {
                // set default meta data
                $head->setTitle(Mage::getStoreConfig('articlesnews/news/metatitle'));
                $head->setKeywords(Mage::getStoreConfig('articlesnews/news/metakeywords'));
                $head->setDescription(Mage::getStoreConfig('articlesnews/news/metadescription'));
                // set category meta data if defined
                $currentCategory = $this->getCurrentCategory();
                if ($currentCategory != null) {
                    if ($currentCategory->getTitle() != '') {
                        $head->setTitle($currentCategory->getTitle());
                    }
                    if ($currentCategory->getMetaKeywords() != '') {
                        $head->setKeywords($currentCategory->getMetaKeywords());
                    }
                    if ($currentCategory->getMetaDescription() != '') {
                        $head->setDescription($currentCategory->getMetaDescription());
                    }
                }
            }
        }
    }

    public function getCurrentCategory() {
        return false;
    }

    public function getNewsPost() {
        $tag = $this->getRequest()->getParam('tag');
        $tag = trim($tag);
        $tag = str_replace('-', ' ', $tag);
        $collection = Mage::getModel('articlesnews/news')->getCollection()
        ;
        $storeId = Mage::app()->getStore()->getId();
        $collection
                ->addEnableFilter(1)
                ->addStoreFilter($storeId)
                ->addStoreEnableFilter(1);
        $collection->getSelect()->where("main_table.tags LIKE '%" . $tag . "%'");
        return $collection;
    }

}
