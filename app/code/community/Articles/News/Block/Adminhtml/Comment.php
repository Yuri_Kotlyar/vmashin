<?php


class Articles_News_Block_Adminhtml_Comment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_comment';
        $this->_blockGroup = 'articlesnews';
        $this->_headerText = Mage::helper('articlesnews')->__('Comment Manager');
        parent::__construct();
        $this->_removeButton('add');
    }
}
