<?php

class Articles_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'articlesnews';
        $this->_headerText = Mage::helper('articlesnews')->__('News Manager');
        $this->_addButtonLabel = Mage::helper('articlesnews')->__('Add New Article');
        parent::__construct();
    }
}
