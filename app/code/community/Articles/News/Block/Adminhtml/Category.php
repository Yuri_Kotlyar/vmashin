<?php

class Articles_News_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_category';
        $this->_blockGroup = 'articlesnews';
        $this->_headerText = Mage::helper('articlesnews')->__('Category Manager');
        $this->_addButtonLabel = Mage::helper('articlesnews')->__('Add Category');
        parent::__construct();
    }
}
