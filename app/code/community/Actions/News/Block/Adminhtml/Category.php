<?php

class Actions_News_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_category';
        $this->_blockGroup = 'actionsnews';
        $this->_headerText = Mage::helper('actionsnews')->__('Category Manager');
        $this->_addButtonLabel = Mage::helper('actionsnews')->__('Add Category');
        parent::__construct();
    }
}
