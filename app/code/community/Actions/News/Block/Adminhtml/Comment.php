<?php


class Actions_News_Block_Adminhtml_Comment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_comment';
        $this->_blockGroup = 'actionsnews';
        $this->_headerText = Mage::helper('actionsnews')->__('Comment Manager');
        parent::__construct();
        $this->_removeButton('add');
    }
}
