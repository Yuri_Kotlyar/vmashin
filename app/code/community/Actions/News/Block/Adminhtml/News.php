<?php

class Actions_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'actionsnews';
        $this->_headerText = Mage::helper('actionsnews')->__('News Manager');
        $this->_addButtonLabel = Mage::helper('actionsnews')->__('Add New Article');
        parent::__construct();
    }
}
