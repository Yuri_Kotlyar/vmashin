<?php

class Actions_News_Block_Tags extends Actions_News_Block_Abstract {

    protected function _prepareLayout() {
        if ($head = $this->getLayout()->getBlock('head')) {
            // show breadcrumbs

            $moduleName = $this->getRequest()->getModuleName();
            $showBreadcrumbs = (int) Mage::getStoreConfig('actionsnews/news/showbreadcrumbs');
            if ($showBreadcrumbs && ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs'))) {
                $breadcrumbs->addCrumb('home', array(
                    'label' => Mage::helper('actionsnews')->__('Home'),
                    'title' => Mage::helper('actionsnews')->__('Go to Home Page'),
                    'link' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)));
                $breadcrumbs->addCrumb('actionsnews', array(
                    'label' => Mage::helper('actionsnews')->__(Mage::getStoreConfig('actionsnews/news/title')),
                    'title' => Mage::helper('actionsnews')->__('Return to %s', Mage::helper('actionsnews')->__('News')),
                    'link' => Mage::getUrl(Mage::helper('actionsnews')->getRoute())
                ));

                $breadcrumbs->addCrumb('tag', array(
                    'label' => Mage::helper('actionsnews')->__("Tag"),
                    'title' => Mage::helper('actionsnews')->__('Tag'),
                ));

                $breadcrumbs->addCrumb('tagitem', array(
                    'label' => Mage::helper('actionsnews')->__("Tag item"),
                    'title' => Mage::helper('actionsnews')->__('Tag item'),
                ));
            }

            if ($moduleName == 'actionsnews') {
                // set default meta data
                $head->setTitle(Mage::getStoreConfig('actionsnews/news/metatitle'));
                $head->setKeywords(Mage::getStoreConfig('actionsnews/news/metakeywords'));
                $head->setDescription(Mage::getStoreConfig('actionsnews/news/metadescription'));
                // set category meta data if defined
                $currentCategory = $this->getCurrentCategory();
                if ($currentCategory != null) {
                    if ($currentCategory->getTitle() != '') {
                        $head->setTitle($currentCategory->getTitle());
                    }
                    if ($currentCategory->getMetaKeywords() != '') {
                        $head->setKeywords($currentCategory->getMetaKeywords());
                    }
                    if ($currentCategory->getMetaDescription() != '') {
                        $head->setDescription($currentCategory->getMetaDescription());
                    }
                }
            }
        }
    }

    public function getCurrentCategory() {
        return false;
    }

    public function getNewsPost() {
        $tag = $this->getRequest()->getParam('tag');
        $tag = trim($tag);
        $tag = str_replace('-', ' ', $tag);
        $collection = Mage::getModel('actionsnews/news')->getCollection()
        ;
        $storeId = Mage::app()->getStore()->getId();
        $collection
                ->addEnableFilter(1)
                ->addStoreFilter($storeId)
                ->addStoreEnableFilter(1);
        $collection->getSelect()->where("main_table.tags LIKE '%" . $tag . "%'");
        return $collection;
    }

}
