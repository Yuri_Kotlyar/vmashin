<?php

class Actions_News_Model_News extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('actionsnews/news');
    }

    public function getUrl() {
        $urlOption = Mage::helper('actionsnews')->getUrloption();
        $url = "";
        if ($urlOption == 2) {
            $catUrl = Mage::app()->getRequest()->getPathInfo();
            if (substr($catUrl, 0, 1) == '/') {
                $catUrl = substr($catUrl, 1);
            }
            if (substr($catUrl, -1) == '/') {
                $catUrl = substr($catUrl, 0, -1);
            }
            $urlSuffix = '/' . $this->getIdentifier() . Mage::helper('actionsnews')->getNewsitemUrlSuffix();
            $catUrl = str_replace(Mage::helper('actionsnews')->getNewsitemUrlSuffix(), '', $catUrl);
            $url = Mage::getUrl() . $catUrl . $urlSuffix;
        } else {
            $url = Mage::getUrl(Mage::helper('actionsnews')->getRoute()) . $this->getIdentifier() . Mage::helper('actionsnews')->getNewsitemUrlSuffix();
        }
        return $url;
    }

    public function getCommentsCount() {
        $model = Mage::getModel('actionsnews/comment')->getCollection()
                ->addFieldToFilter('news_id', $this->getId())
                ->count();
        return $model;
    }

    public function getUrlPrint() {
        $urlOption = Mage::helper('actionsnews')->getUrloption();
        $url = "";
        if ($urlOption == 2) {
            $catUrl = str_replace(Mage::helper('actionsnews')->getNewsitemUrlSuffix(), '', Mage::registry('current_offi_news_cat_url'));
            $catUrl = str_replace(Mage::getUrl(Mage::helper('actionsnews')->getRoute()), '', $catUrl);
            if ($catUrl) {
                $catUrl = $catUrl . '/';
            }
            $url = Mage::getUrl(Mage::helper('actionsnews')->getRoute() . '/print') . $catUrl . $this->getIdentifier() . Mage::helper('actionsnews')->getNewsitemUrlSuffix();
        } else {
            $url = Mage::getUrl(Mage::helper('actionsnews')->getRoute()) . $this->getIdentifier() . Mage::helper('actionsnews')->getNewsitemUrlSuffix() . '?mode=print';
        }
        return $url;
    }

    /**
     * Reset all model data
     *
     * @return Actions_News_Model_News
     */
    public function reset() {
        $this->setData(array());
        $this->setOrigData();
        $this->_attributes = null;
        return $this;
    }

    public function getTimePublic() {
        if ($this->getPublicateFromDate() == NULL) {
            return Mage::helper('actionsnews')->formatDate($this->getCreatedTime());
        } else {
            return Mage::helper('actionsnews')->formatDate($this->getPublicateFromDate());
        }
    }

}
