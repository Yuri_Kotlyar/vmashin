<?php

class Actions_News_Model_Mysql4_CategoryStore extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('actionsnews/categoryStore', array('category_id,store_id'));
    }

}
