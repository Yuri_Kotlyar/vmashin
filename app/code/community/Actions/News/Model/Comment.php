<?php

class Actions_News_Model_Comment extends Mage_Core_Model_Abstract
{
    public function _construct(){
        $this->_init('actionsnews/comment');
    }

    public function load($id, $field=null){
        return parent::load($id, $field);
    }
}
