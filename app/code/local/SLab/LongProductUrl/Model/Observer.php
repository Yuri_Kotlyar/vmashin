<?php

/**
 * Rewrite Catalog url rewrite resource model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class SLab_LongProductUrl_Model_Observer  {

    const STORE_ID = 1;
    const IS_SYSTEM = 1;

    public function reindexRewriteUrlToLong() {
        $rewriteUrlModel = Mage::getModel('longurl/catalog_resource_url');

        $productsId = $rewriteUrlModel->getProductIds();
        foreach ($productsId as $productId) {
            $product = Mage::getModel('catalog/product')->load($productId);
            if ($product->getId()) {
                $longUrl = Mage::getSingleton('longurl/catalog_product_url')->getLongUrl($product); //.$product->getUrlPath();
                $rewritesByProductId = $rewriteUrlModel->getRewritesByProductId($productId, self::STORE_ID, self::IS_SYSTEM);
                foreach ($rewritesByProductId as $rewrite) {
                    if (($rewrite->getRequestPath() == $longUrl) || 
                        ($rewrite->getTargetPath() == $longUrl)  && 
                        ($rewrite->getOptions() == 'RP')
                      ){
                            continue;
                       }else{
                        $rewrite
                                ->setTargetPath($longUrl)
                                ->setOptions('RP')
//                    ->setIsSystem('0')
                        ;
                        try {
                            $rewriteUrlModel->updateRewrite($rewrite);
                        } catch (Exception $e) {

                        }
                    } 
                }
            }
        }
    }

}
