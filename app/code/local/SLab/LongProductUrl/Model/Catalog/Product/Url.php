<?php
/**
 * Rewrite Product Url model
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class SLab_LongProductUrl_Model_Catalog_Product_Url extends Mage_Catalog_Model_Product_Url
{

    public function getLongUrl(Mage_Catalog_Model_Product $product){
        $categoryIds = $product->getCategoryIds();
        $maxPath = '';
        foreach($categoryIds as $categoryId){
            $cat = Mage::getModel('catalog/category')->load($categoryId);
            if(substr_count($cat->getPath(),'/') > substr_count($maxPath,'/')){
                $maxPath = $cat->getUrlPath();
            }
        }
        $path = explode('.',$maxPath);
        $longPath = $path[0];

        $name = $product->getUrlKey().'.html';

        return $longPath.'/'.$name;
    }

    /**
     * Revrite Mehod
     * Retrieve Product URL
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  bool $useSid forced SID mode
     * @return string
     */
    public function getProductUrl($product, $useSid = null)
    {
        $longUrl = $this->getLongUrl($product);
        $product->setRequestPath($longUrl);

        if ($useSid === null) {
            $useSid = Mage::app()->getUseSessionInUrl();
        }

        $params = array();
        if (!$useSid) {
            $params['_nosid'] = true;
        }
        $result = $this->getUrl($product, $params);
        
        return $result;
    }


    /**
     * Revrite Mehod
     *
     * @param Mage_Catalog_Model_Product $product
     * @param type $params
     * @return type
     */
    public function getUrl(Mage_Catalog_Model_Product $product, $params = array())
    {
        $longUrl = $this->getLongUrl($product);
        $product->setRequestPath($longUrl);

        $routePath      = '';
        $routeParams    = $params;

        $storeId    = $product->getStoreId();
        if (isset($params['_ignore_category'])) {
            unset($params['_ignore_category']);
            $categoryId = null;
        } else {
            $categoryId = $product->getCategoryId() && !$product->getDoNotUseCategoryId()
                ? $product->getCategoryId() : null;
        }

        if ($product->hasUrlDataObject()) {
            $requestPath = $product->getUrlDataObject()->getUrlRewrite();
            $routeParams['_store'] = $product->getUrlDataObject()->getStoreId();
        } else {
            $requestPath = $product->getRequestPath();
            if (empty($requestPath) && $requestPath !== false) {
                $idPath = sprintf('product/%d', $product->getEntityId());
                if ($categoryId) {
                    $idPath = sprintf('%s/%d', $idPath, $categoryId);
                }
                $rewrite = $this->getUrlRewrite();
                $rewrite->setStoreId($storeId)
                    ->loadByIdPath($idPath);
                if ($rewrite->getId()) {
                    $requestPath = $rewrite->getRequestPath();
                    $product->setRequestPath($requestPath);
                } else {
                    $product->setRequestPath(false);
                }
            }
        }

        if (isset($routeParams['_store'])) {
            $storeId = Mage::app()->getStore($routeParams['_store'])->getId();
        }

        if ($storeId != Mage::app()->getStore()->getId()) {
            $routeParams['_store_to_url'] = true;
        }

        if (!empty($requestPath)) {
            $routeParams['_direct'] = $requestPath;
        } else {
            $routePath = 'catalog/product/view';
            $routeParams['id']  = $product->getId();
            $routeParams['s']   = $product->getUrlKey();
            if ($categoryId) {
                $routeParams['category'] = $categoryId;
            }
        }

        // reset cached URL instance GET query params
        if (!isset($routeParams['_query'])) {
            $routeParams['_query'] = array();
        }

        return $this->getUrlInstance()->setStore($storeId)
            ->getUrl($routePath, $routeParams);
    }


}
