<?php
/**
 * Rewrite Catalog url rewrite resource model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class SLab_LongProductUrl_Model_Catalog_Resource_Url extends Mage_Catalog_Model_Resource_Url
{

    /**
     * Retrieve rewrite by idPath
     *
     * @param string $idPath
     * @param int $storeId
     * @return Varien_Object|false
     */
    public function getRewriteByIdPath($idPath, $storeId)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable())
            ->where('store_id = :store_id')
            ->where('id_path = :id_path');
        $bind = array(
            'store_id' => (int)$storeId,
            'id_path'  => $idPath
        );
        $row = $adapter->fetchRow($select, $bind);

        if (!$row) {
            return false;
        }
        $rewrite = new Varien_Object($row);
        $rewrite->setIdFieldName($this->getIdFieldName());

        return $rewrite;
    }

    /**
     * Retrieve rewrite by requestPath
     *
     * @param string $requestPath
     * @param int $storeId
     * @return Varien_Object|false
     */
    public function getRewriteByRequestPath($requestPath, $storeId)
    {
        $adapter = $this->_getWriteAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable())
            ->where('store_id = :store_id')
            ->where('request_path = :request_path');
        $bind = array(
            'request_path'  => $requestPath,
            'store_id'      => (int)$storeId
        );
        $row = $adapter->fetchRow($select, $bind);

        if (!$row) {
            return false;
        }
        $rewrite = new Varien_Object($row);
        $rewrite->setIdFieldName($this->getIdFieldName());

        return $rewrite;
    }


    /**
     * Save rewrite URL
     *
     * @param array $rewriteData
     * @param int|Varien_Object $rewrite
     * @return Mage_Catalog_Model_Resource_Url
     */
    public function saveRewrite($rewriteData, $rewrite)
    {
        $adapter = $this->_getWriteAdapter();
        if ($rewrite && $rewrite->getId()) {
            if ($rewriteData['request_path'] != $rewrite->getRequestPath()) {
                $where = array($this->getIdFieldName() . '=?' => $rewrite->getId());
                $adapter->update($this->getMainTable(), $rewriteData, $where);

                // Update existing rewrites history and avoid chain redirects
                $where = array('target_path = ?' => $rewrite->getRequestPath());
                if ($rewrite->getStoreId()) {
                    $where['store_id = ?'] = (int)$rewrite->getStoreId();
                }
                $adapter->update(
                    $this->getMainTable(),
                    array('target_path' => $rewriteData['request_path']),
                    $where
                );
            }
        } else {
            try {
                $adapter->insert($this->getMainTable(), $rewriteData);
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::throwException(Mage::helper('catalog')->__('An error occurred while saving the URL rewrite'));
            }
        }
        unset($rewriteData);

        return $this;
    }

    public function getProductIds(){
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable());
        $row = $adapter->fetchAssoc($select);

        $productIds = array();
        if (!$row) {
            return $productIds;
        }

        foreach ($row as $rewrite) {
            $productIds[] = $rewrite['product_id'];
        }
        

        return array_unique($productIds);
    }
    
    public function getRewritesByProductId($productId, $storeId, $isSystem = '0'){
        
        $adapter = $this->_getWriteAdapter();
        $select = $adapter->select()
            ->from($this->getMainTable())
            ->where('store_id = :store_id')
            ->where('product_id = :product_id');

        if($isSystem!==false){
            $select->where('is_system = :is_system');
        }
        $bind = array(
            'product_id'  => $productId,
            'is_system'  => $isSystem,
            'store_id'      => (int)$storeId
        );

        $rewrites = array();

        foreach($adapter->fetchAssoc($select,$bind) as $row){
//            $row = $adapter->fetchRow($select, $bind);

            if (!$row) {
                continue;
            }
            $rewrite = new Varien_Object($row);
            $rewrite->setIdFieldName($this->getIdFieldName());

            $rewrites[] = $rewrite;
        }


        return $rewrites;
    }
    
    public function updateRewrite($rewrite){
        $adapter = $this->_getWriteAdapter();

        $where = array($this->getIdFieldName() . '=?' => $rewrite->getId());
        $adapter->update($this->getMainTable(), $rewrite->getData(), $where);

//        // Update existing rewrites history and avoid chain redirects
//        $where = array('target_path = ?' => $rewrite->getRequestPath());
//        if ($rewrite->getStoreId()) {
//            $where['store_id = ?'] = (int) $rewrite->getStoreId();
//        }
//        $adapter->update(
//                $this->getMainTable(), array('target_path' => $rewriteData['request_path']), $where
//        );

//        return $this;
        return true;
    }






}
