<?php


$installer = $this;

$installer->startSetup();

Mage::getModel('longurl/observer')->reindexRewriteUrlToLong();

$installer->endSetup();