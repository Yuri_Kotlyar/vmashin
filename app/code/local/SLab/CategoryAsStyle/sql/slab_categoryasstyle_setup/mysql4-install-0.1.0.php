<?php
/**
 * SLab extension for Magento
 *
 * Add category as style attribute
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the SLab CategoryAsStyle module to newer versions in the future.
 * If you wish to customize the SLab CategoryAsStyle module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   SLab
 * @package    SLab_CategoryAsStyle
 * @copyright  Copyright (C) 2012 SLab
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * @var $this Mage_Catalog_Model_Resource_Eav_Mysql4_Setup
 */

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    'catalog_category',
    'use_as_style',
    array(
        'type'     => 'int',
        'label'    => 'Use As Style',
        'input'    => 'select',
        'source'   => 'eav/entity_attribute_source_boolean',
        'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'required' => false,
        'default'  => 0
    )
);

$installer->endSetup();
