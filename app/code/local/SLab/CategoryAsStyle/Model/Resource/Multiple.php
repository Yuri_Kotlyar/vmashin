<?php
/**
 * SLab extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the SLab CategoryAsStyle module to newer versions in the future.
 * If you wish to customize the SLab CategoryAsStyle module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   SLab
 * @package    SLab_CategoryAsStyle
 * @copyright  Copyright (C) 2012 SLab
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * filter category as multiple select resource model
 *
 * @category   SLab
 * @package    SLab_CategoryAsStyle
 * @subpackage Model
 * @author     Vladimir Fishchenko <vladimir.fishchenko@gmail.com>
 */
class SLab_CategoryAsStyle_Model_Resource_Multiple extends Mage_Core_Model_Abstract
{
    protected $_categories;

    public function addCategoryFilter(
        Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection, Mage_Catalog_Model_Category $category
    ) {
        $this->_categories[] = $category->getId();

        $select = $collection->getSelect();
        $fromPart = $select->getPart(Zend_Db_Select::FROM);

        if (isset($fromPart['cat_index'])) {
            $conditions = $fromPart['cat_index']['joinCondition'];
            $conditions = explode(' AND ', $conditions);
            $newConditions = array();
            foreach ($conditions as $_condition) {
                if (strpos($_condition, 'cat_index.category_id') === false) {
                    $newConditions[] = $_condition;
                }
            }
            $fromPart['cat_index']['joinCondition'] = implode(' AND ', $newConditions);
            $fromPart['cat_index']['joinType'] = Zend_Db_Select::LEFT_JOIN;
        }

        $select->setPart(Zend_Db_Select::FROM, $fromPart);

        $select->distinct();
        $select->group('e.entity_id');

        $select->orWhere('cat_index.category_id IN (?)', $this->_categories);


        return $collection;
    }
}
