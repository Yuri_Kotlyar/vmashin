<?php
/**
 * Created by JetBrains PhpStorm.
 * User: svyatoslav
 * Date: 18.06.12
 * Time: 8:26
 * To change this template use File | Settings | File Templates.
 */
class SLab_Slideshow_Block_Item extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getSlideshow()
    {
        if (!$this->hasData('slideshow')) {
            $this->setData('slideshow', Mage::registry('slideshow'));
        }
        return $this->getData('slideshow');

    }
}