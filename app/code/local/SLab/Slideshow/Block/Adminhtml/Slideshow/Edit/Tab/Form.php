<?php

class SLab_Slideshow_Block_Adminhtml_Slideshow_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('slideshow_form', array('legend'=>Mage::helper('slideshow')->__('Item information')));

      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Name'),
          'required'  => false,
          'name'      => 'name',
      ));

      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('slideshow')->__('Content'),
          'title'     => Mage::helper('slideshow')->__('Content'),
          'style'     => 'width:700px; height:100px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));

      $fieldset->addField('price', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Price'),
          'required'  => false,
          'name'      => 'price',
      ));

      $fieldset->addField('button', 'select', array(
          'label'     => Mage::helper('slideshow')->__('Button'),
          'name'      => 'button',
          'values'    => array(
              array(
                  'value'     => 'Нет',
                  'label'     => Mage::helper('slideshow')->__('Нет'),
              ),

              array(
                  'value'     => 'Купить',
                  'label'     => Mage::helper('slideshow')->__('Купить'),
              ),

              array(
                  'value'     => 'Подробнее',
                  'label'     => Mage::helper('slideshow')->__('Подробнее'),
              ),
          ),
      ));

      $fieldset->addField('img', 'imagefile', array(
          'label'     => Mage::helper('slideshow')->__('Image'),
          'required'  => false,
          'name'      => 'img',
      ));

      $fieldset->addField('url', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Url'),
          'required'  => false,
          'name'      => 'url',
      ));


      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('slideshow')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slideshow')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('slideshow')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getSlideshowData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSlideshowData());
          Mage::getSingleton('adminhtml/session')->setSlideshowData(null);
      } elseif ( Mage::registry('slideshow_data') ) {
          $form->setValues(Mage::registry('slideshow_data')->getData());
      }
      return parent::_prepareForm();
  }
}