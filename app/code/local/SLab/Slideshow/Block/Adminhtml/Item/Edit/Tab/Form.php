<?php

class SLab_Slideshow_Block_Adminhtml_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('slideshow_form', array('legend'=>Mage::helper('slideshow')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Name'),
          'required'  => true,
          'name'      => 'name',
      ));

      $fieldset->addField('price', 'text', array(
          'label'     => Mage::helper('slideshow')->__('Price'),
          'required'  => true,
          'name'      => 'price',
      ));

      $fieldset->addField('img', 'imagefile', array(
          'label'     => Mage::helper('slideshow')->__('Image'),
          'required'  => false,
          'name'      => 'img',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('slideshow')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('slideshow')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('slideshow')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('slideshow')->__('Content'),
          'title'     => Mage::helper('slideshow')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getSlideshowData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getSlideshowData());
          Mage::getSingleton('adminhtml/session')->setSlideshowData(null);
      } elseif ( Mage::registry('slideshow_data') ) {
          $form->setValues(Mage::registry('slideshow_data')->getData());
      }
      return parent::_prepareForm();
  }
}