<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('slideshow_item')};
CREATE TABLE {$this->getTable('slideshow_item')} (
  `item_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `content` text NOT NULL default '',
  `price` int(11) NOT NULL default '0',
  `button` varchar(255) NOT NULL default '',
  `img` varchar(255) NOT NULL default '',
  `status` int (11) NOT NULL default '1',
  `url` varchar(255) NOT NULL default '',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 