<?php
/**
 * Created by JetBrains PhpStorm.
 * User: svyatoslav
 * Date: 04.06.12
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */

class SLab_RequestCall_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('core/session');
        $this->renderLayout();
    }

    public function sentAction(){
        $params = $this->getRequest()->getParams();
        $sender = array(
            'email' => Mage::getStoreConfig('trans_email/ident_general/email'),
            'name' => Mage::getStoreConfig('trans_email/ident_general/name'));
        $from = $params['h_from'].':'.$params['m_from'];
        $to = $params['h_to'].':'.$params['m_to'];
        if (($params['cmb_name'] == '') or ($params['cmb_phone'] == '')){
            Mage::getSingleton('core/session')->addError(Mage::helper('core')->__('Пожалуйста, заполните поля отмеченные * '));
            $this->_redirect('callmeback/index/index', array('error' => 'fail'));
            return;
        }
        $vars = array(
            'cust_name' => $params['cmb_name'],
            'cust_phone' => $params['cmb_phone'],
            'from' => $from,
            'to' => $to,
            'cust_comment' => $params['comment'],
        );

        $template_Id = 1;

        try {
            Mage::getModel('core/email_template')
                ->setDesignConfig(array('area' => 'frontend', 'store' => Mage::app()->getStore()->getId()))
                ->sendTransactional($template_Id, $sender, $sender, '', $vars);

//            Mage::getSingleton('core/session')->addSuccess(Mage::helper('core')->__('Вы успешно оставили заявку на звонок от Администратора.'));
            $data['result'] = true;
            $data['message'] = Mage::helper('core')->__('Вы успешно оставили заявку на звонок от Администратора.');
            echo json_encode($data);
            return;
        } catch (Exception $e) {
            $data['result'] = false;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            return;
        }
    }
}