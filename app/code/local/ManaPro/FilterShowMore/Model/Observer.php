<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterShowMore
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
/* BASED ON SNIPPET: Models/Observer */
/**
 * This class observes certain (defined in etc/config.xml) events in the whole system and provides public methods - handlers for
 * these events.
 * @author Mana Team
 *
 */
class ManaPro_FilterShowMore_Model_Observer {
	/* BASED ON SNIPPET: Models/Event handler */
	/**
	 * In case filter item list is too long, truncates it and sets a flag on whole filter to enable additional 
	 * show more/show less actions on it. 
	 * @param Varien_Event_Observer $observer
	 */
	public function limitNumberOfVisibleItems($observer) {
		/* @var $filter Mage_Catalog_Model_Layer_Filter_Abstract */ $filter = $observer->getEvent()->getFilter();
		/* @var $items Varien_Object */ $items = $observer->getEvent()->getItems();
		
		/* @var $_helper ManaPro_FilterShowMore_Helper_Data */ $_helper = Mage::helper(strtolower('ManaPro_FilterShowMore'));
		if (!$filter->getMIsShowMoreDisabled()) {
			/* @var $m Mana_Core_Helper_Data */ $m = Mage::helper(strtolower('Mana_Core'));
                        $maxItemCount = $filter->getFilterOptions()->getShowMoreItemCount();
			if (count($items->getItems()) > $maxItemCount) {
//				if (!$_helper->isShowAllRequested($filter)) {
//					$items->setItems(array_slice($items->getItems(), 0, $maxItemCount));
//				}
				$filter->setMIsShowMoreApplied(true);
			}
		}
	}
	
}