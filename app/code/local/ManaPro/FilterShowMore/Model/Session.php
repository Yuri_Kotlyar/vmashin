<?php
/**
 *  extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the ManaPro FilterShowMore module to newer versions in the future.
 * If you wish to customize the ManaPro FilterShowMore module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   ManaPro
 * @package    ManaPro_FilterShowMore
 * @copyright  Copyright (C) 2012 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Show more/less session
 *
 * @category   ManaPro
 * @package    ManaPro_FilterShowMore
 * @subpackage Model
 * @author     Vladimir Fishchenko <vladimir.fishchenko@gmail.com>
 */
class ManaPro_FilterShowMore_Model_Session extends Mage_Core_Model_Session
{
    /** @var \Mage_Core_Model_Cookie */
    protected  $_cookie = null;

    public function __construct()
    {
        $this->init('showmore');
        $this->_cookie = Mage::getSingleton('core/cookie');
    }

    public function expandFilter($attributeCode)
    {
        $options = $this->getExpandedFilters();

        if(!isset($options[$attributeCode])) {
            $options[$attributeCode] = 1;
        }

        $this->setExpandedFilters($options);
    }

    public function collapseFilter($attributeCode)
    {
        $options = $this->getExpandedFilters();

        if(isset($options[$attributeCode])) {
            unset($options[$attributeCode]);
        }

        $this->setExpandedFilters($options);
    }

    public function isFilterExpanded($attributeCode)
    {
        $options = $this->getExpandedFilters();
        if(isset($options[$attributeCode])) {
            return $options[$attributeCode];
        }
        return false;
    }

    public function getExpandedFilters()
    {
        return Zend_Json::decode($this->_cookie->get('expandedFilters'));
    }

    public function setExpandedFilters($value)
    {
        $this->_cookie->set('expandedFilters', Zend_Json::encode($value), null, null, '/');
        return $this;
    }
}
