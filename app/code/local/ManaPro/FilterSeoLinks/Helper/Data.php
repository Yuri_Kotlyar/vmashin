<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterSeoLinks
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
/* BASED ON SNIPPET: New Module/Helper/Data.php */
/**
 * Generic helper functions for ManaPro_FilterSeoLinks module. This class is a must for any module even if empty.
 * @author Mana Team
 */
class ManaPro_FilterSeoLinks_Helper_Data extends Mage_Core_Helper_Abstract {
	// INSERT HERE: helper functions that should be available from any other place in the system
}