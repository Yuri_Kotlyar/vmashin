<?php

$installer = $this;

$installer->startSetup();

$table = 'm_translate';

$installer->run("
    TRUNCATE TABLE `{$this->getTable($table)}`;
    ALTER TABLE `{$this->getTable($table)}` ADD `attribute_value_id` int(11) NOT NULL;
    ");

$installer->endSetup();