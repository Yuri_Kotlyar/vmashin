<?php

$installer = $this;

$installer->startSetup();

$table = 'm_translate';
$installer->run("
	DROP TABLE IF EXISTS `{$this->getTable($table)}`;
	CREATE TABLE `{$this->getTable($table)}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `translated` varchar(30) NOT NULL,
    `attribute_code` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';

");
$installer->endSetup();