<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterSeoLinks
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
/* BASED ON SNIPPET: Models/Observer */
/**
 * This class observes certain (defined in etc/config.xml) events in the whole system and provides public methods - handlers for
 * these events.
 * @author Mana Team
 *
 */
class ManaPro_FilterSeoLinks_Model_Observer {
	/* BASED ON SNIPPET: Models/Event handler */
	/**
	 * Replaces ?a=1&b=2 with /a/1/b/2 (handles event "mana_filters_url")
	 * @param Varien_Event_Observer $observer
	 */
	public function getSeoFriendlyUrl($observer) {
		/* @var $route string */ $route = $observer->getEvent()->getRoute();
		/* @var $wrapper Varien_Object */ $wrapper = $observer->getEvent()->getParams();

        $params = $wrapper->getParams();
        if (!$wrapper->getIsHandled()) {
        
			$result = str_replace('&amp;', '&', Mage::getUrl($route, $params));
			if (strpos($result, '/catalogsearch/result/index') !== false) {
				return;
			}
			$parts = parse_url($result);
				
        	/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
			/* @var $resource ManaPro_FilterSeoLinks_Resource_Rewrite */
            $resource = Mage::getResourceSingleton('manapro_filterseolinks/rewrite');
			$conditionalWord = $core->getStoreConfig('mana_filters/seo/conditional_word');
		    $categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix();
			$showAllSuffix = $core->getStoreConfig('mana_filters/seo/show_all_suffix');
            $showAllSeoSuffix = ($showAllSuffix) ? $core->hyphenCased($showAllSuffix) : '';
				
			$result = Mage::getBaseUrl();
            $specialResult = trim($result, '/');
			$path = $parts['scheme'].'://'.$parts['host'].$parts['path'];
			if (!$core->startsWith($path, $result)) throw new Exception('Not implemented');
			$path = substr($path, strlen($result));
			
			if ($categorySuffix) {
				if ($core->endsWith($path, $categorySuffix)) {
					$path = substr($path, 0, strlen($path) - strlen($categorySuffix));
					$categorySuffixSubtracted = true;
				}
				else {
					$categorySuffixSubtracted = false;
				}
			}
			$result .= $path;
            $leftQuery = '';
            if (isset($parts['query'])) {
				$query = array();				
				parse_str($parts['query'], $query);								
				$seoQuery = '';
				foreach ($query as $key => $value) {
					if ($showAllSuffix && strrpos($key, $showAllSuffix) === strlen($key) - strlen($showAllSuffix)) {
						if ($value == 1) {
							$filterCode = $core->hyphenCased(substr($key, 0, strlen($key) - strlen($showAllSuffix)));
							if ($filterCode == 'cat') {
								$filterCode = 'category';
							}
							$seoQuery .= '/'.$filterCode.$showAllSeoSuffix;
						}
					}
					else {
						if ($key == 'cat') {
							$seoQuery .= '/category/';
							$valueLabel = array();
							foreach (explode('_', $value) as $valueFragment) {
								$valueLabel[] = $resource->getCategoryLabel($valueFragment);
							}
							$seoQuery .= implode('_', $valueLabel);
						}
						elseif ($resource->isFilterName($key)) {
							$seoQuery .= '/'.$core->hyphenCased($key).'/';
							$valueLabel = array();							
							foreach (explode('_', $value) as $valueFragment) {							
								$valueLabel[] = $resource->getFilterValueLabel($key, $valueFragment);
							}
							$seoQuery .= implode('_', $valueLabel);
						}
						elseif ($key == 'q' && strpos($result, '/catalogsearch/result/index') !== false) {
							$seoQuery .= '/'.$key.'/'.$value;
						}
						else {
							if ($leftQuery) $leftQuery .= '&';//'&amp;';
							$leftQuery .= $key . '=' . $value;
						}
					}
				}
				if ($seoQuery) {
					if (!$core->endsWith($result, '/')) {
						$result .= '/';
					}
					$result .= $conditionalWord.$seoQuery;
                    $specialResult .= $seoQuery;
				}
			}

            /** @var $special ManaPro_FilterSeoLinks_Model_Special */
            $special = Mage::getSingleton('manapro_filterseolinks/special');
            /** @var $category Mage_Catalog_Model_Category */
            $category = Mage::registry('current_category');
            if (!$category) {
                $category = Mage::getModel('catalog/category')->loadByAttribute('url_key', $path);
            }

            if (
                $category instanceof Mage_Catalog_Model_Category
                && $category->getId() == ManaPro_FilterSeoLinks_Model_Special::ALL_PRODUCTS_CATEGORY_ID
                || (isset($params['_do_not_check_category']) && $params['_do_not_check_category'])
            ) {
                if ($special->canBeSpecial($specialResult)) {
                    $result = $specialResult;
                }
            }

            if ($categorySuffix) $result .= $categorySuffix;
			if ($leftQuery) $result .= '?' . $leftQuery;
			$wrapper->setResult($result);
			$wrapper->setIsHandled(true);
		}
	}

    /**
     * check url
     *
     * @param Varien_Event_Observer $observer
     *
     * @return void
     */
    public function checkUrl($observer)
    {
        /** @var $controllerAction Mage_Core_Controller_Varien_Action */
        $controllerAction = $observer->getEvent()->getControllerAction();
        $request = $controllerAction->getRequest();
        $currentUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getOriginalRequest()->getRequestUri();
        $seoUrl = Mage::helper('mana_filters/extended')->getFilterUrl(
            '*/*/*',
            array('_current' => true, '_use_rewrite' => true)
        );
        
        
        if ($currentUrl != $seoUrl) {     
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: '.$seoUrl);
            exit;
        }
    }
}
