<?php
/**
 * SLab extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the ManaPro FilterSeoLinks module to newer versions in the future.
 * If you wish to customize the ManaPro FilterSeoLinks module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   ManaPro
 * @package    ManaPro_FilterSeoLinks
 * @copyright  Copyright (C) 2012 SLab
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Get data for special url
 *
 * @category   ManaPro
 * @package    ManaPro_FilterSeoLinks
 * @subpackage Model
 * @author     Vladimir Fishchenko <vladimir.fishchenko@gmail.com>
 */
class ManaPro_FilterSeoLinks_Model_Special extends Mage_Core_Model_Abstract
{
    const SPECIAL_URL_PATH = 'seourl/special';
    const PREFIX_PATH = 'prefix';

    /**
     * HARDCODED all rugs category id
     */
    const ALL_PRODUCTS_CATEGORY_ID = 10;

    public function getRequestPath($requestPath)
    {
        $isSpecialUrlRequest = false;
        foreach ($this->_getSpecialUrlPrefixes() as $_prefix) {
            if (strpos($requestPath, $_prefix . '/') === 0) {
                $isSpecialUrlRequest = true;
                break;
            }
        }

        if ($isSpecialUrlRequest) {
            /* @var $_core Mana_Core_Helper_Data */
            $_core = Mage::helper(strtolower('Mana_Core'));
            $conditionalWord = $_core->getStoreConfig('mana_filters/seo/conditional_word');
            $urlKey = Mage::getModel('catalog/category')->load(self::ALL_PRODUCTS_CATEGORY_ID)->getUrlKey();
            $requestPath = $urlKey . '/' . $conditionalWord . '/' . $requestPath;
        }
        return $requestPath;
    }

    protected function _getSpecialUrlPrefixes()
    {
        if (!$this->hasData('prefixes')) {
            $data = Mage::app()->getConfig()->getNode(self::SPECIAL_URL_PATH)->asArray();
            $prefixes = array();
            foreach ($data as $_url) {
                $prefixes[] = $_url[self::PREFIX_PATH];
            }
            $this->setData('prefixes', $prefixes);
        }
        return (array) $this->getData('prefixes');
    }

    public function canBeSpecial($path)
    {
        $canBeSpecial = false;
        $path = str_replace(Mage::getBaseUrl(), '',$path);
        foreach ($this->_getSpecialUrlPrefixes() as $_prefix) {
            if (strpos($path, $_prefix . '/') === 0) {
                $canBeSpecial = true;
                break;
            }
        }
        return $canBeSpecial;
    }
}
