<?php

class ManaPro_FilterSeoLinks_Model_Translate extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		$this->_init('manapro_filterseolinks/translate');
	}

    public function addTranslate($translate, $attribute_value_id=false, $attribute_code=false) {
//        $res = $this->loadByTranslate($translate);
        $res = $this->loadByTranslateAndAttributeCode($translate,$attribute_code);
        if ((count($res) == 0) && $attribute_code) {
            $data = array(
                'translated' => $translate,
                'attribute_code' => $attribute_code,
                'attribute_value_id' => (int)$attribute_value_id,
            );
            $this->getResource()->_write->insert($this->getResource()->getTable('manapro_filterseolinks/translate'), $data);
        }
    }

//    public function loadByTranslate($translate){
//        return $this->getResource()->loadByCode($translate);
//    }

    public function loadByTranslateAndAttributeCode($translate,$attributeCode){
        return $this->getResource()->loadByCodeAndTranslate($translate,$attributeCode);
    }


}
