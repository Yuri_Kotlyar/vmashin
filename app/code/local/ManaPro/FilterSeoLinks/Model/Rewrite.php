<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterSeoLinks
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
/**
 * Extends standard Magento SEO techniques in layered navigation urls. 
 * Standard Magento parses "apparel/shoes.html?shoe_type=52&shoe_type-show-all=1" into 
 * "catalog/category/view/id/5?shoe_type=52&shoe_type-sh". For layered navigation we go further and parse
 * "apparel/shoes/where/shoe-type/golf-shoes/shoe-type-show-all.html" into the same original URL. 
 * @author Mana Team
 *
 */
class ManaPro_FilterSeoLinks_Model_Rewrite extends Mage_Core_Model_Url_Rewrite {

    public function _construct() {
    	parent::_construct();
    	$this->_resourceName = 'manapro_filterseolinks/rewrite';
    }
    /** 
     * Implements database-centered URL rewrite logic (translates human friendly URLs into standard 
     * module/controller/action/params form).
     * 
     * @see Mage_Core_Model_Url_Rewrite::rewrite()
     * 
     * This method is based on Mage_Core_Model_Url_Rewrite::rewrite() source. Modifications are marked with comments.
     */
    public function rewrite(Zend_Controller_Request_Http $request=null, Zend_Controller_Response_Http $response=null)
    {
        if (!Mage::isInstalled()) {
            return false;
        }
        if (is_null($request)) {
            $request = Mage::app()->getFrontController()->getRequest();
        }
        if (is_null($response)) {
            $response = Mage::app()->getFrontController()->getResponse();
        }
        if (is_null($this->getStoreId()) || false===$this->getStoreId()) {
            $this->setStoreId(Mage::app()->getStore()->getId());
        }
        
        /**
         * We have two cases of incoming paths - with and without slashes at the end ("/somepath/" and "/somepath").
         * Each of them matches two url rewrite request paths - with and without slashes at the end ("/somepath/" and "/somepath").
         * Choose any matched rewrite, but in priority order that depends on same presence of slash and query params.
         */
        $requestCases = array();
        $pathInfo = $request->getPathInfo();
        $origSlash = (substr($pathInfo, -1) == '/') ? '/' : '';
        $requestPath = trim($pathInfo, '/');
        
        $altSlash = $origSlash ? '' : '/'; // If there were final slash - add nothing to less priority paths. And vice versa.
        $queryString = $this->_getQueryString(); // Query params in request, matching "path + query" has more priority
        if ($queryString) {
            $requestCases[] = $requestPath . $origSlash . '?' . $queryString;
            $requestCases[] = $requestPath . $altSlash . '?' . $queryString;
        }
        $requestCases[] = $requestPath . $origSlash;
        $requestCases[] = $requestPath . $altSlash;

        // MANA BEGIN: in case we have specially named segment in URL suspect it is layered navigation url. Then 
        // add additional checks to $requestCases
        /* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
        $conditionalWord = $_core->getStoreConfig('mana_filters/seo/conditional_word');
        $categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix();

        //update request path if it is a special url
        $requestPath = Mage::getSingleton('manapro_filterseolinks/special')->getRequestPath($requestPath);

        if ($conditionalWord && (($conditionPos = strpos($requestPath, '/'.$conditionalWord.'/')) != false) &&
        	(!$categorySuffix || strrpos($requestPath, $categorySuffix) == strlen($requestPath) - strlen($categorySuffix))) 
        {
        	$layeredPath = substr($requestPath, 0, $conditionPos) . $categorySuffix;
	        $requestCases[] = $layeredPath . $origSlash;
	        $requestCases[] = $layeredPath . $altSlash;
        } else {
            $layeredPath = false;
        }
        // MANA END
        
        $this->loadByRequestPath($requestCases);

        /**
         * Try to find rewrite by request path at first, if no luck - try to find by id_path
         */
        if (!$this->getId() && isset($_GET['___from_store'])) {
            try {
                $fromStoreId = Mage::app()->getStore($_GET['___from_store'])->getId();
            }
            catch (Exception $e) {
                return false;
            }

            $this->setStoreId($fromStoreId)->loadByRequestPath($requestCases);
            if (!$this->getId()) {
                return false;
            }
            $this->setStoreId(Mage::app()->getStore()->getId())->loadByIdPath($this->getIdPath());
        }

        if (!$this->getId()) {
            return false;
        }

		// MANA BEGIN: in case we found rewrite based on filtered navigation url, do some magic passes
		// around request object and $_SERVER['QUERY_STRING'] to make it look like 
		// "apparel/shoes.html?shoe_type=52&shoe_type-show-all=1"
		if ($layeredPath !== false && in_array($this->getRequestPath(), array($layeredPath . $origSlash, $layeredPath . $altSlash))
			&& $this->getCategoryId()) {
			if ($categorySuffix) {
				$parameters = substr($requestPath, $conditionPos + strlen('/'.$conditionalWord.'/'), 
					strlen($requestPath) - strlen($categorySuffix) - $conditionPos - strlen('/'.$conditionalWord.'/'));
			}
			else {
				$parameters = substr($requestPath, conditionPos + strlen('/'.$conditionalWord.'/'));
			}
			$_SERVER['QUERY_STRING'] = http_build_query($this->_getQueryParameters($parameters));
		}
        // MANA END
        
        $request->setAlias(self::REWRITE_REQUEST_PATH_ALIAS, $this->getRequestPath());
        $external = substr($this->getTargetPath(), 0, 6);
        $isPermanentRedirectOption = $this->hasOption('RP');
        if ($external === 'http:/' || $external === 'https:') {
            if ($isPermanentRedirectOption) {
                header('HTTP/1.1 301 Moved Permanently');
            }
            header("Location: ".$this->getTargetPath());
            exit;
        } else {
            $targetUrl = $request->getBaseUrl(). '/' . $this->getTargetPath();
        }
        $isRedirectOption = $this->hasOption('R');
        if ($isRedirectOption || $isPermanentRedirectOption) {
            if (Mage::getStoreConfig('web/url/use_store') && $storeCode = Mage::app()->getStore()->getCode()) {
                $targetUrl = $request->getBaseUrl(). '/' . $storeCode . '/' .$this->getTargetPath();
            }
            if ($isPermanentRedirectOption) {
                header('HTTP/1.1 301 Moved Permanently');
            }
            header('Location: '.$targetUrl);
            exit;
        }

        if (Mage::getStoreConfig('web/url/use_store') && $storeCode = Mage::app()->getStore()->getCode()) {
                $targetUrl = $request->getBaseUrl(). '/' . $storeCode . '/' .$this->getTargetPath();
            }

        $queryString = $this->_getQueryString();
        if ($queryString) {
            $targetUrl .= '?'.$queryString;
        }
        $request->setRequestUri($targetUrl);
        $request->setPathInfo($this->getTargetPath());
        
        return true;
    }
    
    protected function _getQueryParameters($parameters) {
    	$parameters = explode('/', $parameters);
    	$state = 0; // waiting for parameter
    	$result = array();
        
        /* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
		$showAllSuffix = $_core->getStoreConfig('mana_filters/seo/show_all_suffix');
		if ($showAllSuffix) $showAllSeoSuffix = $_core->hyphenCased($showAllSuffix);
    	foreach ($parameters as $parameter) {
    		switch ($state) {
    			case 0: // waiting for parameter
    				if ($showAllSuffix && strrpos($parameter, $showAllSeoSuffix) === strlen($parameter) - strlen($showAllSeoSuffix)) {
    					$requestVar = substr($parameter, 0, strlen($parameter) - strlen($showAllSeoSuffix));
    					$filterName = $this->_getFilterName($requestVar);
    					$result[$filterName.$showAllSuffix] = 1;
    				}
    				else {
    					$filterName = $this->_getFilterName($parameter);
    					$state = 1; // waiting for value
    				}
    				break;
    			case 1: // waiting for value
    				$values = array();
    				foreach (explode('_', $parameter) as $value) {
    					$values_temp = $this->_getFilterValue($filterName, $value);

                        if ($values_temp==false){
                            $translaters = Mage::getSingleton('manapro_filterseolinks/translate')->loadByTranslateAndAttributeCode($value, $filterName);;
                            if (isset($translaters[0])){
                                $values_temp = $translaters[0]['attribute_value_id'];
                            }
                        }
                        $values[] = $values_temp;
    				}
    				$result[$filterName] = implode('_', $values);
    				$state = 0; // waiting for parameter
    				break;
    		}
    	}
    	return $result;
    }
    protected function _getFilterName($seoName) {
    	if ($seoName == 'category') {
    		return 'cat'; 
    	}
    	else {
        	/* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
    		$candidateNames = array($seoName, $_core->lowerCased($seoName));
    		/*@var $resource ManaPro_FilterSeoLinks_Resource_Rewrite */ $resource = $this->_getResource();
    		return $resource->getFilterName($candidateNames);
    	}
    }
    
    protected function _getFilterValue($name, $seoValue) {
    	/*@var $resource ManaPro_FilterSeoLinks_Resource_Rewrite */ $resource = $this->_getResource();
    	if ($name == 'cat') {
    		return $resource->getCategoryValue($this, $seoValue);
    	}
    	else {
    		return $resource->getFilterValue($this, $name, $seoValue);
    	}
    }
}
