<?php

class ManaPro_FilterSeoLinks_Resource_Translate extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct() {
        $this->_init('manapro_filterseolinks/translate', 'id');
        $this->_read = $this->_getReadAdapter();
        $this->_write = $this->_getWriteAdapter();
    }

//    public function loadByCode($code) {
//		$select = $this->_read->select()
//            ->from($this->getTable('manapro_filterseolinks/translate'))
//            ->where('translated=?',$code);
//
//        if($result = $this->_read->fetchAll($select)) {
//        	return $result;
//        }
//
//		return array();
//	}

    public function loadByCodeAndTranslate($translate,$attributeCode) {
		$select = $this->_read->select()
            ->from($this->getTable('manapro_filterseolinks/translate'))
            ->where('translated=?',$translate)
            ->where('attribute_code=?',$attributeCode);

        if($result = $this->_read->fetchAll($select)) {            
        	return $result;
        }

		return array();
	}

}
