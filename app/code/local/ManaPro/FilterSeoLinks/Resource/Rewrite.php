<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterSeoLinks
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */

/**
 * SQL Selects for SEO friendly layered navigation URLs
 * @author Mana Team
 *
 */
class ManaPro_FilterSeoLinks_Resource_Rewrite extends Mage_Core_Model_Mysql4_Url_Rewrite {
    /**
     * attributes
     *
     * @var array
     */
    protected $_attributes = array();

	public function isFilterName($name) {
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
        if (!isset($this->_attributes[$name])) {
            $select
                ->from(array('a' => $this->_resources->getTableName('eav_attribute')), array('attribute_id', 'backend_model', 'source_model', 'backend_type', 'frontend_input'))
                ->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
                ->where('a.attribute_code = ?', $name)
                ->where('t.entity_type_code = ?', 'catalog_product');
            $attribute = $this->_getReadAdapter()->fetchRow($select);
            $this->_attributes[$name] = $attribute ? $attribute : false;
        }
        return (bool) $this->_attributes[$name];
	}
	public function getFilterName($candidates) {
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
		$select 
			->from(array('a' => $this->_resources->getTableName('eav_attribute')), 'attribute_code')
			->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
			->where('a.attribute_code IN (?)', $candidates)
			->where('t.entity_type_code = ?', 'catalog_product');			
		return $this->_getReadAdapter()->fetchOne($select);
	}
	
	public function getCategoryValue($model, $urlKey) {
		$path = $this->_getReadAdapter()->fetchOne("SELECT path FROM {$this->_resources->getTableName('catalog_category_entity')} WHERE entity_id = ?", $model->getCategoryId());
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
		$select
			->from(array('e' => $this->_resources->getTableName('catalog_category_entity')), 'entity_id')
			->join(array('v' => $this->_resources->getTableName('catalog_category_entity_varchar')), 'v.entity_id = e.entity_id', null)
			->join(array('a' => $this->_resources->getTableName('eav_attribute')), 'a.attribute_id = v.attribute_id', null)
			->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
			->where('v.value = ?', $urlKey)
			->where('t.entity_type_code = ?', 'catalog_category')
			->where('a.attribute_code = ?', 'url_key')
//			->where('e.path LIKE ?', $path.'/%')
			->where('v.store_id IN (?)', array(0, $model->getStoreId()));
		return $this->_getReadAdapter()->fetchOne($select);
	}
	
	public function getFilterValue($model, $name, $candidate) {
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
        if (!isset($this->_attributes[$name])) {
            $select
                ->from(array('a' => $this->_resources->getTableName('eav_attribute')), array('attribute_id', 'backend_model', 'source_model', 'backend_type', 'frontend_input'))
                ->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
                ->where('a.attribute_code = ?', $name)
                ->where('t.entity_type_code = ?', 'catalog_product');
            $attribute = $this->_getReadAdapter()->fetchRow($select);
            $this->_attributes[$name] = $attribute;
        } else {
            $attribute = $this->_attributes[$name];
        }

        if (is_array($candidate)) {
            $candidate = (string) current($candidate);
        }

        $like = '%' . str_replace(array('-'), '%', (string) $candidate) . '%';

		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
		if ($attribute['backend_model'] == 'eav/entity_attribute_backend_array' || $attribute['source_model'] == 'eav/entity_attribute_source_table' && !$attribute['backend_model'] == 'eav/entity_attribute_backend_price' || $attribute['frontend_input'] == 'select' && !$attribute['source_model']) {		
            $select
				->from(array('o' => $this->_resources->getTableName('eav_attribute_option')), 'option_id')
				->join(array('v' => $this->_resources->getTableName('eav_attribute_option_value')), 'o.option_id = v.option_id', null)
				->where('LOWER(v.value) LIKE ?', $like)
				->where('o.attribute_id = ?', $attribute['attribute_id'])
				->where('v.store_id IN (?)', array(0, $model->getStoreId()));
			return $this->_getReadAdapter()->fetchOne($select);
		}
		else {
			return $candidate;
		}
	}
	public function getCategoryLabel($categoryId) {
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
		$select
			->from(array('e' => $this->_resources->getTableName('catalog_category_entity')), null)
			->join(array('v' => $this->_resources->getTableName('catalog_category_entity_varchar')), 'v.entity_id = e.entity_id', 'value')
			->join(array('a' => $this->_resources->getTableName('eav_attribute')), 'a.attribute_id = v.attribute_id', null)
			->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
			->where('e.entity_id = ?', $categoryId)
			->where('t.entity_type_code = ?', 'catalog_category')
			->where('a.attribute_code = ?', 'url_key')
			->where('v.store_id IN (?)', array(0, Mage::app()->getStore()->getId()));			
		return $this->_getReadAdapter()->fetchOne($select);
	}
	public function getFilterValueLabel($code, $value) {	
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
        if (!isset($this->_attributes[$code])) {
            $select
                ->from(array('a' => $this->_resources->getTableName('eav_attribute')), array('attribute_id', 'backend_model', 'source_model', 'backend_type', 'frontend_input'))
                ->join(array('t' => $this->_resources->getTableName('eav_entity_type')), 't.entity_type_id = a.entity_type_id', null)
                ->where('a.attribute_code = ?', $code)
                ->where('t.entity_type_code = ?', 'catalog_product');
            $attribute = $this->_getReadAdapter()->fetchRow($select);
            //cache attribute
            $this->_attributes[$code] = $attribute;
        } else {
            //get from cache
            $attribute = $this->_attributes[$code];
        }
		/* @var $select Varien_Db_Select */ $select = $this->_getReadAdapter()->select();
		if ($attribute['backend_model'] == 'eav/entity_attribute_backend_array' || $attribute['source_model'] == 'eav/entity_attribute_source_table' && !$attribute['backend_model'] == 'eav/entity_attribute_backend_price' || $attribute['frontend_input'] == 'select' && !$attribute['source_model']) {
			/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
            if (!isset($this->_attributes[$code]['values'])) {
                $select
                    ->from(array('o' => $this->_resources->getTableName('eav_attribute_option')), null)
                    ->join(array('v' => $this->_resources->getTableName('eav_attribute_option_value')), 'o.option_id = v.option_id', array('option_id', 'value'))
//                    ->where('o.option_id = ?', $value)
                    ->where('o.attribute_id = ?', $attribute['attribute_id'])
                    ->where('v.store_id IN (?)', array(0, Mage::app()->getStore()->getId()));
                $this->_attributes[$code]['values'] = $this->_getReadAdapter()->fetchPairs($select);
            }
            if (isset($this->_attributes[$code]['values'][$value])) {
                return $core->labelToUrl($this->_attributes[$code]['values'][$value]);
            }
		}
        return $value;
	}
}
