<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterAdmin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */

/* BASED ON SNIPPET: Models/Observer */
/**
 * This class observes certain (defined in etc/config.xml) events in the whole system and provides public methods - handlers for
 * these events.
 * @author Mana Team
 *
 */
class ManaPro_FilterAdmin_Model_Filter_Observer {
	/* BASED ON SNIPPET: Models/Event handler */
	/**
	 * Updates form title (handles event "mana_admin_form_config")
	 * @param Varien_Event_Observer $observer
	 */
	public function updateEavForms($observer) {
		/* @var $form Mage_Core_Model_Config_Base */ $form = $observer->getEvent()->getForm();
		/* @var $model Mana_Filters_Model_Filter */ $model = $observer->getEvent()->getModel();
		/* @var $result ArrayObject */ $result = $observer->getEvent()->getResult();
		if ($form->getName() == 'filter' && $model instanceof Mana_Filters_Model_Filter) {
			// update title
			$xml = '<'.$form->getName().'><title><filters><title>';
			$xml .= Mage::helper('manapro_filteradmin')->__('%s - Layered Navigation Filter', $model->getName());
			$xml .= '</title></filters></title>';
			$xml .= '</'.$form->getName().'>';
			$result->append($xml);
			
			// update display options
			$xml = '<'.$form->getName().'><fields><display><options>mana_filters/source_display_'.$model->getType().'</options></display></fields></'.$form->getName().'>';
			$result->append($xml);
		}
	}
	
}