<?php
/**
 * @category    Mana
 * @package     ManaPro_FilterAdmin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */

/**
 * This controller contains actions for managing layered navigation filters
 * @author Mana Team
 *
 */
class ManaPro_FilterAdmin_Mana_FiltersController  extends Mana_Admin_Controller_Eav {
    protected function _construct() {
		$this->setGridName('filter')->setFormName('filter')->setEavName('mana_filters/filter');
    }
}