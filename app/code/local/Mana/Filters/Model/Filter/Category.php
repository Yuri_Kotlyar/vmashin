<?php
/**
 * @category    Mana
 * @package     Mana_Filters
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Model type for holding information in memory about possible or applied category filter
 * @author Mana Team
 * Injected instead of standard catalog/layer_filter_attribute in Mana_Filters_Block_Filter_Category constructor.
 */
class Mana_Filters_Model_Filter_Category extends Mage_Catalog_Model_Layer_Filter_Category {
    /**
     * Apply category filter to product collection
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mana_Filters_Block_Filter_Category $filterBlock
     * @return  Mana_Filters_Block_Filter_Category
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     * @see app/code/core/Mage/Catalog/Model/Layer/Filter/Mage_Catalog_Model_Layer_Filter_Category::apply()
     */
	public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filterValues = $this->getMSelectedValues();

        if (empty($filterValues)) {
            return $this;
        }

        /** @var $multiple SLab_CategoryAsStyle_Model_Multiple */
        $multiple = Mage::getModel('categoryAsStyle/multiple');

        $collection = $this->getLayer()->getProductCollection();

        foreach ($filterValues as $filter) {
            $this->_categoryId = $filter;

//            $category   = $this->getCategory();
//            Mage::register('current_category_filter', $category);

            $this->_appliedCategory = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($filter);

            if ($this->_isValidCategory($this->_appliedCategory)) {
                $multiple->addCategoryFilter($collection, $this->_appliedCategory);
//                $collection->addCategoryFilter($this->_appliedCategory);

                $this->getLayer()->getState()->addFilter($this->_createItemEx(array(
                    'label' => $this->_appliedCategory->getName(),
                    'value' => $filter,
                    'm_selected' => true,
                )));

//                $breadcrumbs = Mage::app()->getLayout()->getBlock('breadcrumbs');
//                $breadcrumbs->addCrumb($this->_appliedCategory->getUrlKey(), array('label'=>$this->_appliedCategory->getName(), 'title'=>$this->_appliedCategory->getName(), 'link'=>''));

//                $_SESSION['current_cat'] = $this->_appliedCategory->getName();
//                $_SESSION['current_cat_id'] = $this->_appliedCategory->getId();
            }
        }
        return $this;
    }

	/**
     * Returns all values currently selected for this filter
     *
     * @return array
     */
    public function getMSelectedValues() {
    	$values = Mage::app()->getRequest()->getParam($this->_requestVar);
		return $values ? explode('_', $values) : array();
    }

    /**
     * Creates in-memory representation of a single option of a filter
     * @param array $data
     * @return Mana_Filters_Model_Item
     * This method is cloned from method _createItem() in parent class (method body was pasted from parent class 
     * completely rewritten.
     * Standard method did not give us possibility to initialize non-standard fields. 
     */
    protected function _createItemEx($data)
    {
        return Mage::getModel('mana_filters/item')
            ->setData($data)
            ->setFilter($this);
    }
    /** 
     * Initializes internal array of in-memory representations of options of a filter
     * @return Mana_Filters_Model_Filter_Attribute
     * @see Mage_Catalog_Model_Layer_Filter_Abstract::_initItems()
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     */
    protected function _initItems()
    {
        $data = $this->_getItemsData();
        $items=array();
        foreach ($data as $_key => $itemData) {
        	// MANA BEGIN
            $items[$_key] = $this->_createItemEx($itemData);
            // MANA END
        }
        // MANA BEGIN: enable additional filter item processing
    	/* @var $ext Mana_Filters_Helper_Extended */ $ext = Mage::helper(strtolower('Mana_Filters/Extended'));
        $items = $ext->processFilterItems($this, $items);
        // MANA END
        $this->_items = $items;
        return $this;
    }
    protected function _getCategoryItemsData($category, $products)
    {
        $categories = $this->_getChildrenCategories($category->getParentCategory());

        $products->addCountToCategories($categories);

        $_coreHelper = Mage::helper('core');
        $data = array();
        foreach ($categories as $category) {
        	if ($category->getIsActive() && $category->getProductCount()) {
                $name = $_coreHelper->escapeHtml($category->getName());
            	$data[strtolower($name)] = array(
                	'label' => $name,
                    'value' => $category->getId(),
                    'count' => $category->getProductCount(),
            		'm_selected' => in_array($category->getId(), $this->getMSelectedValues())
                );
            }
        }
        return $data;
    }
    protected function _getItemsData()
    {
        $key = $this->getLayer()->getStateKey().'_SUBCATEGORIES';
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        if ($data === null) {
            $category = $this->getCategory();
            /** @var $category Mage_Catalog_Model_Categeory */
            $categories = $this->_getChildrenCategories($category->getParentCategory());

            $this->getLayer()->getProductCollection()
                ->addCountToCategories($categories);

            $data = array();
            foreach ($categories as $category) {
                if ($category->getIsActive() && $category->getProductCount()) {
                    $data[] = array(
                        'label' => Mage::helper('core')->htmlEscape($category->getName()),
                        'value' => $category->getId(),
                        'urlkey'=>Mage::helper('mana_core')->labelToUrl(Mage::helper('core')->htmlEscape($category->getName())),
                        'count' => $category->getProductCount(),
                    );
                }
            }
            $data = $this->_getCategoryItemsData($this->getCategory(), $this->getLayer()->getProductCollection());
//            if (!count($data)) { // no child categories having products
//            	$category = $this->getCategory()->getParentCategory();
//            	$products = clone ($this->getLayer()->getProductCollection());
//            	$products->addCategoryFilter($category);
//            	$data = $this->_getCategoryItemsData($category, $products);
//            }
            $tags = $this->getLayer()->getStateTags();
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }
        return $data;
    }

    /**
     * get children categories
     *
     * @param \Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Model_Category
     */
    protected function _getChildrenCategories(Mage_Catalog_Model_Category $category)
    {
        $collection = $category->getCollection();
        /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        $collection->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('all_children')
            ->addAttributeToSelect('is_anchor')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('use_as_style', 1)
            ->addIdFilter($category->getChildren())
            ->setOrder('position', 'ASC')
            ->joinUrlRewrite()
            ->load();
        return $collection;
    }

    public function getItemsData()
    {
        return $this->_getItemsData();
    }

    public function getName()
    {
        return 'Style';
    }

    /**
     * Get filter value for reset current filter state
     *
     * @return mixed
     */
    public function getResetValue()
    {
        return null;
    }
}
