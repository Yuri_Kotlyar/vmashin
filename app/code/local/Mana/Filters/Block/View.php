<?php
/**
 * @category    Mana
 * @package     Mana_Filters
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Block type for showing filters in category view pages.
 * @author Mana Team
 * Injected into layout instead of standard catalog/layer_view in layout XML file.
 */
class Mana_Filters_Block_View extends Mage_Catalog_Block_Layer_View {
	protected $_mode = 'category';

    /**
     * This method is called during page rendering to generate additional child blocks for this block.
     * @return Mana_Filters_Block_View_Category
     * This method is overridden by copying (method body was pasted from parent class and modified as needed). All
     * changes are marked with comments.
     * @see app/code/core/Mage/Catalog/Block/Layer/Mage_Catalog_Block_Layer_View::_prepareLayout()
     */
    protected function _prepareLayout()
    {
        $stateBlock = $this->getLayout()->createBlock('catalog/layer_state')
            ->setLayer($this->getLayer());
        $this->setChild('layer_state', $stateBlock);


     //   $cacheId = 'CATEGORY_FILTERS_OPTIONS';

      //  $cacheData = Mage::app()->getCache()->load($cacheId);

     //   if ($cacheData !== false) {

      //     $filtersOp = unserialize($cacheData);

//else {


        $filtersOp = Mage::helper('mana_filters')->getFilterOptionsCollection();




   // Mage::app()->getCache()->save(serialize($filtersOp), $cacheId);

//}
            
        foreach ($filtersOp as $filterOptions) {
        	$displayOptions = $filterOptions->getDisplayOptions();
        	$block = $this->getLayout()->createBlock((string)$displayOptions->block)->setLayer($this->getLayer());
            if ($attribute = $filterOptions->getAttribute()) {
        		$block->setAttributeModel($attribute);
            }
            $block->setFilterOptions($filterOptions)->setDisplayOptions($displayOptions)->setMode($this->_mode)->init();
            $this->setChild($filterOptions->getCode() . '_filter', $block);
        }

        $this->getLayer()->apply();

        return $this;
    }
    
    public function getFilters() {
        $filters = array();
    	foreach (Mage::helper('mana_filters')->getFilterOptionsCollection() as $filterOptions) {
            $filters[] = $this->getChild($filterOptions->getCode() . '_filter');
        }

        return $filters;
    }

    public function getManufacturers(){
        $manufacturerAttributes = Mage::helper('mana_filters')->getManufacturerAttributes();
        $currentAttributes = $this->getCurrentManufacturers();

        $currentAttributeCodes = array_intersect($manufacturerAttributes, $currentAttributes);

        /***GET ALL MANUFACTURERS AVAILABLE IN CURRENT CATEGORY*****/
        $manufacturerAttributes = array(
            'battery_manufacturer',
            'boat_manufacturer',
            'heli_manufacturer',
            'other_manufacturer',
            'manufacturer',
        );
        $productCollection = Mage::registry('current_category')->getProductCollection()
                ->addAttributeToSelect('battery_manufacturer')
                ->addAttributeToSelect('boat_manufacturer')
                ->addAttributeToSelect('heli_manufacturer')
                ->addAttributeToSelect('other_manufacturer')
                ->addAttributeToSelect('manufacturer')
                ->addAttributeToFilter('status', 1);

        $manufacturers = array();
        foreach($productCollection as $_product){
            foreach ($manufacturerAttributes as $manAttr){
                if ($_product->getData($manAttr)){
                    $manufacturers[]= $_product->getResource()->getAttribute($manAttr)->getFrontend()->getValue($_product);
                }
            }
        }
        $manufacturersUnique = array_unique($manufacturers);
        /***********************************************************/

        $result = array();
        foreach ($currentAttributeCodes as $attributeCode) {
            $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attributeCode);


            if ($attribute->usesSource()) {
                $resultTe = $attribute->getSource()->getAllOptions(false);

                foreach ($resultTe as $key => $option) {
                    $resultTe[$key]['value'] = $attributeCode . ':' . Mage::helper('mana_core')->labelToUrl($option['label']);
                    if( ! in_array($option['label'], $manufacturersUnique)){
                        unset($resultTe[$key]);
                    }
                }
                $result = array_merge($result, $resultTe);
            }
        }

/*old*/
//        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'manufacturer');
//
//
//        if ($attribute->usesSource()) {
//            $result = $attribute->getSource()->getAllOptions(false);
//        } else {
//            return false;
//        }
//
//        foreach ($result as $key=>$option){
//            $result[$key]['value'] = Mage::helper('mana_core')->labelToUrl($option['label']);
//        }
        return $result;
    }

    public function getSelectedManufacturer(){
//        $manufacturerAttributes = $this->getManufacturerAttributes();
//        $currentUrl = $this->helper('core/url')->getCurrentUrl();
//        $manufacturerSelected = '';
//        foreach ($manufacturerAttributes as $manufacturerCod) {
//            $manufacturerMatch = array();
//            preg_match('/\/'.$manufacturerCod.'\/([^\/\.]+)/', $currentUrl, $manufacturerMatch);
//            if (isset($manufacturerMatch[1])){
//                $manufacturerSelected = $manufacturerMatch[1];
//            }
//        }
//        return $manufacturerSelected;

        $manufacturerAttributes = $this->getManufacturerAttributes();
        $currentUrl = $this->helper('core/url')->getCurrentUrl();
        foreach ($manufacturerAttributes as $manufacturerCod) {
            $manufacturerCod = strtr($manufacturerCod,array(
                '_'=>'-'
            ));
            $manufacturerMatch = array();
            preg_match('/\/'.$manufacturerCod.'\/([^\/\.]+)/', $currentUrl, $manufacturerMatch);
            if (isset($manufacturerMatch[1])){
                return $manufacturerMatch[1];
            }
        }
        return '';
    }

    public function getSelectedManufacturerAndCod(){
        $manufacturerAttributes = $this->getManufacturerAttributes();
        $currentUrl = $this->helper('core/url')->getCurrentUrl();

        foreach ($manufacturerAttributes as $manufacturerCod) {
            $manufacturerCod = strtr($manufacturerCod,array(
                '_'=>'-'
            ));
            $manufacturerMatch = array();
            preg_match('/\/'.$manufacturerCod.'\/([^\/\.]+)/', $currentUrl, $manufacturerMatch); 
            if (isset($manufacturerMatch[1])){
                return $manufacturerCod.'/'.$manufacturerMatch[1];
            }
        }
        return '';
    }

    public function getCurrentManufacturers() {
        $filters = array();
        $attributeCodes = array();
    	foreach (Mage::helper('mana_filters')->getFilterOptionsCollection() as $filterOptions) {
		 
            $filters[] = $this->getChild($filterOptions->getCode() . '_filter');
        }

        foreach ($filters as $filter) {
            $attributeCodes[] =  $filter->getFilterOptions()->getCode();
        }
        return $attributeCodes;
    }

    public function getManufacturerAttributes(){
        return  Mage::helper('mana_filters')->getManufacturerAttributes();
    }

}
