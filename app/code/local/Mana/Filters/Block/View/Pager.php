<?php 
/**
 * SLab extension for Magento
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Mana Filters module to newer versions in the future.
 * If you wish to customize the Mana Filters module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mana
 * @package    Mana_Filters
 * @copyright  Copyright (C) 2012 SLab
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Pager for product list with mana layered navigation
 *
 * @category   Mana
 * @package    Mana_Filters
 * @subpackage Block
 * @author     Vladimir Fishchenko <vladimir.fishchenko@gmail.com>
 */
class Mana_Filters_Block_View_Pager extends Mage_Page_Block_Html_Pager
{
    /**
     * get pager url
     *
     * @param array $params
     * @return string
     */
    public function getPagerUrl($params=array())
    {
        if ($params['p'] == 1) {
            $params['p'] = null;
        }

        $urlParams = array();
        $urlParams['_current']  = true;
        $urlParams['_escape']   = true;
        $urlParams['_use_rewrite']   = true;
        $urlParams['_query']    = $params;
        return $this->helper('mana_filters/extended')->getFilterUrl('*/*/*', $urlParams);
    }
}
