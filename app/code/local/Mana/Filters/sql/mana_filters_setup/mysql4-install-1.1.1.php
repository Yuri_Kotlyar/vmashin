<?php
/**
 * @category    Mana
 * @package     Mana_Filters
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/* BASED ON SNIPPET: Resources/Install/upgrade script */
/* @var $installer Mana_Filters_Resource_Setup */
$installer = $this;

$installer->startSetup();
$table = 'mana_filters/filter';
$installer->installEntities();
$installer->createEntityTables($table);

/* BASED ON SNIPPET: Resources/Table creation/alteration script */

$installer->getConnection()->dropForeignKey($installer->getTable($table), 'FK_m_filter_store');
$installer->getConnection()->dropColumn($installer->getTable($table), 'store_id');
$installer->getConnection()->dropColumn($installer->getTable($table), 'increment_id');

$installer->run("
	ALTER TABLE `{$this->getTable($table)}` ADD COLUMN (
		`code` varchar(255) NOT NULL default ''
	);
	ALTER TABLE `{$this->getTable($table)}` ADD KEY `code` (`code`);
");
$installer->updateDefaultMaskFields(Mana_Filters_Model_Filter::ENTITY);

$installer->endSetup();