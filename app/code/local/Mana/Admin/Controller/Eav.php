<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * This is base class for actions rendering grids and editing pages for data accessed via resources based on 
 * Mana_Core_Resource_Eav
 * @author Mana Team
 *
 */
class Mana_Admin_Controller_Eav extends Mage_Adminhtml_Controller_Action {
	protected $_gridName;
	/**
	 * Returns name of current grid configuration used to retrieve grid settings from grid.xml. Typically set in
	 * overwritten _construct() method. 
	 * @throws Exception
	 */
	public function getGridName() { 
		if (!$this->_gridName) throw new Exception('Grid name matching configuration in grids.xml files should be set.');
		return $this->_gridName; 
	}
	/**
	 * Sets name of current grid configuration used to retrieve grid settings from grid.xml. Typically set in
	 * overwritten _construct() method. 
	 * @param string $value
	 * @return Mana_Admin_Controller_Eav
	 */
	public function setGridName($value) { 
		$this->_gridName = $value; 
		return $this; 
	}

	protected $_formName;
	/**
	 * Returns name of current form configuration used to retrieve form settings from forms.xml. Typically set in
	 * overwritten _construct() method. 
	 * @throws Exception
	 */
	public function getFormName() { 
		if (!$this->_formName) throw new Exception('Form name matching configuration in forms.xml files should be set.');
		return $this->_formName; 
	}
	/**
	 * Sets name of current form configuration used to retrieve form settings from forms.xml. Typically set in
	 * overwritten _construct() method. 
	 * @param string $value
	 * @return Mana_Admin_Controller_Eav
	 */
	public function setFormName($value) { 
		$this->_formName = $value; 
		return $this; 
	}

	protected $_eavName;
	/**
	 * Returns name of current eav entity entity used to retrieve eav entity settings from eav_attribute and
	 * related tables. Typically set in overwritten _construct() method. 
	 * @throws Exception
	 */
	public function getEavName() { 
		if (!$this->_eavName) throw new Exception('Valid entity type name should be set.');
		return $this->_eavName; 
	}
	/**
	 * Sets name of current eav entity entity used to retrieve eav entity settings from eav_attribute and
	 * related tables. Typically set in overwritten _construct() method. 
	 * @param string $value
	 * @return Mana_Admin_Controller_Eav
	 */
	public function setEavName($value) { 
		$this->_eavName = $value; 
		return $this; 
	}
	
	/**
	 * Full page rendering action displaying list of entities of certain type. 
	 */
	public function indexAction() {
		/* @var $core Mana_Core_Helper_Data */
        $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Grid_Config */
        $config = Mage::getSingleton(strtolower('Mana_Admin/Grid_Config'));

		$grid = $config->getGrid($this->getGridName());
		
		foreach ($core->getSortedXmlChildren($grid, 'title', 'title') as $title) {
			$this->_title($title);
		}

        $this->loadLayout('mana_admin');
        
        $containerBlock = isset($grid->rewrite->block->container) ? (string) $grid->rewrite->block->container : 'mana_admin/grid';
        $this->getLayout()->getBlock('content')->insert( 
        	$this->getLayout()->createBlock($containerBlock, 'grid.container', array(
        		'grid_name' => $this->getGridName(),
        		'eav_name' => $this->getEavName(),
        	))
        );
        
        $this->_setActiveMenu((string) $grid->active_menu);
        
        $this->renderLayout();
	}
	
	protected function _getKeyParams() {
        /** @var $filter Mana_Filters_Resource_Filter */
        $filter = Mage::getResourceModel($this->getEavName());
		$keyAttributes = $filter->getKeyAttributes()->load();
		if ($keyAttributes->count() > 0) {
			$key = null;
			$keyField = null;
			foreach ($keyAttributes as $keyAttribute) {
				if (!$keyField) {
					$keyField = $keyAttribute->getAttributeCode();
					$key = $this->getRequest()->getParam($keyField);
				}
				else {
					throw new Exception(sprintf('Multiple keys not supported (entity %s)', $this->getEavName()));
				}
			}
			return array($keyField, $key);
		}
		else {
			return array('id', isset($params['id']) ? $params['id'] : null);
		}
	}
	public function editAction() {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		$model = Mage::getModel($this->getEavName());
		$model->setStoreId($this->getRequest()->getParam('store', 0));
		list($key, $value) = $this->_getKeyParams();
		if ($key == 'id') { 
			$model->load($value); 
		} 
		else { 
			$method = 'set'.$core->pascalCased($key);
			$model->$method($value);
			$model = $model->loadByAttribute($key, $value); 
		}
		
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		$form = $config->getForm($this->getFormName(), $model);
		
		foreach ($core->getSortedXmlChildren($form, 'title', 'title') as $title) {
			$this->_title($title);
		}

        $this->loadLayout('mana_admin');
        
        $containerBlock = isset($form->rewrite->block->container) ? (string) $form->rewrite->block->container : 'mana_admin/form';
        $this->getLayout()->getBlock('content')->insert( 
        	$this->getLayout()->createBlock($containerBlock, 'form.container', array(
        		'form_name' => $this->getFormName(),
        		'eav_name' => $this->getEavName(),
        		'model' => $model,
        	))
        );
        
        $this->_setActiveMenu((string) $form->active_menu);
        
        $this->renderLayout();
	}
	
	public function saveAction() {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		$storeId = $this->getRequest()->getParam('store', 0);
		$fields = $this->getRequest()->getPost('fields');
        $useDefault = $this->getRequest()->getPost('use_default');
		
		/* @var $model Mana_Core_Model_Eav */ $model = Mage::getModel($this->getEavName());
		$model->setStoreId($storeId);
        
		list($key, $keyValue) = $this->_getKeyParams();
		if ($key == 'id') {
			$model->load($keyValue); 
		} 
		else {
			$method = 'set'.$core->pascalCased($key);
			$model->$method($keyValue);
			$model = $model->loadByAttribute($key, $keyValue); 
		}
		
        $data = array();
        $attributes = Mage::getResourceModel($this->getEavName())->loadAllAttributes()->getAttributesByCode();
        foreach ($useDefault as $code) {
	        if (!$storeId) {
	        	// clear bit in default mask
	        	if ($attributes[$code]->getHasDefault()) {
		        	$model->setData($attributes[$code]->getDefaultMaskField(), 
		        		((int)$model->getData($attributes[$code]->getDefaultMaskField())) & ~((int)$attributes[$code]->getDefaultMask()));
	        	}
	        }
	        else {
	        	$model->setData($code, null); // this will force to delete store specific value from database
	        }
        }
        if (!empty($fields)) {
            foreach ($fields as $code => $value) {
                $data[$code] = $value;
                if (!$storeId) {
                    // set bit in default mask
                    if ($attributes[$code]->getHasDefault()) {
                        $model->setData($attributes[$code]->getDefaultMaskField(),
                            ((int)$model->getData($attributes[$code]->getDefaultMaskField())) | ((int)$attributes[$code]->getDefaultMask()));
                    }
                }
            }
        }
        $model->addData($data);
        
        $response = new Varien_Object();
        $update = array();
        /* @var $messages Mage_Adminhtml_Block_Messages */ $messages = $this->getLayout()->createBlock('adminhtml/messages');

        try {
        	// validate
            if (!empty($fields)) {
                foreach ($fields as $code => $value) {
                    if ($attributes[$code]->getIsRequired() && (trim($value) === '' || $attributes[$code]->isValueEmpty($value))) {
                        throw new Mage_Core_Exception($this->__('Please fill in %s field', $attributes[$code]->getFrontendLabel()));
                    }
                }
            }

        	// do save
        	$model->save();
        	$messages->addSuccess($this->__('Your changes are successfully saved.'));
        }
        catch (Exception $e) {
        	$messages->addError($e->getMessage());
        	$response->setError(true);
        }
        
        $update[] = array('selector' => '#messages', 'html' => $messages->getGroupedHtml());
        $response->setUpdate($update);
        $this->getResponse()->setBody($response->toJson());
	}

	public function gridAction() {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Grid_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Grid_Config'));
		$grid = $config->getGrid($this->getGridName());
        $gridBlock = isset($grid->rewrite->block->grid) ? (string) $grid->rewrite->block->grid : 'mana_admin/grid_widget';
        
		$this->loadLayout();
        $this->getResponse()->setBody(
			$this->getLayout()->createBlock($gridBlock, 'entity.grid', array(
	        		'grid_name' => $this->getGridName(),
	        		'eav_name' => $this->getEavName(),
	        ))
            ->toHtml());
	}
}