<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Used in XML file processing to resolve URLs
 * @author Mana Team
 *
 */
class Mana_Admin_Model_Converter_Url {
	public function evaluate($xml) {
		$params = array();
		if (isset($xml->params)) {
			foreach ($xml->params->children() as $key => $param) {
				if (isset($param['from'])) {
					if (((string)$param['from']) == '_current') {
						$params[$key] = Mage::app()->getRequest()->getParam($key);
					}
					else {
						throw new Exception('Not implemented');
					}
				}
				else {
					$params[$key] = (string) $param;
				}
			}
		}
		return Mage::getModel('adminhtml/url')->getUrl((string)$xml->value, $params);
	}
}