<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Singleton class for accessing forms.xml configuration data files
 * @author Mana Team
 *
 */
class Mana_Admin_Model_Form_Config {
	/**
	 * Enter description here ...
	 * @var Mage_Core_Model_Config_Base
	 */
	protected $_config;
	/**
	 * Lazily reads form configuration from files and marge them Magento style 
	 * @return Mage_Core_Model_Config_Base
	 */
	protected function _getConfig() {
		if (!$this->_config) {
			$this->_config = Mage::getConfig()->loadModulesConfiguration('forms.xml');
		}
		return $this->_config;
	}
	/**
	 * Returns merged form configuration by its name
	 * @param string $name
	 * @return Varien_Simplexml_Element | boolean
	 */
	protected $_parts; 
	public function getForm($name, $model) {
		if (!$this->_parts) $this->_parts = array();
		if (!isset($this->_parts[$name])) {
			/*@var $config Mage_Core_Model_Config_Base */ $config = $this->_getConfig();
			/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
			
			// get static data from all XML files
			$result = $config->getNode($name);
			
			// translate captions to current locale
			$result = $core->translateConfig($result);
			
			// get dynamic data from all observers
			$wrapper = new ArrayObject(array());
			Mage::dispatchEvent('mana_admin_form_config', array('form' => $result, 'model' => $model, 'result' => $wrapper));
			$result = $core->mergeConfig($result, $wrapper);
			
			$this->_parts[$name] = $result;
		}
		return $this->_parts[$name];
	}
}