<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* BASED ON SNIPPET: Models/Observer */
/**
 * This class observes certain (defined in etc/config.xml) events in the whole system and provides public methods - handlers for
 * these events.
 * @author Mana Team
 *
 */
class Mana_Admin_Model_Form_Observer {
	/* BASED ON SNIPPET: Models/Event handler */
	/**
	 * Provides default group and section definitions (handles event "mana_admin_form_config")
	 * @param Varien_Event_Observer $observer
	 */
	public function getDefaultFormConfig($observer) {
		/* @var $form Mage_Core_Model_Config_Base */ $form = $observer->getEvent()->getForm();
		/* @var $result ArrayObject */ $result = $observer->getEvent()->getResult();
		
		$this->updateGeneralDescriptions($form, $result);
	}
	
	public function updateGeneralDescriptions($form, $result) {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		$xml = '<'.$form->getName().'>';
		
		$xml .= '<groups>';
		foreach ($form->groups->children() as $name => $group) {
			if ($name == 'general') {
				$xml .= '<'.$name.'>';
				$xml .= $this->getGeneralGroupConfig($group);
				$xml .= '</'.$name.'>';
			}
		}
		$xml .= '</groups>';
		
		$xml .= '<sections>';
		foreach ($form->sections->children() as $name => $section) {
			if ($name == 'general') {
				$xml .= '<'.$name.'>';
				$xml .= $this->getGeneralSectionConfig($section);
				$xml .= '</'.$name.'>';
			}
		}
		$xml .= '</sections>';
		
		$xml .= '<fieldsets>';
		foreach ($form->fieldsets->children() as $name => $fieldset) {
			if ($name == 'general') {
				$xml .= '<'.$name.'>';
				$xml .= $this->getGeneralFieldsetConfig($fieldset);
				$xml .= '</'.$name.'>';
			}
		}
		$xml .= '</fieldsets>';

		$xml .= '</'.$form->getName().'>';
		$result->append($xml);
	}
	public function getGeneralFieldsetConfig($fieldset) {
		$result = '';
		if (!isset($fieldset->title)) $result .= '<title>'.Mage::helper('mana_admin')->__('General Information').'</title>';
		if (!isset($fieldset->sort_order)) $result .= '<sort_order>100</sort_order>';
		return $result;
	}
	public function getGeneralSectionConfig($section) {
		$result = '';
		if (!isset($section->title)) $result .= '<title>'.Mage::helper('mana_admin')->__('General').'</title>';
		if (!isset($section->sort_order)) $result .= '<sort_order>100</sort_order>';
		return $result;
	}
	public function getGeneralGroupConfig($group) {
		$result = '';
		if (!isset($group->title)) $result .= '<title>'.Mage::helper('mana_admin')->__('General').'</title>';
		if (!isset($group->sort_order)) $result .= '<sort_order>100</sort_order>';
		return $result;
	}
	
}