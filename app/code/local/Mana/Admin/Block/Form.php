<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * This one occupies whole content area of the form page and directly manipulates form caption and buttons.
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Form extends Mage_Adminhtml_Block_Widget_Form_Container {
	public function __construct() {
		//parent::__construct();
		
		// parent constructor does not pass arguments to Varien_Obejct constructor, so DIY
        $args = func_get_args();
		if (!empty($args[0])) {
        	$this->_data = $args[0];
        }
        
		if (!$this->hasData('template')) {
            $this->setTemplate('widget/form/container.phtml');
        }
		// set up save button
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		$form = $config->getForm($this->getFormName(), $this->getModel());
		$this->setData('form_action_url', $this->getUrl((string) $form->action_url));
	}
    public function getHeaderText()
    {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		$form = $config->getForm($this->getFormName(), $this->getModel());
		$titles = $core->getSortedXmlChildren($form, 'title', 'title');
		return array_pop($titles);
    }
	public function getHeaderCssClass()
    {
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		$form = $config->getForm($this->getFormName(), $this->getModel());
    	return (string)$form->title_css;
    }
	protected function _prepareLayout() {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		/* @var $form Mage_Core_Model_Config_Element */ $form = $config->getForm($this->getFormName(), $this->getModel());
		/* @var $js Mana_Core_Helper_Js */ $js = Mage::helper(strtolower('Mana_Core/Js'));
		// add buttons
		foreach ($core->getSortedXmlChildren($form, 'buttons') as $key => $button) {
			$options = array(
	            'label'   => (string) $button->title,
	            'class'   => (string) $button->css,
	        );
	        if (isset($button->js_options)) {
	        	$selector = (string) $button->js_options->selector;
	        	$jsOptions = array();
	        	foreach ($button->js_options->children() as $jsOptionName => $jsOption) {
	        		if ($jsOptionName == 'selector') continue;
	        		
	        		if (!empty($jsOption->converter) && ($converter = Mage::getSingleton((string)$jsOption->converter))) {
	        			$jsOptions[$jsOptionName] = $converter->evaluate($jsOption);
	        		}
	        		elseif( isset($jsOption->value)) {
	        			$jsOptions[$jsOptionName] = (string)$jsOption->value;
	        		}
	        	}
	        	$js->options($selector, $jsOptions);
	        }
	        if (isset($button->action) && ($action = Mage::getSingleton((string) $button->action))) {
	        	$action->renderOptions($form, $button);
	        }
			$this->_addButton($key, $options);
		}
        foreach ($this->_buttons as $level => $buttons) {
            foreach ($buttons as $id => $data) {
                $childId = $this->_prepareButtonBlockId($id);
                $this->_addButtonChildBlock($childId);
            }
        }
		
		// add store switcher to the left
		if( !Mage::app()->isSingleStoreMode() ) {
			$this->getLayout()->getBlock('left')->insert( 
        		$this->getLayout()->createBlock('adminhtml/store_switcher', 'store_switcher')
        	);	
		}
		
		// include block showing all groups if there is more than one
		if ($core->countXmlChildren($form->groups) > 1) {
			$groupsBlock = $this->getLayout()->createBlock('mana_admin/form_groups', 'groups', array(
        		'form_name' => $this->getFormName(),
        		'eav_name' => $this->getEavName(),
        		'model' => $this->getModel(),
        	));
			$this->setChild('form', $groupsBlock);
			$group = $groupsBlock->getCurrentGroup();
			throw new Exception('Not implemented');
		}
		else {
			$group = null;
			foreach ($form->groups->children() as $group) { break; }
			if (!$group) {
				throw new Exception(sprintf('No groups defined in form %s.', $this->getFormName()));
			}
		}
		
		// include block showing all section of current group if there is more than one
		if ($core->countXmlChildren($form->sections, array('group' => $group->getName())) > 1) {
			throw new Exception('Not implemented');
		}
		else {
			$section = null;
			foreach ($core->getSortedXmlChildren($form, 'sections', '', array('group' => $group->getName())) as $section) { break; }
			if (!$section) {
				throw new Exception(sprintf('No groups defined in form %s.', $this->getFormName()));
			}
		}
		
		// include block showing all fieldsets and fields of current section
        $formBlock = isset($section->block->type) ? (string) $section->block->type : 'mana_admin/form_fieldsets';
		$this->setChild('form', 
        	$this->getLayout()->createBlock($formBlock, 'form', array(
        		'form_name' => $this->getFormName(),
        		'group' => $group,
        		'section' => $section,
        		'eav_name' => $this->getEavName(),
        		'model' => $this->getModel(),
        	))
        );
		
		return $this;
    }
    public function getFormHtml()
    {
        $this->getChild('form')->setData('action', $this->getSaveUrl());
        return $this->getChildHtml('sections').$this->getChildHtml('form');
    }
}