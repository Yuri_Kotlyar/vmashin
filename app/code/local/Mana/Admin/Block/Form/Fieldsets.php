<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * This block displays section consisting of one or more fieldsets of current form
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Form_Fieldsets extends Mage_Adminhtml_Block_Widget_Form {
	/** 
	 * Adds fieldsets and fields to be rendered
	 * @see Mage_Adminhtml_Block_Widget_Form::_prepareForm()
	 */
	protected function _prepareForm() {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Form_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Form_Config'));
		/* @var $js Mana_Core_Helper_Js */ $js = Mage::helper(strtolower('Mana_Core/Js'));
		
		$form = $config->getForm($this->getFormName(), $this->getModel());
		$group = $this->getGroup();
		$section = $this->getSection();
		
		$uiform = new Varien_Data_Form();
		$htmlId = 'mf_'.$group->getName().'_'.$section->getName();
        $uiform
        	->setModel($this->getModel())
        	->setHtmlIdPrefix($htmlId.'_')
        	->setUseContainer(true)
        	->setMethod('post')
        	//->setAction($this->getUrl('*/*/save', array('_current' => true)))
        	->setId($htmlId)
        	->setFieldNameSuffix('fields');
		$js->options('edit-form', array('subforms' => array('#'.$htmlId)));
		
        foreach ($core->getSortedXmlChildren($form, 'fieldsets', '', array('section' => $section->getName())) as $fieldsetName => $fieldset) {
        	$options = array();
        	foreach ($fieldset->children() as $key => $option) {
        		$options[$key] = (string)$option;
        	}
        	if (!isset($options['legend']) && isset($options['title'])) $options['legend'] = $options['title'];
        	$renderer = isset($options['block']) ? $options['block'] : 'mana_admin/form_fields';
		    $uifieldset = $uiform->addFieldset('mfs_'.$fieldsetName, $options)
		    	->setRenderer($this->getLayout()->getBlockSingleton($renderer));
		    
		    $resource = Mage::getResourceModel($this->getEavName())->loadAllAttributes();
		    $attributes = $resource->getAttributesByCode();
		    
	        foreach ($core->getSortedXmlChildren($form, 'fields', '', array('fieldset' => $fieldset->getName())) as $fieldName => $field) {
	        	
	        	$options = array(
	        		'name'      		=> $fieldName,
		            'label'     		=> $core->__($attributes[$fieldName]->getFrontendLabel()),
		            'title'     		=> $core->__($attributes[$fieldName]->getFrontendLabel()),
		            'required'  		=> $attributes[$fieldName]->getIsRequired(),
		            'note'      		=> $core->__($attributes[$fieldName]->getNote()),
	        		'type'				=> $core->__($attributes[$fieldName]->getFrontendInput()),
	        		'entity_attribute'	=> $attributes[$fieldName],
	        	);
	        	if (isset($field->options)) {
	        		$options['options'] = Mage::getSingleton((string)$field->options)->getOptionArray();
	        	}
	        	elseif ($sourceModel = $attributes[$fieldName]->getSourceModel()) {
	        		$options['options'] = Mage::getSingleton($sourceModel)->getOptionArray();
	        	}
	        	foreach ($field->children() as $key => $option) {
	        		if ($key == 'options') continue;
	        		$options[$key] = (string)$option;
	        	}
        		$renderer = isset($options['block']) ? $options['block'] : 'mana_admin/form_field';
	        	$uifield = $uifieldset->addField($fieldName, $options['type'], $options)
		    		->setRenderer($this->getLayout()->getBlockSingleton($renderer));
	        }
        }
        
        $this->setForm($uiform);
        return parent::_prepareForm();
	}
	
	protected function _initFormValues() {
		$this->getForm()->setValues($this->getModel()->getData());
		return parent::_initFormValues();
	}
}