<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * FOR FUTURE USE: This block displays one or more fieldsets of field sections as horisontal tabs
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Form_Sections extends Mage_Adminhtml_Block_Widget_Tabs {
}