<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Renders one fieldset field including label, field itself and 'use default checkbox'
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Form_Field extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element {
    protected function _construct()
    {
        $this->setTemplate('mana/admin/field.phtml');
    }
	
    protected $_defaults = array();
    protected function _prepareDefaults() {
    	if (!isset($this->_defaults[$this->getAttributeCode()])) {
        	$attribute = $this->getAttribute();
        	$defaults = new Varien_Object();
    		if ($attribute->getIsGlobal() == Mana_Core_Model_Attribute_Scope::_STORE && $this->getModel()->getStoreId()) {
				// currently displayed value is for specific store and it may be inherited from all stores
				$defaults->setDisplayUseDefault(true);
				$defaults->setUseDefaultEnabled(true);
				$defaults->setDefaultLabel($this->__('Same As For All Stores'));
				$defaults->setUsedDefault(!$this->getModel()->isStoreValue($attribute));
    		}
	        elseif ($this->getModel()->getStoreId() && $attribute->getHasDefault() ) {
				// currently displayed value is for all stores and it may be inherited from some external source
				// and that can not be changes as some specific store is chosen
				$defaults->setDisplayUseDefault(true);
				$defaults->setUseDefaultEnabled(false);
    			$defaultProvider = Mage::getSingleton($attribute->getDefaultModel());
				$defaults->setDefaultLabel($defaultProvider->getUseDefaultLabel());
				$defaults->setUsedDefault($this->getModel()->isDefaultValue($attribute));
	        }
	        elseif ($attribute->getHasDefault() ) {
				// currently displayed value is for all stores and it may be inherited from some external source
				$defaults->setDisplayUseDefault(true);
				$defaults->setUseDefaultEnabled(true);
    			$defaultProvider = Mage::getSingleton($attribute->getDefaultModel());
				$defaults->setDefaultLabel($defaultProvider->getUseDefaultLabel());
				$defaults->setUsedDefault($this->getModel()->isDefaultValue($attribute));
	        }
	        else {
				$defaults->setDisplayUseDefault(false);
	        }
	        $this->_defaults[$this->getAttributeCode()] = $defaults;
    	}
    	return $this;
    }
    public function getDefaultLabel() {
    	$this->_prepareDefaults();
    	return $this->_defaults[$this->getAttributeCode()]->getDefaultLabel();
	}
	public function getUsedDefault() {
    	$this->_prepareDefaults();
    	return $this->_defaults[$this->getAttributeCode()]->getUsedDefault();
	}
    public function checkFieldDisable()
    {
        if ($this->getDisplayUseDefault() && $this->getUsedDefault()) {
            $this->getElement()->setDisabled(true);
        }
        return $this;
    }
    public function getDisplayUseDefault()
    {
    	$this->_prepareDefaults();
    	return $this->_defaults[$this->getAttributeCode()]->getDisplayUseDefault();
    }
    public function getUseDefaultEnabled()
    {
    	$this->_prepareDefaults();
    	return $this->_defaults[$this->getAttributeCode()]->getUseDefaultEnabled();
    }
    public function getAttribute()
    {
        return $this->getElement()->getEntityAttribute();
    }

    /**
     * Retrieve associated attribute code
     *
     * @return string
     */
    public function getAttributeCode()
    {
        return $this->getAttribute()->getAttributeCode();
    }
    public function getModel()
    {
        return $this->getElement()->getForm()->getModel();
    }
}