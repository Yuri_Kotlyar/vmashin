<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contains grid of all entities of specific type. Also hosts additional widgets such as  store
 * switcher and buttons. All the functionality based on configuration in grids.xml files. 
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Grid extends Mage_Adminhtml_Block_Widget_Container {
    /**
     * Set template
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('mana/admin/grid.phtml');
    }
	
    /**
     * Prepare button and grid
     *
     * @return Mage_Adminhtml_Block_Catalog_Product
     */
    protected function _prepareLayout()
    {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Grid_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Grid_Config'));
		$grid = $config->getGrid($this->getGridName());

		foreach ($core->getSortedXmlChildren($grid, 'buttons') as $key => $button) {
			$action = Mage::getSingleton((string) $button->action);
			$this->_addButton($key, array(
	            'label'   => (string) $button->title,
	            'onclick' => $action->getInvocationCode($button->args),
	            'class'   => (string) $button->css,
	        ));
		}

        $gridBlock = isset($grid->rewrite->block->grid) ? (string) $grid->rewrite->block->grid : 'mana_admin/grid_widget';
		$this->setChild('entity.grid', 
        	$this->getLayout()->createBlock($gridBlock, 'entity.grid', array(
        		'grid_name' => $this->getGridName(),
        		'eav_name' => $this->getEavName(),
        	))
        );
        $this->setChild('store_switcher', 
        		$this->getLayout()->createBlock('adminhtml/store_switcher', 'store_switcher'));
        return parent::_prepareLayout();
    }
}