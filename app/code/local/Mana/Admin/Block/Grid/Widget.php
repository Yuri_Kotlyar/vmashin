<?php
/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Grid widget for showing all entities of specific type. All the functionality based on 
 * configuration in grids.xml files. 
 * @author Mana Team
 *
 */
class Mana_Admin_Block_Grid_Widget extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        //$this->setVarNameFilter('product_filter');

    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _isVisible($column) {
		if (isset($column->visiblity_scope)) {
			$scope = (string)$column->visiblity_scope;
			if ($scope == Mana_Core_Model_Attribute_Scope::_STORE_TEXT) {
				$visible = $store->getId() != 0;
			}
			elseif ($scope == Mana_Core_Model_Attribute_Scope::_GLOBAL_TEXT) {
				$visible = $store->getId() == 0;
			}
			elseif ($scope == Mana_Core_Model_Attribute_Scope::_SINGLE_STORE_TEXT) {
				$visible = Mage::app()->isSingleStoreMode();
			}
			else {
				throw new Exception('Not implemented');
			}
		}
		else {
			$visible = true;
		}
		return $visible;
    }
    protected function _prepareCollection()
    {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Grid_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Grid_Config'));
		$grid = $config->getGrid($this->getGridName());

    	$store = $this->_getStore();
        $adminStoreId = Mage_Core_Model_App::ADMIN_STORE_ID;
    	
        /* @var $collection Mana_Core_Resource_Eav_Collection */
        if (isset($grid->rewrite->resource->collection)) {
        	$collection = Mage::getResourceModel((string)$grid->rewrite->resource->collection);
        }
        else {
        	$collection = Mage::getModel($this->getEavName())->getCollection();
        }

        if ($store->getId()) {
            $collection->addStoreFilter($store);
    	}
        
    	foreach ($core->getSortedXmlChildren($grid, 'columns') as $key => $column) {
			$alias = isset($column->alias) ? (string) $column->alias : $key;
			if ($this->_isVisible($column)) {
				if (isset($column->attribute)) {
	            	$collection->joinAttribute($alias, (string)$column->attribute, (string)$column->bind, 
						isset($column->condition) ? (string) $column->condition : null, 
						isset($column->join_type) ? (string) $column->join_type : 'inner',
	            		isset($column->value_scope) ? ((int)(string)$column->value_scope == Mana_Core_Model_Attribute_Scope::_STORE ? $store->getId() : $adminStoreId) : null);
				}
				elseif (isset($column->field)) {
					$collection->joinField($alias, (string)$column->table, (string)$column->field, (string)$column->bind, 
						isset($column->condition) ? (string) $column->condition : null, 
						isset($column->join_type) ? (string) $column->join_type : 'inner');
				}
				else {
					$collection->addAttributeToSelect($alias, 
						isset($column->join_type) ? (string) $column->join_type : false);
				}
			}
		}
		
		$collection->setOrder(
			isset($grid->order->column) ? ((string) $grid->order->column) : 'entity_id',
			isset($grid->order->direction) ? ((string) $grid->order->direction) : 'desc'
		);
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
		/* @var $config Mana_Admin_Model_Grid_Config */ $config = Mage::getSingleton(strtolower('Mana_Admin/Grid_Config'));
		$grid = $config->getGrid($this->getGridName());

    	$store = $this->_getStore();
    	
    	foreach ($core->getSortedXmlChildren($grid, 'columns') as $key => $column) {
			$id = isset($column->id) ? (string) $column->id : $key;
			if ($this->_isVisible($column)) {
				$options = array(
					'index' => $key,
					'header' => (string) $column->title,
				);
				if (isset($column->width)) $options['width'] = (string) $column->width;
				if (isset($column->type)) {
					$options['type'] = (string) $column->type;
					if ($options['type'] == 'price') {
						$options['currency_code'] = $store->getBaseCurrency()->getCode();
					}	
				}
				if (isset($column->options)) {
					$options['options'] = Mage::getSingleton((string) $column->options)->getOptionArray();
				}
				elseif (isset($column->collection)) {
					$collection = Mage::getResourceModel((string) $column->collection);
					// TODO: inject collection filters
					$options['options'] = $collection->load()->toOptionHash();
				}
				if (isset($column->align)) $options['align'] = (string) $column->align;
				if (isset($column->column_css)) $options['column_css_class'] = (string) $column->column_css;
				if (isset($column->editable)) $options['editable'] = (string) $column->editable;
				$this->addColumn($id, $options);
			}
		}
    }
    
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
		/* @var $core Mana_Core_Helper_Data */ $core = Mage::helper(strtolower('Mana_Core'));
    	$params = array('store'=>$this->getRequest()->getParam('store'));
		$keyAttributes = Mage::getResourceModel($this->getEavName())->getKeyAttributes()->load();
		if ($keyAttributes->count() > 0) {
			foreach ($keyAttributes as $keyAttribute) {
				$method = 'get'.$core->pascalCased($keyAttribute->getAttributeCode());
				$params[$keyAttribute->getAttributeCode()] = $row->$method();
			}
		}
		else {
			$params['id'] = $row->getId();
		}		
        return $this->getUrl('*/*/edit', $params);
    }
}