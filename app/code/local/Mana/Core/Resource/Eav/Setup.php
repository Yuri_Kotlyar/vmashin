<?php
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base class for setup scripts
 * @author Mana Team
 *
 */
class Mana_Core_Resource_Eav_Setup extends Mage_Eav_Model_Entity_Setup {
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'is_key'		   			=> $this->_getValue($attr, 'is_key', 0),
            'is_global'		   			=> $this->_getValue($attr, 'is_global', Mana_Core_Model_Attribute_Scope::_GLOBAL),
        	'has_default'   			=> $this->_getValue($attr, 'has_default', 0),
            'default_model'             => $this->_getValue($attr, 'default_model', ''),
            'default_source'            => $this->_getValue($attr, 'default_source', ''),
            'default_mask'  	        => $this->_getValue($attr, 'default_mask', 0),
            'default_mask_field'        => $this->_getValue($attr, 'default_mask_field', 'default_mask0'),
        ));
        return $data;
    }
	public function updateDefaultMaskFields($entity) {
		/* @var $db  Varien_Db_Adapter_Pdo_Mysql */ $db = $this->getConnection();
		$defaultMaskFields = $db->fetchCol("SELECT DISTINCT m.`default_mask_field` 
			FROM {$this->getTable('m_attribute')} AS m
			INNER JOIN {$this->getTable('eav_attribute')} AS a ON a.attribute_id = m.attribute_id
			INNER JOIN {$this->getTable('eav_entity_type')} AS t ON t.entity_type_id = a.entity_type_id
			WHERE t.`entity_type_code` = '$entity'");
		$existingFields = $db->describeTable($this->getTable($entity));
		foreach ($defaultMaskFields as $defaultMaskField) {
			if (!isset($existingFields[$defaultMaskField])) {
				$this->run("
					ALTER TABLE `{$this->getTable($entity)}` ADD COLUMN ( 
						`$defaultMaskField` int(10) unsigned NOT NULL default '0'
					);
				");
			}
		}
		return $this;
	}
	public function getDefaultEntities() {
		return array();
	}
	public function getEntityExtensions() {
		return array();
	}
	public function installEntities($entities=null) {
		parent::installEntities($entities);

        foreach ($this->getEntityExtensions() as $entityName=>$entity) {
            $frontendPrefix = isset($entity['frontend_prefix']) ? $entity['frontend_prefix'] : '';
            $backendPrefix = isset($entity['backend_prefix']) ? $entity['backend_prefix'] : '';
            $sourcePrefix = isset($entity['source_prefix']) ? $entity['source_prefix'] : '';

            foreach ($entity['attributes'] as $attrCode=>$attr) {
                if (!empty($attr['backend'])) {
                    if ('_'===$attr['backend']) {
                        $attr['backend'] = $backendPrefix;
                    } elseif ('_'===$attr['backend']{0}) {
                        $attr['backend'] = $backendPrefix.$attr['backend'];
                    } else {
                        $attr['backend'] = $attr['backend'];
                    }
                }
                if (!empty($attr['frontend'])) {
                    if ('_'===$attr['frontend']) {
                        $attr['frontend'] = $frontendPrefix;
                    } elseif ('_'===$attr['frontend']{0}) {
                        $attr['frontend'] = $frontendPrefix.$attr['frontend'];
                    } else {
                        $attr['frontend'] = $attr['frontend'];
                    }
                }
                if (!empty($attr['source'])) {
                    if ('_'===$attr['source']) {
                        $attr['source'] = $sourcePrefix;
                    } elseif ('_'===$attr['source']{0}) {
                        $attr['source'] = $sourcePrefix.$attr['source'];
                    } else {
                        $attr['source'] = $attr['source'];
                    }
                }

                $this->addAttribute($entityName, $attrCode, $attr);
            }
        }

        return $this;
	}
}