<?php
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/* BASED ON SNIPPET: New Module/Helper/Data.php */
/**
 * Generic helper functions for Mana_Core module. This class is a must for any module even if empty.
 * @author Mana Team
 */
class Mana_Core_Helper_Data extends Mage_Core_Helper_Abstract {
    /**
     * Retrieve config value for store by path. By default uses standard Magento function to query core_config_data
     * table and use config.xml for default value. Though this could be replaced by extensions (in later versions).
     *
     * @param string $path
     * @param mixed $store
     * @return mixed
     */
	public function getStoreConfig($path, $store = null) {
		return Mage::getStoreConfig($path, $store);
	}
	public function endsWith($haystack, $needle) {
		return (strrpos($haystack, $needle) === strlen($haystack) - strlen($needle));
	}
	public function startsWith($haystack, $needle) {
		return (strpos($haystack, $needle) === 0);
	}

	public static $upperCaseCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	public static $lowerCaseCharacters = 'abcdefghijklmnopqrstuvwxyz';
	public static $whitespaceCharacters = " \t\r\n";

	protected function _explodeIdentifier($identifier) {
		$result = array(); 
		$segment = substr($identifier, 0, 1);
		$mode = 0; // not recognized
		if ($segment == '_') { $mode = 1; $result[] = ''; $segment = ''; }
		elseif ($segment == '_') { $mode = 3; $result[] = ''; $segment = ''; }
		$allUppers = !$segment || strpos(self::$upperCaseCharacters, $segment) !== false;
		for ($i = 1; $i < strlen($identifier); $i++) {
			$ch = substr($identifier, $i, 1);
			switch ($mode) {
				case 0: // not recognized
					if ($ch == '_') {
						$mode = 1; // underscored
						$result[] = $segment;
						$segment = '';
					}
					elseif ($ch == '-') {
						$mode = 3; // hyphened
						$result[] = $segment;
						$segment = '';
					}
					elseif (strpos(self::$upperCaseCharacters, $ch) !== false) {
						if (!$allUppers) {
							$mode = 2; // case separated
							$result[] = $segment;
							$segment = '';
						}
						$segment .= $ch;
					}
					else {
						if (strpos(self::$lowerCaseCharacters, $ch) !== false) $allUppers = false;
						$segment .= $ch;
					}
					break;
				case 1: // underscored
					if ($ch == '_') {
						$result[] = $segment;
						$segment = '';
					}
					else {
						$segment .= $ch;
					}
					break;
				case 2: // case separated
					if (strpos(self::$upperCaseCharacters, $ch) !== false) {
						$result[] = $segment;
						$segment = '';
					}
					$segment .= $ch;
					break;
				case 3: // hyphened
					if ($ch == '-') {
						$result[] = $segment;
						$segment = '';
					}
					else {
						$segment .= $ch;
					}
					break;
				default:
					throw new Exception('Not implemented.');
			}
		}
		if ($segment) $result[] = $segment;
		return $result;
	}
	public function pascalCased($identifier) {
		$result = '';
		foreach (self::_explodeIdentifier($identifier) as $segment) {
			$result .= ucfirst(strtolower($segment));
		}
		return $result;
	}
	public function camelCased($identifier) {
		$result = '';
		$first = true;
		foreach (self::_explodeIdentifier($identifier) as $segment) {
			if ($first) {
				$result .= strtolower($segment);
				$first = false;
			}
			else {
				$result .= ucfirst(strtolower($segment));
			}
		}
		return $result;
	}
	public function lowerCased($identifier) {
		$result = '';
		$separatorNeeded = false;
		foreach (self::_explodeIdentifier($identifier) as $segment) {
			if ($separatorNeeded) $result .= '_'; else $separatorNeeded = true;
			$result .= strtolower($segment);
		}
		return $result;
	}
	public function upperCased($identifier) {
		$result = '';
		$separatorNeeded = false;
		foreach (self::_explodeIdentifier($identifier) as $segment) {
			if ($separatorNeeded) $result .= '_'; else $separatorNeeded = true;
			$result .= strtoupper($segment);
		}
		return $result;
	}
	public function hyphenCased($identifier) {
		$result = '';
		$separatorNeeded = false;
		foreach (self::_explodeIdentifier($identifier) as $segment) {
			if ($separatorNeeded) $result .= '-'; else $separatorNeeded = true;
			$result .= strtolower($segment);
		}
		return $result;
	}
	public function labelToUrl($textIn,$attrCode=false,$attribute_value_id=false) {
        $translit = $this->translitIt($textIn);
		$text = mb_strtolower($translit);
//		$text = str_replace('-', '--', $text);
//		$text = str_replace(' ', '-', $text);
        $text = str_replace('+', '-', $text);
        $text = str_replace(' ', '-', $text);
		$text = str_replace('/', '-', $text);
		$text = str_replace(',', '-', $text);
		$text = preg_replace('/-{2,}/', '-', $text);
        $text = trim($text, '-');

        if ($translit!=$textIn){
            Mage::getModel('manapro_filterseolinks/translate')->addTranslate($text,$attrCode,$attribute_value_id);
        }

        return $text;
	}
    
	public function urlToLabel($text) {
//		$text = str_replace(array('-', '--'), array(' ', '-'), $text);
        return $text;
	}

    function translitIt($str)
{
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
    );
    return strtr($str,$tr);
}


	public function translateConfig($config) {
		$this->_translateConfigRecursively($config);
		return $config;
	}
	/**
	 * Enter description here ...
	 * @param Varien_Simplexml_Element $config
	 * @param array | null $fields
	 * @param string | null $module
	 */
	protected function _translateConfigRecursively($config, $fields = null, $module = null) {
		if ($fields && in_array($config->getName(), $fields)) {
			$name = $config->getName();
			$parent = $config->getParent();
			$value = (string)$config;
			$moduleName = $module ? $module : $this->_getModuleName();
			$parent->$name = Mage::app()->getTranslator()->translate(array(new Mage_Core_Model_Translate_Expr($value, $moduleName)));
		}
		$fields = isset($config['translate']) ? explode(',', (string)$config['translate']) : null;
		$module = isset($config['module']) ? (string)$config['module'] : null;
		foreach ($config->children() as $key => $value) {
			$this->_translateConfigRecursively($value, $fields, $module);
		}
	}
    public function mergeConfig($mergeToObject, $extensions) {
        foreach ($extensions as $extension) {
                if ($extension) {
                	$mergeModel = new Mage_Core_Model_Config_Base;
                	if ($mergeModel->loadString($extension)) {
                    	$mergeToObject->extend($mergeModel->getNode(), true);
                	}
                }
        }
        return $mergeToObject;
    }
    public function getSortedXmlChildren($parent, $child, $select = '', $filter = array()) {
		$sortedResult = array();
		$result = array();
		if ($parent && isset($parent->$child)) {
			foreach ($parent->$child->children() as $key => $options) {
				if ($this->_doesXmlConformsFilter($options, $filter)) {
					$sortOrder = isset($options->sort_order) ? (int)(string)$options->sort_order : 0; 
					if ($sortOrder != 0) {
						$sortedResult[$sortOrder] = $key;
					}
					else {
						$result[] = $key;
					}
				}
			}
			ksort($sortedResult);
			$result = array_merge(array_values($sortedResult), $result);
		}

		$selectedResult = array();
		if ($select) {
			foreach ($result as $key) {
				$selectedResult[$key] = (string)$parent->$child->$key->$select;
			}
		}
		else {
			foreach ($result as $key) {
				$selectedResult[$key] = $parent->$child->$key;
			}
		}
		return $selectedResult;
    }
	public function arrayFind($array, $column, $value)
	{
		foreach ($array as $index => $item) {
			if ($item[$column] == $value) {
				return $index;
			}
		}
		return false;
	}
	public function collectionFind($collection, $column, $value)
	{
		$method = 'get'.$this->pascalCased($column);
		foreach ($collection as $item) {
			if ($item->$method() == $value) {
				return $item;
			}
		}
		return false;
	}
	public function countXmlChildren($xml, $filter = array()) {
		$result = 0;
		foreach ($xml->children() as $child) {
			if ($this->_doesXmlConformsFilter($child, $filter)) {
				$result++;
			}
		}
		return $result;
	}
	protected function _doesXmlConformsFilter($xml, $filter) {
		foreach ($filter as $field => $value) {
			if (((string) ($xml->$field)) != $value) {
				return false;
			}
		}
		return true;
	}
	/**
	 * Returns rendered additional markup registered by extensions in configuration under $name key 
	 * @param string $name
	 * @param array $parameters
	 * @return string
	 */
	public function getNamedHtml($root, $name, $parameters = array()) {
		$result = '';
		foreach ($this->getSortedXmlChildren(Mage::getConfig()->getNode($root), $name) as $markup) {
			$filename = Mage::getBaseDir('design').DS.
				Mage::getDesign()->getTemplateFilename((string)$markup->template, array('_relative'=>true));
			if (file_exists($filename)) {
        		$result .= $this->_fetchHtml($filename, $parameters);
			}
		}
		return $result;
	}
	protected function _fetchHtml($filename, $parameters) {
        extract ($parameters, EXTR_OVERWRITE);
        ob_start();
        try {
            include $filename;
        } 
        catch (Exception $e) {
            ob_get_clean();
            throw $e;
        }
        return ob_get_clean();
	}
}