<?php
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Naive HTML tokenizer
 * @author Mana Team
 *
 */
class Mana_Core_Model_Html_Reader {
	protected $_source;
	protected $_state;
	protected $_pos = 0;
	protected $_length;
	public function setSource($source) {
		$this->_source = $source;
		$this->_state = Mana_Core_Model_Html_State::INITIAL;
		$this->_length = mb_strlen($source);
		return $this;
	}
	
	public function read($expected = 0, $allowWhitespace = true) {
	}
}