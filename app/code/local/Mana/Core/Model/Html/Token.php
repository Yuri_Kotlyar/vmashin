<?php
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * HTML tokens recognized by scanner
 * @author Mana Team
 *
 */
class Mana_Core_Model_Html_Token {
	const NOTHING = 0;
	const EOF = 1;
	const TAG_START = 2; // <
	const TAG_END = 3; // </
	const TAG_SELF_CLOSE = 4; // />
	const TAG_CLOSE = 5; // >
	const EQ = 6; // =
	const CDATA = 7; // <![CDATA[ some text ]]>
	const COMMENT = 8; // <!-- some text -->
	const TEXT = 9;
	const NAME = 10;
	const VALUE = 11; // value | 'value' | "value"
	
	protected static $_names = array(
		self::NOTHING => 'nothing',
		self::EOF => 'end-of-file',
		self::TAG_START => '"<"',
		self::TAG_END => '"</"',
		self::TAG_SELF_CLOSE => '"/>"',
		self::TAG_CLOSE => '">"',
		self::EQ => '"="',
		self::CDATA => 'CDATA section (<![CDATA[ ]]>)',
		self::COMMENT => 'comment (<!-- -->)',
		self::TEXT => 'raw text',
		self::NAME => 'element or attribute name',
		self::VALUE => 'attribute value',
		);
	public static function getName($token) {
		return Mage::helper('mana_core')->__(self::$_names[$token]);
	}
	protected static $_voidElements = array('area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 
		'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr');
	public static function isVoid($elementName) {
		return in_array(strtolower($elementName), self::$_voidElements);
	}
}