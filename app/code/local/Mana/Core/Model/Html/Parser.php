<?php
/**
 * @category    Mana
 * @package     Mana_Core
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Implements naive HTML parser
 * @author Mana Team
 *
 */
class Mana_Core_Model_Html_Parser extends Varien_Object {
	protected $_token; 
	
	protected function _construct() {
		$this->_token = $this->getReader()->read($this->hasStartsWith() ? $this->getStartsWith() : Mana_Core_Model_Html_Token::TEXT);
	}
	protected function _read($expect = 0, $throw = false, $allowWhitespace = true) {
		$this->_token = $this->getReader()->read($expect, $allowWhitespace);
		if ($throw && $this->_token['type'] != $expect) {
			throw new Exception(Mage::helper('mana_core')->__('HTML parser error: %s expected', 
				Mana_Core_Model_Html_Token::getName($expect)));
		}
		return $this_token['type'];
	}
	/**
	 * Content ::= { Normal | SelfClosing | Void | TEXT | CDATA | COMMENT }
	 * Void ::= '<' (area | base | br | col | command | embed | hr | img | input | keygen | link | meta | param | 
	 * 		source | track | wbr) Attributes ('>' | '/>')
	 * Normal ::= '<' NAME Attributes '/>' | ('>' Content '</' ID '>')
	 * Attributes ::= { NAME ['=' VALUE ] } 
	 */
	public function parseContent($parentElement) {
		$result = $this->_beforeParsingContent($parentElement);
		while ($this->_token['type'] != Mana_Core_Model_Html_Token::EOF && $this->_token['type'] != Mana_Core_Model_Html_Token::TAG_END) {
			switch ($this->_token['type']) {
				case Mana_Core_Model_Html_Token::TAG_START:
					$this->_afterParsingChildElement($parentElement, $result, $this->parseElement($this->_beforeParsingChildElement($parentElement, $result)));
					break;
				case Mana_Core_Model_Html_Token::CDATA:
					$this->_processCDATA($parentElement, $result, $this->_token);
					$this->_read(Mana_Core_Model_Html_Token::TEXT);
					break;
				case Mana_Core_Model_Html_Token::COMMENT:
					$this->_processComment($parentElement, $result, $this->_token);
					$this->_read(Mana_Core_Model_Html_Token::TEXT);
					break;
				case Mana_Core_Model_Html_Token::TEXT:
					$this->_processText($parentElement, $result, $this->_token);
					$this->_read(Mana_Core_Model_Html_Token::TEXT);
					break;
				default: 
					throw new Exception(Mage::helper('mana_core')->__('HTML parser error: unexpected %s', 
						Mana_Core_Model_Html_Token::getName($this->_token['type'])));
			}
		}
		return $this->_afterParsingContent($parentElement, $result);
	}
	public function parseElement($parentContent) {
		$result = $this->_beforeParsingElement($parentContent);
		$this->_read(Mana_Core_Model_Html_Token::NAME, true, false);
		$elementName = $this->_token['text'];
		$void = Mana_Core_Model_Html_Token::isVoid($elementName);
		$this->_processElementName($parentContent, $result, $this->_token, $elementName, $void);
		while ($this->_read() == Mana_Core_Model_Html_Token::NAME || ($this->_token['type'] == Mana_Core_Model_Html_Token::EQ)) {
			if ($this->_token['type'] != Mana_Core_Model_Html_Token::EQ) {
				$attributeName = $this->_token['text'];
				$this->_processAttributeName($parentContent, $result, $this->_token, $attributeName);
			}
			else {
				$this->_processAttributeEq($parentContent, $result, $this->_token);
				$this->_read(Mana_Core_Model_Html_Token::VALUE, true, true);
				$attributeValue = $this->_token['text'];
				$this->_processAttributeValue($parentContent, $result, $this->_token, $attributeValue);
			}
		}
		switch ($this->_token['type']) {
			case Mana_Core_Model_Html_Token::TAG_SELF_CLOSE: 
				$this->_processElementClose($parentContent, $result, $this->_token);
				break;
			case Mana_Core_Model_Html_Token::TAG_CLOSE: 
				$this->_processElementClose($parentContent, $result, $this->_token);
				if (!$void) {
					$this->_afterParsingChildContent($parentContent, $result, $this->parseContent($this->_beforeParsingChildContent($parentContent, $result)));
					$this->_read(Mana_Core_Model_Html_Token::TAG_END, true, false);
					$this->_read(Mana_Core_Model_Html_Token::NAME, true, false);
					if ($this->_token['text'] != $elementName) {
						throw new Exception(Mage::helper('mana_core')->__('HTML parser error: closing tag for %s expected', 
							$elementName));
					}
					$this->_read(Mana_Core_Model_Html_Token::TAG_CLOSE, true, false);
					$this->_processElementEnd($parentContent, $result, $this->_token, $elementName);
				}
				break;
			default:
				throw new Exception(Mage::helper('mana_core')->__('HTML parser error: %s or %s expected', 
					Mana_Core_Model_Html_Token::getName(Mana_Core_Model_Html_Token::TAG_SELF_CLOSE), 
					Mana_Core_Model_Html_Token::getName(Mana_Core_Model_Html_Token::TAG_CLOSE)));
		}
		$this->_read(Mana_Core_Model_Html_Token::TEXT);
		return $this->_afterParsingElement($parentContent, $result);
	}
	
	/* CONTENT CALLBACKS */
	
	protected function _beforeParsingContent($parentElement) {
		return null;
	}
	protected function _afterParsingContent($parentElement, $content) {
		return $content;
	}
	protected function _beforeParsingChildElement($parentElement, $content) {
		return $content;
	}
	protected function _afterParsingChildElement($parentElement, $content, $childElement) {
	}
	protected function _processCDATA($parentElement, $content, $token) {
	}
	protected function _processComment($parentElement, $content, $token) {
	}
	protected function _processText($parentElement, $content, $token) {
	}
	
	/* ELEMENT CALLBACKS */
	
	protected function _beforeParsingElement($parentContent) {
		return null;
	}
	protected function _afterParsingElement($parentContent, $element) {
		return $element;
	}
	protected function _processElementName($parentContent, $element, $token, $elementName, $void) {
	}
	protected function _processAttributeName($parentContent, $element, $token, $attributeName) {
	}
	protected function _processAttributeEq($parentContent, $element, $token) {
	}
	protected function _processAttributeValue($parentContent, $element, $token, $attributeValue) {
	}
	protected function _processElementClose($parentContent, $element, $token) {
	}
	protected function _beforeParsingChildContent($parentContent, $element) {
		return $element;
	}
	protected function _afterParsingChildContent($parentContent, $element, $childContent) {
	}
	protected function _processElementEnd($parentContent, $element, $token, $elementName) {
	}
}