<?php
// методы взяты из класса Mage_Eav_Model_Entity_Setup в файле app\code\core\Mage\Eav\Model\Entity\Setup.php

$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$installer->removeAttribute($entityTypeId, 'h1');
//$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId); //можно получить дефолтную группу
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'General Information'); //получить группу по названию или ID

$installer->addAttribute('catalog_category', 'h1',  array(
    'type'     => 'text',
    'label'    => 'Заголовок H1',
    'input'    => 'text',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => ''
));


$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'h1',
    '6'                    //позиция атрибута в закладке. В закладке General всего 10 элементов
);

$attributeId = $installer->getAttributeId($entityTypeId, 'h1');
 
$installer->endSetup();
