<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * Mage_Eav_Model_Entity_Setup
 */
$installer = $this;
$installer->startSetup();

$gui_data = 'a:7:{s:6:"export";a:1:{s:13:"add_url_field";s:1:"0";}s:6:"import";a:2:{s:17:"number_of_records";s:4:"1000";s:17:"decimal_separator";s:1:".";}s:4:"file";a:8:{s:4:"type";s:4:"file";s:8:"filename";s:18:"export_product.csv";s:4:"path";s:10:"var/export";s:4:"host";s:0:"";s:4:"user";s:0:"";s:8:"password";s:0:"";s:9:"file_mode";s:1:"2";s:7:"passive";s:0:"";}s:5:"parse";a:5:{s:4:"type";s:3:"csv";s:12:"single_sheet";s:0:"";s:9:"delimiter";s:1:",";s:7:"enclose";s:1:""";s:10:"fieldnames";s:0:"";}s:3:"map";a:3:{s:14:"only_specified";s:0:"";s:7:"product";a:2:{s:2:"db";a:5:{i:0;s:13:"attribute_set";i:1;s:3:"sku";i:2;s:5:"price";i:3;s:11:"is_in_stock";i:4;s:3:"yml";}s:4:"file";a:5:{i:0;s:0:"";i:1;s:14:"Артикул";i:2;s:8:"Цена";i:3;s:32:"Наличие на складе";i:4;s:12:"Яндекс";}}s:8:"customer";a:2:{s:2:"db";a:2:{i:0;s:12:"billing_city";i:1;s:12:"billing_city";}s:4:"file";a:2:{i:0;s:0:"";i:1;s:0:"";}}}s:7:"product";a:1:{s:6:"filter";a:8:{s:4:"name";s:0:"";s:3:"sku";s:0:"";s:4:"type";s:1:"0";s:13:"attribute_set";s:0:"";s:5:"price";a:2:{s:4:"from";s:0:"";s:2:"to";s:0:"";}s:3:"qty";a:2:{s:4:"from";s:0:"";s:2:"to";s:0:"";}s:10:"visibility";s:1:"0";s:6:"status";s:1:"0";}}s:8:"customer";a:1:{s:6:"filter";a:10:{s:9:"firstname";s:0:"";s:8:"lastname";s:0:"";s:5:"email";s:0:"";s:5:"group";s:1:"0";s:10:"adressType";s:15:"default_billing";s:9:"telephone";s:0:"";s:8:"postcode";s:0:"";s:7:"country";s:0:"";s:6:"region";s:0:"";s:10:"created_at";a:2:{s:4:"from";s:0:"";s:2:"to";s:0:"";}}}}';

$write = Mage::getSingleton("core/resource")->getConnection("core_write");
$query = 'INSERT INTO mage_dataflow_profile (name, actions_xml, gui_data, direction, entity_type, store_id, data_transfer)
    VALUES (
\'AppDragon Import\', 
\'<action type="dataflow/convert_parser_csv" method="parse">
    <var name="delimiter"><![CDATA[,]]></var>
    <var name="enclose"><![CDATA["]]></var>
    <var name="fieldnames"></var>
    <var name="map">
        <map name="Артикул"><![CDATA[sku]]></map>
        <map name="Цена"><![CDATA[price]]></map>
        <map name="Наличие на складе"><![CDATA[is_in_stock]]></map>
        <map name="Яндекс"><![CDATA[yml]]></map>
    </var>
    <var name="store"><![CDATA[1]]></var>
    <var name="number_of_records">1000</var>
    <var name="decimal_separator"><![CDATA[.]]></var>
    <var name="adapter">appdragon_import/adapter_product</var>
    <var name="method">parse</var>
</action>\',
:gui_data,
\'import\',
\'product\',
\'1\',
\'interactive\'    
)';

$binds = array(
    'gui_data'  => $gui_data,
);

$write->query($query, $binds);

$installer->endSetup();