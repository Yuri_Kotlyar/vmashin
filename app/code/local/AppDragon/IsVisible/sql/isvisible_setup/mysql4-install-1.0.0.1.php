<?php
// методы взяты из класса Mage_Eav_Model_Entity_Setup в файле app\code\core\Mage\Eav\Model\Entity\Setup.php

$installer = $this;
$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
//$installer->removeAttribute($entityTypeId, 'is_visible');
//$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId); //можно получить дефолтную группу
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'General Information'); //получить группу по названию или ID

$installer->addAttribute('catalog_category', 'is_visible',  array(
    'type'      => 'varchar',
    'label'     => 'Показывать пиктограмму ?',
    'input'     => 'select',
    'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'   => true,
    'required'  => false,
    'user_defined' => true,
    'option' => array(
        'value' => array(
            'no'=>array('no'),
            'yes'=>array('yes')
        )
    )
));


$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'is_visible',
    '10'                    //позиция атрибута в закладке. В закладке General всего 10 элементов
);

$attributeId = $installer->getAttributeId($entityTypeId, 'is_visible');

$installer->endSetup();
