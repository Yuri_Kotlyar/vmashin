<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product View block
 *
 * @category Mage
 * @package  Mage_Catalog
 * @module   Catalog
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default MAP renderer type
     *
     * @var string
     */
    protected $_mapRenderer = 'msrp_item';

    private $_session;
    
    private $_customer_name = '';
    
    private $_customer_phone = '';
    
    private $_customer_email = '';
    
    private $_customer_address = '';
    /**
     * Add meta information from product to head block
     *
     * @return Mage_Catalog_Block_Product_View
     */
protected function _prepareLayout()
{
    $product = $this->getProduct();
    if ($this->helper('catalog/product')->canUseCanonicalTag()) {
        $params = array('_ignore_category'=>true);
        /////////////////////////////////////// START WITH CANONICAL
        $cannURL = $product->getUrlModel()->getUrl($product, $params);

        $currentURL = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REDIRECT_URL'];

        if($currentURL != $cannURL){
            header ('HTTP/1.1 301 Moved Permanently');
            header ('Location: ' . $cannURL);
            exit;                
        }
        //////////////////////////////////// END WITH CANONICAL
    }
    $this->getLayout()->createBlock('catalog/breadcrumbs');
    $headBlock = $this->getLayout()->getBlock('head');
    if ($headBlock) {
        $title = $product->getMetaTitle();
        if ($title) {
            $headBlock->setTitle("$title Москва, Санкт-Петербург, Новосибирск, Екатеринбург, Нижний Новгород, Казань");
        }else{
            $title = $product->getName();
            $headBlock->setTitle("$title Москва, Санкт-Петербург, Новосибирск, Екатеринбург, Нижний Новгород, Казань");
        }
        $keyword = $product->getMetaKeyword();
        $currentCategory = Mage::registry('current_category');
        if ($keyword) {
            $headBlock->setKeywords($keyword);
        } elseif($currentCategory) {
            $headBlock->setKeywords($product->getName());
        }
        $description = $product->getMetaDescription();
        if ($description) {
            $headBlock->setDescription( ($description) );
        } else {
            $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255));
        }
        if ($this->helper('catalog/product')->canUseCanonicalTag()) {
            $headBlock->addLinkRel('canonical', $cannURL);   
        }
        
    }
    return parent::_prepareLayout();
}

    /**
     * Retrieve current product model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!Mage::registry('product') && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
            Mage::register('product', $product);
        }
        return Mage::registry('product');
    }

    /**
     * Check if product can be emailed to friend
     *
     * @return bool
     */
    public function canEmailToFriend()
    {
        $sendToFriendModel = Mage::registry('send_to_friend_model');
        return $sendToFriendModel && $sendToFriendModel->canEmailToFriend();
    }

    /**
     * Retrieve url for direct adding product to cart
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $additional
     * @return string
     */
    public function getAddToCartUrl($product, $additional = array())
    {
        if ($this->hasCustomAddToCartUrl()) {
            return $this->getCustomAddToCartUrl();
        }

        if ($this->getRequest()->getParam('wishlist_next')){
            $additional['wishlist_next'] = 1;
        }

        $addUrlKey = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
        $addUrlValue = Mage::getUrl('*/*/*', array('_use_rewrite' => true, '_current' => true));
        $additional[$addUrlKey] = Mage::helper('core')->urlEncode($addUrlValue);

        return $this->helper('checkout/cart')->getAddUrl($product, $additional);
    }

    /**
     * Get JSON encoded configuration array which can be used for JS dynamic
     * price calculation depending on product options
     *
     * @return string
     */
    public function getJsonConfig()
    {
        $config = array();
        if (!$this->hasOptions()) {
            return Mage::helper('core')->jsonEncode($config);
        }

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest(false, false, false);
        /* @var $product Mage_Catalog_Model_Product */
        $product = $this->getProduct();
        $_request->setProductClassId($product->getTaxClassId());
        $defaultTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest();
        $_request->setProductClassId($product->getTaxClassId());
        $currentTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_regularPrice = $product->getPrice();
        $_finalPrice = $product->getFinalPrice();
        $_priceInclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, true);
        $_priceExclTax = Mage::helper('tax')->getPrice($product, $_finalPrice);
        $_tierPrices = array();
        $_tierPricesInclTax = array();
        foreach ($product->getTierPrice() as $tierPrice) {
            $_tierPrices[] = Mage::helper('core')->currency($tierPrice['website_price'], false, false);
            $_tierPricesInclTax[] = Mage::helper('core')->currency(
                Mage::helper('tax')->getPrice($product, (int)$tierPrice['website_price'], true),
                false, false);
        }
        $config = array(
            'productId'           => $product->getId(),
            'priceFormat'         => Mage::app()->getLocale()->getJsPriceFormat(),
            'includeTax'          => Mage::helper('tax')->priceIncludesTax() ? 'true' : 'false',
            'showIncludeTax'      => Mage::helper('tax')->displayPriceIncludingTax(),
            'showBothPrices'      => Mage::helper('tax')->displayBothPrices(),
            'productPrice'        => Mage::helper('core')->currency($_finalPrice, false, false),
            'productOldPrice'     => Mage::helper('core')->currency($_regularPrice, false, false),
            'priceInclTax'        => Mage::helper('core')->currency($_priceInclTax, false, false),
            'priceExclTax'        => Mage::helper('core')->currency($_priceExclTax, false, false),
            /**
             * @var skipCalculate
             * @deprecated after 1.5.1.0
             */
            'skipCalculate'       => ($_priceExclTax != $_priceInclTax ? 0 : 1),
            'defaultTax'          => $defaultTax,
            'currentTax'          => $currentTax,
            'idSuffix'            => '_clone',
            'oldPlusDisposition'  => 0,
            'plusDisposition'     => 0,
            'oldMinusDisposition' => 0,
            'minusDisposition'    => 0,
            'tierPrices'          => $_tierPrices,
            'tierPricesInclTax'    => $_tierPricesInclTax,
        );

        $responseObject = new Varien_Object();
        Mage::dispatchEvent('catalog_product_view_config', array('response_object'=>$responseObject));
        if (is_array($responseObject->getAdditionalOptions())) {
            foreach ($responseObject->getAdditionalOptions() as $option=>$value) {
                $config[$option] = $value;
            }
        }

        return Mage::helper('core')->jsonEncode($config);
    }

    /**
     * Return true if product has options
     *
     * @return bool
     */
    public function hasOptions()
    {
        if ($this->getProduct()->getTypeInstance(true)->hasOptions($this->getProduct())) {
            return true;
        }
        return false;
    }

    /**
     * Check if product has required options
     *
     * @return bool
     */
    public function hasRequiredOptions()
    {
        return $this->getProduct()->getTypeInstance(true)->hasRequiredOptions($this->getProduct());
    }

    /**
     * Define if setting of product options must be shown instantly.
     * Used in case when options are usually hidden and shown only when user
     * presses some button or link. In editing mode we better show these options
     * instantly.
     *
     * @return bool
     */
    public function isStartCustomization()
    {
        return $this->getProduct()->getConfigureMode() || Mage::app()->getRequest()->getParam('startcustomization');
    }

    /**
     * Get default qty - either as preconfigured, or as 1.
     * Also restricts it by minimal qty.
     *
     * @param null|Mage_Catalog_Model_Product $product
     * @return int|float
     */
    public function getProductDefaultQty($product = null)
    {
        if (!$product) {
            $product = $this->getProduct();
        }

        $qty = $this->getMinimalQty($product);
        $config = $product->getPreconfiguredValues();
        $configQty = $config->getQty();
        if ($configQty > $qty) {
            $qty = $configQty;
        }

        return $qty;
    }
    
    public function createOrderOnFly($_product){
       // get current customer 
       $customer = $this->_getCustomer();
       // get customer data from $_POST
       $this->_getCustomerData();
       $storeId = $customer->getStoreId();
       // set Order Details
       $order = $this->_setOrder($storeId);
       // set Customer Data
       $this->_setCustomerData($order, $customer);
       // set Billing Details
       $billingAddress = $this->_setBilling($customer, $storeId);       
       $order->setBillingAddress($billingAddress);
       // set Shipping Details
       $shippingAddress = $this->_setShipping($customer, $storeId);
       $order->setShippingAddress($shippingAddress)
       ->setShipping_method('flatrate_flatrate')
       ->setShippingDescription($this->getCarrierName('flatrate'));
       // Set Payment Details
       $orderPayment = Mage::getModel('sales/order_payment')
       ->setStoreId($storeId)
       ->setCustomerPaymentId(0)
       ->setMethod('ig_cashondelivery')
       ->setPo_number(' - ');
       $order->setPayment($orderPayment);

       // if we have 2 products uncomment follow lines
       //$subTotal = 0;
       //$products = array('1001' => array('qty' => 1),
       //                  '1002' => array('qty' => 3),
       //                  );
       //foreach ($products as $productId=>$product) {
       //$_product = Mage::getModel('catalog/product')->load($productId);
       $rowTotal = $_product->getPrice();// * $product['qty'];
       // Set order item
       $orderItem = $this->_setOrderItem($_product, $storeId, $rowTotal);
       $subTotal = $rowTotal; // +=
       $order->addItem($orderItem);
       //}

       $order->setSubtotal($subTotal)
       ->setBaseSubtotal($subTotal)
       ->setGrandTotal($subTotal)
       ->setBaseGrandTotal($subTotal);

       $transaction = $this->_setTransaction($order);
       if($transaction->save()){
           $order->sendNewOrderEmail();
           return true;
       }else{
           return false;
       }       
    }
   
    private function _getCustomer(){        
        $this->_session = Mage::getSingleton('customer/session');
        if($this->_session->isLoggedIn()){
            return $this->_session->getCustomer();
        }else{
            return Mage::getModel('customer/customer')->load(1);            
        }
    }
    
    private function _setOrder($storeId){        
       $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
       return Mage::getModel('sales/order')
            ->setIncrementId($reservedOrderId)
            ->setStoreId($storeId)
            ->setQuoteId(0)
            ->setGlobal_currency_code('RUR')
            ->setBase_currency_code('RUR')
            ->setStore_currency_code('RUR')
            ->setOrder_currency_code('RUR');        
    }
    
    private function _setCustomerData($order, $customer){       
       $order->setCustomer_email($this->_customer_email)//$customer->getEmail()
            ->setCustomerFirstname($this->_customer_name)//$customer->getFirstname()
            ->setCustomerLastname('')//$customer->getLastname()
            ->setCustomerGroupId($customer->getGroupId())
            ->setCustomer_is_guest(1)
            ->setCustomer($customer);        
    }
    
    private function _setBilling($customer, $storeId){
       $billing = $customer->getDefaultBillingAddress();
       return Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($customer->getDefaultBilling())
                ->setCustomer_address_id($billing->getEntityId())
                ->setPrefix('')
                ->setFirstname($this->_customer_name)
                ->setMiddlename('')
                ->setLastname('')
                ->setSuffix('')
                ->setCompany('')
                ->setStreet($this->_customer_address)
                ->setCity('')
                ->setCountry_id('RU')
                ->setRegion('')
                ->setRegion_id('')
                ->setPostcode('')
                ->setTelephone($this->_customer_phone)
                ->setFax('');        
    }
    
    private function _setShipping($customer, $storeId){
       $shipping = $customer->getDefaultShippingAddress();
       return Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($customer->getDefaultShipping())
                ->setCustomer_address_id($shipping->getEntityId())
                ->setPrefix('')
                ->setFirstname($this->_customer_name)//$shipping->getFirstname()
                ->setMiddlename('')//$shipping->getMiddlename()
                ->setLastname('')//$shipping->getLastname()
                ->setSuffix('')
                ->setCompany('')
                ->setStreet($this->_customer_address)//$shipping->getStreet()
                ->setCity('')//$shipping->getCity()
                ->setCountry_id('')
                ->setRegion('')
                ->setRegion_id('')
                ->setPostcode('')
                ->setTelephone($this->_customer_phone)//$shipping->getTelephone()
                ->setFax('');        
    }
    
    private function _setOrderItem($_product, $storeId, $rowTotal){
        return Mage::getModel('sales/order_item')
               ->setStoreId($storeId)
               ->setQuoteItemId(0)
               ->setQuoteParentItemId(NULL)
               ->setProductId($_product->getId())
               ->setProductType($_product->getTypeId())
               ->setQtyBackordered(NULL)
               ->setTotalQtyOrdered(1)//$product['rqty']
               ->setQtyOrdered(1)//$product['qty']
               ->setName($_product->getName())
               ->setSku($_product->getSku())
               ->setPrice($_product->getPrice())
               ->setBasePrice($_product->getPrice())
               ->setOriginalPrice($_product->getPrice())
               ->setRowTotal($rowTotal)
               ->setBaseRowTotal($rowTotal);        
    }
    
    private function _setTransaction($order){
        return Mage::getModel('core/resource_transaction')    
               ->addObject($order)
               ->addCommitCallback(array($order, 'place'))
               ->addCommitCallback(array($order, 'save'));        
    }
    
    private function _getCustomerData(){
        if(!empty($_POST['name'])){
            $this->_customer_name = $this->getRequest()->getPost('name');
        }
        if(!empty($_POST['telephone'])){
            $this->_customer_phone = $this->getRequest()->getPost('telephone');
        }
        if(!empty($_POST['email'])){
            $this->_customer_email = $this->getRequest()->getPost('email');
        }
        if(!empty($_POST['address'])){
            $this->_customer_address = $this->getRequest()->getPost('address');
        }        
    }
    
    protected function _validateForm(){
        $fields = array('name', 'telephone');
        $error = false;
        foreach ($fields as $field){
            if (empty($_POST[$field])){
                $error = 'Не заполнено одно из обязательных полей';
                break;
            }
        }
        if (!$error){
            $email = trim($_POST['email']);
            if (!empty($email) && 
                !preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", trim($email))){
                    $error = 'Неверный адрес электронной почты';
            } 
        }
        
        return $error;
    }
}
