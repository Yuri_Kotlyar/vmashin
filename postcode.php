<?php
$data = array();
$post = new Postcode();
$data = $post->getPostcodeData((int) $_REQUEST['term']);
unset($post);
echo $_GET['callback'] . '(' . json_encode($data) . ');';
exit();

/**
 * @author RUGENTO
 *
 */
class Postcode
{
    protected $_db;
    protected $_table_prefix;

    /**
     *
     */
    function __construct()
    {
        $this->_db = $this->getDb();
    }

    /**
     *
     */
    function __destruct()
    {
        $this->_db = null;
    }

    /**
     * @param unknown $keyword
     * @param number $limit
     * @return multitype:
     */
    function getPostcodeData($keyword, $limit = 12)
    {
        if(strlen($keyword) < 4 || !is_int($keyword)) {
            return array();
        }

        $array = $_array = $tmp = array();
        $result = $this->_db->prepare("SELECT * FROM `".$this->_table_prefix."postcalc_post_indexes` WHERE (post_index like '".$keyword."%')");
        $result->execute();

        while($row = $result->fetch()) {
            $tmp[] = $row;
        }

        foreach ($tmp as $keys =>$val) {
              $_ops = '';
              foreach (str_split($val['ops_name'],1) as $val_str) {
                  if(!is_numeric($val_str)) {
                      $_ops .= $val_str;
                  }
              }
              $_ops = trim($_ops);

            if(substr_count($_ops, 'МСЦ')|| substr_count($_ops, 'МОСКВАДОМОДЕДОВОАОПП')|| substr_count($_ops, 'МОСКВАВНУКОВОАОПП')|| substr_count($_ops, 'МОСКВАБЕЛОРУССКИЙВОКЗАЛОПП')|| substr_count($_ops, 'АСЦ')|| substr_count($_ops, 'ЦМП')|| substr_count($_ops, 'ПЖДП')|| substr_count($_ops, 'УФПС') || substr_count($_ops, 'ТАРЫ') || substr_count($_ops, 'ПОЧТА')) continue ;

            if($val['ops_type'] != 'О') continue ;

            if($_ops == $val['ems_location_code'] || $val['ems_location_code'] == 'МОСКВА') {
                $array[$_ops.$keys]['value'] = $this->mb_ucfirst (mb_strtolower ($_ops,'UTF-8'));
            } else {
                $array[$_ops.$keys]['value'] = $this->mb_ucfirst (mb_strtolower ($_ops,'UTF-8')).', '.$this->mb_ucfirst (mb_strtolower ($val['ems_location_code'],'UTF-8'));
            }
            $array[$_ops.$keys]['value']      = $val['post_index'].' ('.$this->mb_ucfirst (mb_strtolower($_ops,'UTF-8')).')';
            $array[$_ops.$keys]['city']       = $this->mb_ucfirst(mb_strtolower($_ops, 'UTF-8'));
            $array[$_ops.$keys]['index']      = $val['post_index'];
            $array[$_ops.$keys]['region']     = $this->mb_ucfirst(mb_strtolower($val['ems_location_code'],'UTF-8'));
            $array[$_ops.$keys]['ems_region'] = $val['ems_location_code'];
        }

        foreach($array as $key => $value) {
            $_array[] = $value;
        }
        return array_slice($_array, 0, $limit);
    }

    /**
     * @param unknown $string
     * @return string
     */
    function mb_ucfirst($string)
    {
        $end = '';
        $data = explode('-', $string);

        foreach ($data as $key => $str) {
            $str = mb_ereg_replace("^[\ ]+","", $str);
            $end .= mb_strtoupper(mb_substr($str, 0, 1, "UTF-8"), "UTF-8").mb_substr($str, 1, mb_strlen($str), "UTF-8" );
            if(count($data) > 1 && !$key) {
                $end .= '-';
            }
        }
        return $end;
    }

    /**
     * @return PDO
     */
    function getDb()
    {
        $fileSettings = file_get_contents('var/post_code.json');
        if(!$fileSettings) {
            require_once 'app/Mage.php';
            Mage::app()->setCurrentStore(0);
            $fileSettings = $this->_getDatabaseDetails();
            if($fileSettings) {
                file_put_contents('var/post_code.json', json_encode($fileSettings));
            } else {
                exit(0);
            }
            $fileSettings = file_get_contents('var/post_code.json');
        }

        $param               = json_decode($fileSettings);
        $host                = (string) $param->host;
        $username            = (string) $param->username;
        $password            = (string) $param->password;
        $dbname              = (string) $param->dbname;
        $this->_table_prefix = $param->table_prefix;

        if($param) {
            try {
                $db = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
                $db->query("SET SQL_MODE=''");
                $db->query('SET NAMES utf8');
            }
            catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
        return $db;
    }

    /**
     *
     * @return multitype:string unknown NULL |boolean
     */
    protected function _getDatabaseDetails ()
    {
        $helper = Mage::helper('onestepcheckout');
        $configs = array(
                'model'        => 'mysql4',
                'active'       => '1',
                'host'         => '',
                'username'     => '',
                'password'     => '',
                'dbname'       => '',
                'charset'      => 'utf8',
                'table_prefix' => '',
        );

        foreach ($configs as $key => $defaultValue) {
            $value = $helper->getConfig('geoip/' . $key);
            if ($value) {
                $configs[$key] = $value;
            }
        }

        foreach (array(
                'password',
        ) as $field) {
            if (isset($configs[$field])) {
                $configs[$field] = Mage::helper('core')->decrypt($configs[$field]);
            }
        }
        if (isset($configs['host']) && $configs['host']) {
            return $configs;
        }
        return false;
    }
}