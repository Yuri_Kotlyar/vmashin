<?php
/**
 * Оптимизаторский файл. Подключать только include_once!!! Не забываем global $aSEOData, где нужно.
 *
 * if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/d-seo.php')) {
 *   include_once($_SERVER['DOCUMENT_ROOT'] . '/d-seo.php');
 * }
 *
 * Изменяемые параметры массива $aSEOData (квадратными скобками выделены неактивные)
 * title             - title страницы
 * descr             - meta descr
 * keywr             - meta keywords
 *
 */

//Глобальные значения (по умолчанию)
  $aSEOData['title'] = '';
  $aSEOData['descr'] = '';
  $aSEOData['keywr'] = '';

//Определяем адрес (REQUEST_URI есть не всегда)
  $sSEOUrl = $_SERVER['REQUEST_URI'];

function preg_h1($sContent){
  return preg_match('#<h1[^>]*>(.*)</h1>#siU', $sContent, $match) ? $match[1] : '';
}

$is_product = stripos($sContent, 'class="product-view"') !== false;

$h1 = preg_h1($sContent);
$meta_ok = 0;
if(!$meta_ok){
  $arr = array(
  '/avtomodeli\.html|/avia\.html|/helicopter\.html|/ships\.html|/tank\.html|/radio\.html|/simulator\.html|/esc-motor\.html|/engines\.html|/zapchasti\.html|/accessories\.html|/radio-controlled-toys\.html|/electric-car\.html|/quadrocycle\.html|/bike-1\.html|/bike\.html|/velomobile\.html|/scooters\.html|/snegokaty\.html|/tubing\.html|/trampolines\.html',
  '/avtomodeli/vnedorozhniki\.html|/avtomodeli/touring\.html|/avtomodeli/gasolin\.html|/avtomodeli/motobikes\.html',
  '/avtomodeli/vnedorozhniki/monster\.html|/avtomodeli/vnedorozhniki/truggy\.html|/avtomodeli/vnedorozhniki/truck\.html|/avtomodeli/vnedorozhniki/krauler\.html|/avtomodeli/vnedorozhniki/buggy\.html|/avtomodeli/vnedorozhniki/short-course\.html|/avtomodeli/vnedorozhniki/the-transponder-scale-1-5\.html',
  '/avtomodeli/vnedorozhniki/manufacturer/traxxas\.html|/avtomodeli/vnedorozhniki/manufacturer/hpi-racing\.html|/avtomodeli/vnedorozhniki/manufacturer/hsp\.html|/avtomodeli/vnedorozhniki/manufacturer/kyosho\.html|/avtomodeli/vnedorozhniki/manufacturer/maverick\.html|/avtomodeli/vnedorozhniki/manufacturer/team-associated\.html|/avtomodeli/vnedorozhniki/manufacturer/axial\.html|/avtomodeli/vnedorozhniki/manufacturer/anderson\.html|/avtomodeli/vnedorozhniki/manufacturer/fg-modellsport\.html|/avtomodeli/vnedorozhniki/manufacturer/losi\.html|/avtomodeli/vnedorozhniki/manufacturer/huanqi\.html|/avtomodeli/vnedorozhniki/manufacturer/pilotage\.html|/avtomodeli/vnedorozhniki/manufacturer/river-hobby\.html|/avtomodeli/vnedorozhniki/manufacturer/bsd-racing\.html|/avtomodeli/vnedorozhniki/manufacturer/protech\.html|/avtomodeli/vnedorozhniki/manufacturer/lrp\.html|/avtomodeli/vnedorozhniki/manufacturer/hotbodies\.html|/avtomodeli/vnedorozhniki/manufacturer/nanda\.html|/avtomodeli/vnedorozhniki/manufacturer/himoto\.html|/avtomodeli/vnedorozhniki/manufacturer/iron-track\.html',
  '/avtomodeli/touring/manufacturer/hpi-racing.html|/avtomodeli/touring/manufacturer/maverick.html|/avtomodeli/touring/manufacturer/hsp.html|/avtomodeli/touring/manufacturer/kyosho.html|/avtomodeli/touring/manufacturer/traxxas.html|/avtomodeli/touring/manufacturer/tamiya.html|/avtomodeli/touring/manufacturer/team-magic.html|/avtomodeli/touring/manufacturer/team-associated.html|/avtomodeli/touring/manufacturer/fg-modellsport.html|/avtomodeli/touring/manufacturer/losi.html|/avtomodeli/touring/manufacturer/firelap.html|/avtomodeli/touring/manufacturer/bsd-racing.html|/avtomodeli/touring/manufacturer/protech.html|/avtomodeli/touring/manufacturer/himoto.html',
  '/avtomodeli/touring/drift\.html|/avtomodeli/touring/on-road\.html|/avtomodeli/touring/rally\.html|/avtomodeli/touring\.html',
  '/avia/trainer\.html|/avia/aerobatic\.html|/avia/copies\.html|/avia/jet-impeller\.html|/avia/glider\.html|/avia\.html',
  '/avia/manufacturer/art-tech-airplane\.html|/avia/manufacturer/airplane-dynam\.html|/avia/manufacturer/great-planes\.html|/avia/manufacturer/nine-eagle-airplane\.html|/avia/manufacturer/airplane-kyosho\.html|/avia/manufacturer/airplane-fms\.html|/avia/manufacturer/protech\.html|/avia/manufacturer/e-flite\.html|/avia/manufacturer/hangar-9\.html|/avia/manufacturer/hobbyzone\.html|/avia/manufacturer/parkzone\.html|/avia/manufacturer/sonic-modell\.html|/avia/manufacturer/hobbico\.html|/avia/manufacturer/flyzone\.html|/avia/manufacturer/top-flite\.html|/avia/manufacturer/pilotage-rc\.html|/avia/manufacturer/easysky\.html',
  '/helicopter/videocamera\.html|/helicopter/quadrocopter\.html|/helicopter/mjx\.html|/helicopter/nine-eagles\.html|/helicopter/align-trex\.html|/helicopter/syma\.html|/helicopter/double-horse\.html|/helicopter/walkera\.html|/helicopter/e-sky\.html|/helicopter/art-tech\.html|/helicopter/pilotage\.html|/helicopter/merlin-trace\.html|/helicopter/blade\.html|/helicopter/wltoys\.html|/helicopter/topwin\.html|/helicopter/winyea\.html|/helicopter/protech\.html|/helicopter/dji-innovations\.html',
  '/ships/yaht\.html|/ships/boats\.html|/ships/fishing-boat\.html|/ships/copy-boats\.html',
  '/ships/yaht/[^/\.]+\.html|/ships/boats/[^/\.]+\.html|/ships/fishing-boat/[^/\.]+\.html|/ships/copy-boats/[^/\.]+\.html',
  '/tank/vs-tank\.html|/tank/pilotage\.html|/tank/huan-qi\.html|/tank/kyosho.html|/tank/heng-long\.html|/tank/household\.html',
  '/radio/receiver\.html|/radio/servo\.html|/radio/kvarcy\.html',
  '/esc-motor/esc-brushed\.html|/esc-motor/esc-brushless\.html|/esc-motor/motor-brushed\.html|/esc-motor/motor-brushless\.html',
  '/engines/auto-engines\.html|/engines/airplane-engine\.html|/engines/helicopter-engine\.html|/engines/gas-engine\.html',
  '/zapchasti/autoparts\.html',
  '/zapchasti/autoparts/hpiracingparts\.html|/zapchasti/autoparts/zapchasti-traxxas\.html|/zapchasti/autoparts/zapchasti-hsp\.html|/zapchasti/autoparts/zapchasti-kyosho\.html|/zapchasti/autoparts/zapchasti-maverick\.html|/zapchasti/autoparts/zapchasti-team-magic\.html|/zapchasti/autoparts/tyres\.html|/zapchasti/autoparts/zapchasti-team-associated\.html|/zapchasti/autoparts/zapchasti-iron-track\.html',
  '/accessories/engine/fuel-bottles\.html|/accessories/engine/starters\.html|/accessories/engine/glow-starters\.html|/accessories/engine/glow-plug\.html|/accessories/engine/instruments\.html',
  '/radio-controlled-toys/baby-rc-cars\.html|/radio-controlled-toys/radio-controlled-robots\.html|/radio-controlled-toys/constructor-radio-control\.html|/radio-controlled-toys/stroitelnaya-tehnika\.html|/radio-controlled-toys/autotrack\.html',
  '/radio-controlled-toys/autotrack/cars-for-auto-racing\.html|/radio-controlled-toys/autotrack/accessories-for-tracks\.html',
  '/electric-car/children-electric-car\.html|/electric-car/golf-car\.html|/electric-car/sigway\.html|/electric-car/monocycle\.html',
  '/electric-car/golf-car/smart-elcar\.html',
  '/electric-car/sigway/smart-cross\.html',
  '/quadrocycle/electric-atv\.html|/quadrocycle/gasoline-atv\.html|/quadrocycle/children-quad-bike\.html',
  '/quadrocycle/electric-atv/sherhan\.html|/quadrocycle/electric-atv/tredz-grizzly\.html|/quadrocycle/electric-atv/joy-automatic\.html',
  '/quadrocycle/electric-atv/sherhan\.html|/quadrocycle/gasoline-atv/armada\.html',
  '/zapchasti/autoparts/hpiracingparts/models-parts/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/chassis/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/drivetrain/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/suspension-steering/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/engine/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/electronics/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/hardware/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/1-5-gasoline-bodies/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-1-8/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-1-10/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-other/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/on-road-bodies/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/tyres/gasoline-tyres/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/tyres/offroad-tyres/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/tyres/onroad-tyres/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/tyres/drift-tyres/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/tools/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-traxxas/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-hsp/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-kyosho/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-maverick/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-team-magic/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-team-associated/[^/\.]+\.html|/zapchasti/autoparts/zapchasti-iron-track/[^/\.]+\.html|/zapchasti/autoparts/spare-parts-for-vaterra/[^/\.]+\.html|/zapchasti/autoparts/tyres/gasoline-tyres/[^/\.]+\.html|/zapchasti/autoparts/tyres/offroad-tyres/[^/\.]+\.html|/zapchasti/autoparts/tyres/onroad-tyres/[^/\.]+\.html|/zapchasti/autoparts/tyres/drift-tyres/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/[^/\.]+\.html',
  '/zapchasti/autoparts/hpiracingparts/models-parts\.html|/zapchasti/autoparts/hpiracingparts/chassis\.html|/zapchasti/autoparts/hpiracingparts/drivetrain\.html|/zapchasti/autoparts/hpiracingparts/suspension-steering\.html|/zapchasti/autoparts/hpiracingparts/engine\.html|/zapchasti/autoparts/hpiracingparts/electronics\.html|/zapchasti/autoparts/hpiracingparts/hardware\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/1-5-gasoline-bodies\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-1-8\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-1-10\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/off-road-bodies-other\.html|/zapchasti/autoparts/hpiracingparts/bodyshells/on-road-bodies\.html|/zapchasti/autoparts/hpiracingparts/tyres/gasoline-tyres\.html|/zapchasti/autoparts/hpiracingparts/tyres/offroad-tyres\.html|/zapchasti/autoparts/hpiracingparts/tyres/onroad-tyres\.html|/zapchasti/autoparts/hpiracingparts/tyres/drift-tyres\.html|/zapchasti/autoparts/hpiracingparts/tools\.html',
  '/electric-car/children-electric-car/kids-electric-cars-for-boys\.html|/electric-car/children-electric-car/kids-electric-cars-for-girls\.html|/electric-car/children-electric-car/kids-electric-cars-with-remote-control\.html|/electric-car/children-electric-car/with-rubber-wheels\.html|/electric-car/children-electric-car/mini\.html|/electric-car/children-electric-car/cheap-children-s-electric-cars\.html|/electric-car/children-electric-car/jeep\.html|/electric-car/children-electric-car/mercedes\.html|/electric-car/children-electric-car/bmw\.html|/electric-car/children-electric-car/audi\.html|/electric-car/children-electric-car/lamborghini\.html|/electric-car/children-electric-car/bugatti\.html|/electric-car/children-electric-car/porshe\.html|/electric-car/children-electric-car/ferrari\.html|/electric-car/children-electric-car/formula-f1\.html|/electric-car/children-electric-car/volvo\.html|/electric-car/children-electric-car/land-rover\.html|/electric-car/children-electric-car/hummer\.html',
  '/electric-car/children-electric-car/manufacturer/kids-cars/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/ct/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/geoby/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/injusa/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/jetem/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/joy-automatic/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/peg-perego/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/rich-toys/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/toys-toys/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/wang-jaingson/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/tcv/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/rastar/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/bugatti/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/kalee/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/chien-ti/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/tjago/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/henes/[^/\.]+\.html|/electric-car/children-electric-car/manufacturer/rt/[^/\.]+\.html',
  '/quadrocycle/children-quad-bike/children-quad-bike-on-gasoline\.html|/quadrocycle/children-quad-bike/baby-electric-atv\.html',
  '/bike-1/motorbikes-vehicles\.html|/bike-1/baby-scooter\.html|/bike-1/children-moped\.html|/bike-1/baby-tricycles\.html',
  '/bike-1/motorbikes-vehicles/manufacturer\.html',
  '/bike-1/baby-scooter/children-electric-motorcycle\.html|/bike-1/baby-scooter/children-gasoline-motorcycle\.html',
  '/bike-1/children-moped/children-electric-moped\.html',
  '/bike/children-s-bicycles\.html|/bike/electric-bikes\.html|/bike-1/benozo-moto\.html|/bike-1/electric-moto.html',
  '/bike/children-s-bicycles/tricycles-with-handle\.html|/bike/children-s-bicycles/tricycles-without-handle\.html|/bike/children-s-bicycles/two-wheeled-bicycles\.html|/bike/children-s-bicycles/velobalansiry\.html',
  '/bike/electric-bikes/folding-electric-bikes\.html|/bike/electric-bikes/celnoramniy-electric-bikes\.html|/bike/children-s-bicycles/two-wheeled-bicycles\.html|/bike/children-s-bicycles/velobalansiry\.html',
  '/bike/electric-bikes/manufacturer/ecobike\.html|/bike/electric-bikes/manufacturer/joy-automatic\.html|/bike/electric-bikes/manufacturer/smart-electric\.html',
  '/bike/children-s-bicycles/tricycles-without-handle/italtrike\.html',
  '/bike/children-s-bicycles/two-wheeled-bicycles/yedoo\.html',
  '/velomobile/recumbent-for-all-ages\.html|/velomobile/recumbent-for-children\.html|/velomobile/pedal-tractor\.html|/velomobile/accessories-for-velomobile\.html',
  '/scooters/electrik-scooters\.html|/scooters/moto-scooters\.html|/scooters/classic-scooters\.html',
  '/trampolines/trampolines-berg\.html|/trampolines/accessories-for-trampolines\.html',
  '/quadrocycle/children-quad-bike/manufacturer/injusa\.html|/quadrocycle/children-quad-bike/manufacturer/sherhan\.html|/quadrocycle/children-quad-bike/manufacturer/joy-automatic\.html|/quadrocycle/children-quad-bike/manufacturer/hoogor\.html|/quadrocycle/children-quad-bike/manufacturer/peg-perego\.html|/quadrocycle/children-quad-bike/manufacturer/armada\.html|/quadrocycle/children-quad-bike/manufacturer/kids-cars\.html|/quadrocycle/children-quad-bike/manufacturer/tcv\.html|/quadrocycle/children-quad-bike/manufacturer/mega-tredz\.html|/quadrocycle/children-quad-bike/manufacturer/rich-toys\.html|/quadrocycle/children-quad-bike/manufacturer/bugati\.html|/quadrocycle/children-quad-bike/manufacturer/beach-car\.html|/quadrocycle/children-quad-bike/manufacturer/jetem\.html',
  '/bike-1/baby-scooter/manufacturer/injusa\.html|/bike-1/baby-scooter/manufacturer/peg-perego\.html|/bike-1/baby-scooter/manufacturer/rich-toys\.html|/bike-1/baby-scooter/manufacturer/umc\.html|/bike-1/baby-scooter/manufacturer/kids-cars\.html|/bike-1/baby-scooter/manufacturer/ducati\.html|/bike-1/baby-scooter/manufacturer/joy-automatic\.html|/bike-1/baby-scooter/manufacturer/armada\.html|/bike-1/baby-scooter/manufacturer/ct\.html|/bike-1/baby-scooter/manufacturer/tcv\.html',
  '/velomobile/manufacturer/smart\.html|/velomobile/manufacturer/berg-velomobile\.html|/velomobile/manufacturer/falk\.html|/velomobile/manufacturer/rolly-toys\.html|/velomobile/manufacturer/kettler\.html|/velomobile/manufacturer/rich-toys\.html|/velomobile/manufacturer/bugati\.html|/velomobile/manufacturer/toys-toys\.html|/velomobile/manufacturer/peg-perego\.html|/velomobile/manufacturer/coloma\.html',
  '/bike/children-s-bicycles/two-wheeled-bicycles/yedoo/[^/\.]+\.html',
  '/electric-car/children-electric-car/manufacturer/kids-cars\.html|/electric-car/children-electric-car/manufacturer/ct\.html|/electric-car/children-electric-car/manufacturer/geoby\.html|/electric-car/children-electric-car/manufacturer/injusa\.html|/electric-car/children-electric-car/manufacturer/jetem\.html|/electric-car/children-electric-car/manufacturer/joy-automatic\.html|/electric-car/children-electric-car/manufacturer/peg-perego\.html|/electric-car/children-electric-car/manufacturer/rich-toys\.html|/electric-car/children-electric-car/manufacturer/toys-toys\.html|/electric-car/children-electric-car/manufacturer/wang-jaingson\.html|/electric-car/children-electric-car/manufacturer/tcv\.html|/electric-car/children-electric-car/manufacturer/rastar\.html|/electric-car/children-electric-car/manufacturer/bugatti\.html|/electric-car/children-electric-car/manufacturer/kalee\.html|/electric-car/children-electric-car/manufacturer/chien-ti\.html|/electric-car/children-electric-car/manufacturer/tjago\.html|/electric-car/children-electric-car/manufacturer/henes\.html|/electric-car/children-electric-car/manufacturer/rt\.html',
  '/electric-car/golf-car/golfcari-tvl\.html',
  '/quadrocycle/gasoline-atv/joy-automatic\.html',
  '/scooter/gasoline-scooter.html|/scooter/electric-scooter.html|/moto-technics/scooter/gasoline-scooter\.html|/moto-technics/scooter/electric-scooter\.html',
    '/bike/mountain-bikes\.html',
  '/bike/full-suspension-bike\.html|/bike/folding-bicycle\.html|/bike/female-bike\.html|/bike/road-bike\.html|/bike/cargo-bike\.html|/bike/cruise-bike\.html|/bike/extreme-bike\.html|/bike/bmx\.html|/bike/hardtail\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = $h1.': купить в интернет-магазине vmashin.ru';
      $aSEOData['descr'] = $h1.' по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
      $aSEOData['keywr'] = $h1.' интернет магазин описание характеристики цены фото';
      $aSEOData['h1'] = $is_product ? $h1 : $h1;
      $meta_ok = 1;
      break;
    }
  }
}

if(!$meta_ok){
  $arr = array(
  '/avtomodeli/vnedorozhniki/[^/]+/[^/]+/[^/\.]+\.html|/avtomodeli/vnedorozhniki/[^/]+/[^/\.]+\.html',
  '/avtomodeli/touring/[^/]+/[^/]+/[^/\.]+\.html|/avtomodeli/touring/[^/]+/[^/\.]+\.html',
  '/avtomodeli/motobikes/[^/\.]+\.html',
  '/avia/trainer/[^/\.]+\.html|/avia/aerobatic/[^/\.]+\.html|/avia/copies/[^/\.]+\.html|/avia/jet-impeller/[^/\.]+\.html|/avia/glider/[^/\.]+\.html',
  '/helicopter/videocamera/[^/\.]+\.html|/helicopter/quadrocopter/[^/\.]+\.html|/helicopter/mjx/[^/\.]+\.html|/helicopter/nine-eagles/[^/\.]+\.html|/helicopter/align-trex/[^/\.]+\.html|/helicopter/syma/[^/\.]+\.html|/helicopter/double-horse/[^/\.]+\.html|/helicopter/walkera/[^/\.]+\.html|/helicopter/e-sky/[^/\.]+\.html|/helicopter/art-tech/[^/\.]+\.html|/helicopter/pilotage/[^/\.]+\.html|/helicopter/merlin-trace/[^/\.]+\.html|/helicopter/blade/[^/\.]+\.html|/helicopter/wltoys/[^/\.]+\.html|/helicopter/topwin/[^/\.]+\.html|/helicopter/winyea/[^/\.]+\.html|/helicopter/protech/[^/\.]+\.html|/helicopter/dji-innovations/[^/\.]+\.html|/helicopter/dynam/[^/\.]+\.html',
  '/tank/vs-tank/[^/\.]+\.html|/tank/pilotage/[^/\.]+\.html|/tank/huan-qi/[^/\.]+\.html|/tank/kyosho/[^/\.]+\.html|/tank/heng-long/[^/\.]+\.html|/tank/household/[^/\.]+\.html',
  '/radio/surface-rc-system\.html|/radio/air-rc-system\.htm',
  '/radio/surface-rc-system/[^/\.]+\.html|/radio/receiver/[^/\.]+\.html|/radio/servo/[^/\.]+\.html|/radio/kvarcy/[^/\.]+\.html',
  '/radio/air-rc-system/3-4-5-channels\.html|/radio/air-rc-system/6-more-channels\.html',
  '/radio/air-rc-system/3-4-5-channels/[^/\.]+\.html|/radio/air-rc-system/6-more-channels/[^/\.]+\.html',
  '/simulator/[^/\.]+\.html',
  '/esc-motor/esc-brushed/[^/\.]+\.html|/esc-motor/esc-brushless/[^/\.]+\.html|/esc-motor/motor-brushed/[^/\.]+\.html|/esc-motor/motor-brushless/[^/\.]+\.html',
  '/engines/auto-engines/[^/\.]+\.html|/engines/airplane-engine/[^/\.]+\.html|/engines/helicopter-engine/[^/\.]+\.html|/engines/gas-engine/[^/\.]+\.html',
  '/accessories/batteries/[^/\.]+\.html|/accessories/chargers/[^/\.]+\.html|/accessories/battery/[^/\.]+\.html|/accessories/bags-for-models/[^/\.]+\.html|/accessories/krasky/[^/\.]+\.html',
  '/radio-controlled-toys/baby-rc-cars/[^/\.]+\.html|/radio-controlled-toys/radio-controlled-robots/[^/\.]+\.html|/radio-controlled-toys/constructor-radio-control/[^/\.]+\.html|/radio-controlled-toys/stroitelnaya-tehnika/[^/\.]+\.html|/radio-controlled-toys/autotrack/[^/\.]+\.html',
  '/radio-controlled-toys/autotrack/cars-for-auto-racing/[^/\.]+\.html|/radio-controlled-toys/autotrack/accessories-for-tracks/[^/\.]+\.html',
  '/electric-car/golf-car/smart-elcar/[^/\.]+\.html',
  '/electric-car/sigway/smart-cross/[^/\.]+\.html',
  '/electric-car/monocycle/[^/\.]+\.html',
  '/accessories/engine/fuel\.html|/accessories/engine/fuel-bottles\.html|/accessories/engine/starters\.html|/accessories/engine/glow-starters\.html|/accessories/engine/glow-plug\.html|/accessories/engine/instruments\.html|/accessories/batteries\.html|/accessories/chargers\.html|/accessories/battery\.html|/accessories/bags-for-models\.html|/accessories/krasky\.html',
  '/electric-car/children-electric-car/kids-electric-cars-for-boys/[^/\.]+\.html|/electric-car/children-electric-car/kids-electric-cars-for-girls/[^/\.]+\.html|/electric-car/children-electric-car/kids-electric-cars-with-remote-control/[^/\.]+\.html|/electric-car/children-electric-car/with-rubber-wheels/[^/\.]+\.html|/electric-car/children-electric-car/mini/[^/\.]+\.html|/electric-car/children-electric-car/cheap-children-s-electric-cars/[^/\.]+\.html|/electric-car/children-electric-car/jeep/[^/\.]+\.html|/electric-car/children-electric-car/mercedes/[^/\.]+\.html|/electric-car/children-electric-car/bmw/[^/\.]+\.html|/electric-car/children-electric-car/audi/[^/\.]+\.html|/electric-car/children-electric-car/lamborghini/[^/\.]+\.html|/electric-car/children-electric-car/bugatti/[^/\.]+\.html|/electric-car/children-electric-car/porshe/[^/\.]+\.html|/electric-car/children-electric-car/ferrari/[^/\.]+\.html|/electric-car/children-electric-car/formula-f1/[^/\.]+\.html|/electric-car/children-electric-car/volvo/[^/\.]+\.html|/electric-car/children-electric-car/land-rover/[^/\.]+\.html|/electric-car/children-electric-car/hummer/[^/\.]+\.html|/electric-car/children-electric-car/[^/\.]+\.html',
  '/quadrocycle/gasoline-atv/joy-automatic/[^/\.]+\.html|/quadrocycle/gasoline-atv/armada/[^/\.]+\.html',
  '/quadrocycle/children-quad-bike/children-quad-bike-on-gasoline/[^/\.]+\.html|/quadrocycle/children-quad-bike/baby-electric-atv/[^/\.]+\.html',
  '/bike-1/baby-scooter/children-electric-motorcycle/[^/\.]+\.html|/bike-1/baby-scooter/children-gasoline-motorcycle/[^/\.]+\.html',
  '/bike-1/children-moped/children-electric-moped/[^/\.]+\.html',
  '/bike-1/baby-tricycles/[^/\.]+\.html',
  '/bike/children-s-bicycles/tricycles-with-handle/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-without-handle/[^/\.]+\.html|/bike/children-s-bicycles/two-wheeled-bicycles/[^/\.]+\.html|/bike/children-s-bicycles/velobalansiry/[^/\.]+\.html',
  '/bike/electric-bikes/folding-electric-bikes/[^/\.]+\.html|/bike/electric-bikes/celnoramniy-electric-bikes/[^/\.]+\.html',
  '/velomobile/recumbent-for-all-ages/[^/\.]+\.html|/velomobile/recumbent-for-children/[^/\.]+\.html|/velomobile/pedal-tractor/[^/\.]+\.html|/velomobile/accessories-for-velomobile/[^/\.]+\.html',
  '/scooters/electrik-scooters/[^/\.]+\.html|/scooters/moto-scooters/[^/\.]+\.html|/scooters/classic-scooters/[^/\.]+\.html',
  '/tubing/luxury-snow/[^/\.]+\.html|/tubing/standard/[^/\.]+\.html|/tubing/practic/[^/\.]+\.html|/tubing/elite/[^/\.]+\.html',
  '/trampolines/trampolines-berg/[^/\.]+\.html|/trampolines/accessories-for-trampolines/[^/\.]+\.html',
  '/bike/children-s-bicycles/tricycles-with-handle/lexus\.html|/bike/children-s-bicycles/tricycles-with-handle/smoby\.html|/bike/children-s-bicycles/tricycles-with-handle/italtrike\.html|/bike/children-s-bicycles/tricycles-with-handle/injusa\.html|/bike/children-s-bicycles/tricycles-with-handle/coloma\.html|/bike/children-s-bicycles/tricycles-with-handle/rich-trike\.html',
  '/snegokaty/snowrunner/[^/\.]+\.html|/snegokaty/snow-moto/[^/\.]+\.html|/snegokaty/russian-winter/[^/\.]+\.html|/snegokaty/limited-edition/[^/\.]+\.html|/snegokaty/vector/[^/\.]+\.html|/snegokaty/penguin/[^/\.]+\.html|/snegokaty/powerful/[^/\.]+\.html|/snegokaty/shustrik/[^/\.]+\.html',
  '/avia/manufacturer/art-tech-airplane/[^/\.]+\.html|/avia/manufacturer/airplane-dynam/[^/\.]+\.html|/avia/manufacturer/great-planes/[^/\.]+\.html|/avia/manufacturer/nine-eagle-airplane/[^/\.]+\.html|/avia/manufacturer/airplane-kyosho/[^/\.]+\.html|/avia/manufacturer/airplane-fms/[^/\.]+\.html|/avia/manufacturer/protech/[^/\.]+\.html|/avia/manufacturer/e-flite/[^/\.]+\.html|/avia/manufacturer/hangar-9/[^/\.]+\.html|/avia/manufacturer/hobbyzone/[^/\.]+\.html|/avia/manufacturer/parkzone/[^/\.]+\.html|/avia/manufacturer/sonic-modell/[^/\.]+\.html|/avia/manufacturer/hobbico/[^/\.]+\.html|/avia/manufacturer/flyzone/[^/\.]+\.html|/avia/manufacturer/top-flite/[^/\.]+\.html|/avia/manufacturer/pilotage-rc/[^/\.]+\.html|/avia/manufacturer/easysky/[^/\.]+\.html',
  '/bike/children-s-bicycles/tricycles-with-handle/lexus/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-with-handle/smoby/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-with-handle/italtrike/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-with-handle/injusa/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-with-handle/coloma/[^/\.]+\.html|/bike/children-s-bicycles/tricycles-with-handle/rich-trike/[^/\.]+\.html',
  '/bike/children-s-bicycles/tricycles-without-handle/italtrike/[^/\.]+\.html',
  '/bike-1/baby-scooter/manufacturer/[^/]+/[^/\.]+\.html',
  '/zapchasti/autoparts/hpiracingparts/models-parts/detali-bullet-mt-30/[^/\.]+\.html|/zapchasti/autoparts/hpiracingparts/models-parts/detali-bullet-st-30/[^/\.]+\.html',
  '/accessories/engine/fuel/[^/\.]+\.html|/accessories/engine/fuel-bottles/[^/\.]+\.html|/accessories/engine/starters/[^/\.]+\.html|/accessories/engine/glow-starters/[^/\.]+\.html|/accessories/engine/glow-plug/[^/\.]+\.html|/accessories/engine/instruments/[^/\.]+\.html',
  '/electric-car/golf-car/golfcari-tvl/[^/\.]+\.html',
  'quadrocycle/electric-atv',
  '/quadrocycle/children-quad-bike/manufacturer/injusa/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/sherhan/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/joy-automatic/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/hoogor/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/peg-perego/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/armada/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/kids-cars/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/tcv/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/mega-tredz/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/rich-toys/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/bugati/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/beach-car/[^/\.]+\.html|/quadrocycle/children-quad-bike/manufacturer/wellness/[^/\.]+\.html',
  '/moto-technics/bike/motorbikes-vehicles/[^/]+/[^/\.]+\.html',
  '/bike-1/benozo-moto/pitbajk-armada-pb-110ss\.html|/moto-technics/bike/benozo-moto/pitbajk-armada-pb-110ss\.html',
  '/motom-technics/scooter/electric-scooter/volteco/[^/\.]+\.html|/moto-technics/scooter/electric-scooter/wellness/[^/\.]+\.html',
  '/bike/children-s-bicycles/tricycles-with-handle/icon/[^/\.]+\.html',
  '/bike/electric-bikes/manufacturer/volteco/[^/\.]+\.html|/bike/electric-bikes/manufacturer/wellness/[^/\.]+\.html|/bike/electric-bikes/manufacturer/winora/[^/\.]+\.html|/bike/electric-bikes/manufacturer/ecobike/[^/\.]+\.html|/bike/electric-bikes/manufacturer/joy-automatic/[^/\.]+\.html|/bike/electric-bikes/manufacturer/smart-electric/[^/\.]+\.html',
  '/bike/mountain-bikes/lorak/[^/\.]+\.html|/bike/mountain-bikes/stark/[^/\.]+\.html|/bike/mountain-bikes/rock-machine/[^/\.]+\.html|/bike/mountain-bikes/mtr/[^/\.]+\.html|/bike/mountain-bikes/black-aqua/[^/\.]+\.html',
  '/bike/full-suspension-bike/[^/\.]+\.html|/bike/folding-bicycle/[^/\.]+\.html|/bike/female-bike/[^/\.]+\.html|/bike/road-bike/[^/\.]+\.html|/bike/cargo-bike/[^/\.]+\.html|/bike/cruise-bike/[^/\.]+\.html|/bike/extreme-bike/[^/\.]+\.html|/bike/bmx/[^/\.]+\.html|/bike/hardtail/[^/\.]+\.html',
  '/velomobile/manufacturer/smart/[^/\.]+\.html|/velomobile/manufacturer/berg-velomobile/[^/\.]+\.html|/velomobile/manufacturer/falk/[^/\.]+\.html|/velomobile/manufacturer/rolly-toys/[^/\.]+\.html|/velomobile/manufacturer/kettler/[^/\.]+\.html|/velomobile/manufacturer/rich-toys/[^/\.]+\.html|/velomobile/manufacturer/bugati/[^/\.]+\.html|/velomobile/manufacturer/toys-toys/[^/\.]+\.html|/velomobile/manufacturer/peg-perego/[^/\.]+\.html|/velomobile/manufacturer/coloma/[^/\.]+\.html',
  '/quadrocycle/electric-atv/[^/]+/[^/\.]+\.html',
  '/moto-technics/scooter/electric-scooter/volteco/[^/\.]+\.html|/moto-technics/scooter/electric-scooter/wellness/[^/\.]+\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = $h1.': купить в интернет-магазине vmashin.ru';
      $aSEOData['descr'] = $h1.' по доступной цене в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
      $aSEOData['keywr'] = $h1.' интернет магазин описание характеристики цены фото';
      $aSEOData['h1'] = $is_product ? $h1 : $h1.' по выгодной цене';
      $meta_ok = 1;
      break;
    }
  }
}

if(!$meta_ok){
  $arr = array(
  '/avtomodeli/vnedorozhniki/manufacturer\.html',
  '/avtomodeli/touring/manufacturer\.html',
  '/avtomodeli/touring/manufacturer\.html',
  '/avia/manufacturer\.htm',
  '/accessories/engine\.html|/accessories/batteries\.html|/accessories/chargers\.html|/accessories/battery\.html|/accessories/bags-for-models\.html|/accessories/krasky\.html',
  '/radio-controlled-toys/manufacturer\.html',
  '/electric-car/children-electric-car/manufacturer\.html',
  '/trampolines/manufacturer\.html',
  '/velomobile/manufacturer\.html',
  '/bike/electric-bikes/manufacturer\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = $h1.': купить в интернет-магазине vmashin.ru';
      $aSEOData['descr'] = $h1.' в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
      $aSEOData['keywr'] = $h1.' интернет магазин описание характеристики цены фото';
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/quadrocycle/children-quad-bike/manufacturer\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = $h1.': купить в интернет-магазине vmashin.ru';
      $aSEOData['descr'] = 'Квадроциклы по производителю в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
      $aSEOData['keywr'] = $h1.' интернет магазин описание характеристики цены фото';
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/snegokaty/snowrunner\.html|/snegokaty/snow-moto\.html|/snegokaty/russian-winter\.html|/snegokaty/limited-edition\.html|/snegokaty/vector\.html|/snegokaty/penguin\.html|/snegokaty/powerful\.html|/snegokaty/shustrik\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Снегокаты $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "снегокаты $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Снегокаты $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/tubing/luxury-snow\.html|/tubing/standard\.html|/tubing/practic\.html|/tubing/elite\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Тюбинг $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "тюбинг $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Тюбинг $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/tank/yed\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Танки $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "$h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Танки $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/quadrocycle/electric-atv/wellness\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Электрические квадроциклы $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "электрические квадроциклы$h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Электрические квадроциклы $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/quadrocycle/children-quad-bike/manufacturer/wellness\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Детские квадроциклы $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "детские квадроциклы $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Детские квадроциклы $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/quadrocycle/children-quad-bike/manufacturer/wellness\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Детские велосипеды с ручкой ICON по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "детские велосипеды с ручкой icon интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Детские велосипеды с ручкой ICON по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/bike/electric-bikes/manufacturer/volteco\.html|/bike/electric-bikes/manufacturer/wellness\.html|/bike/electric-bikes/manufacturer/winora\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Электровелосипеды $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "электровелосипеды $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Электровелосипеды $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/bike/mountain-bikes/lorak\.html|/bike/mountain-bikes/stark\.html|/bike/mountain-bikes/rock-machine\.html|/bike/mountain-bikes/mtr\.html|/bike/mountain-bikes/black-aqua\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Горные велосипеды $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "горные велосипеды $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Горные велосипеды $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}
if(!$meta_ok){
  $arr = array(
  '/scooter/manufacturer/volteco\.html|/moto-technics/scooter/electric-scooter/volteco\.html|/moto-technics/scooter/electric-scooter/wellness\.html|/scooter/manufacturer/wellness\.html',
  );
  foreach ($arr as $urls) {
    if(preg_match('#^(?:'.$urls.')#siU', $_SERVER['REQUEST_URI'])){
      $aSEOData['title'] = "$h1: купить в интернет-магазине vmashin.ru";
      $aSEOData['descr'] = "Скутеры $h1 по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
      $aSEOData['keywr'] = "скутеры $h1 интернет магазин описание характеристики цены фото";
      $aSEOData['h1'] = "Скутеры $h1 по выгодной цене";
      $meta_ok = 1;
      break;
    }
  }
}





// if($_SERVER['REMOTE_ADDR'] == '77.93.126.84' || $_SERVER['REMOTE_ADDR'] == '91.244.169.157'){

  if(stripos(DUR_REQUEST_URI, '/where/') !== false){
    $filter_fields = array(
      'car-motor'=>'Двигатели',
      'readiness-model'=>'Готовность модели',
      'scale'=>'Масштаб',
      'suv-model-type'=>'Типы',
      'type-of-drive'=>'Тип привода',
      'heli-model-type'=>'Количество каналов управления',
      'design-concept'=>'Конструктивная схема',
      'flight-readiness-model'=>'Готовность модели',
      'plane-model-type'=>'Типы',
      'plane-motor'=>'Двигатели',
      'boats-model-type'=>'Типы',
      'scooter-proizv'=>'Коробка передач',
      'bike-tip'=>'Тип велосипеда',
    );

    // Формируется ассоц массив имя фильтра => зн1, зн2, зн...
    function get_str_for_title($filter_alias, $filter_name, $sContent){

      $items = preg_match('#/'.$filter_alias.'/([^/.]+)(?:\.|/)#siU', DUR_REQUEST_URI, $match) ? $match[1] : '';
      if(!empty($items)){
        preg_match_all('#([^_]+)(?:_|$)#siU', $items, $match);
        $items = $match[1];
      }else return '';

      $result = "";
      foreach ($items as $item) {
        $item_pattern = str_replace(array('(', ')'), array('\(','\)'), $item);
        $item_name = preg_match('#<!--'.$item_pattern.'-->(.*)<!--/'.$item_pattern.'-->#siU', $sContent, $match) ? trim($match[1]) : $item;
        $result.= $item_name.', ';
      }
      $result = rtrim($result, ", ");

      return $result;
    }

    $ar_filters = '';
    foreach ($filter_fields as $alias => $name) {
      if(stripos(DUR_REQUEST_URI, "/$alias/") !== false){
        $ar_filters[$name]= get_str_for_title($alias, $name, $sContent);
      }
    }

    $ttl_filter = '';
    foreach ($ar_filters as $k => $v) {
      $ttl_filter.= " | $k: $v";
    }
    $descr_filter = ' ';
    foreach ($ar_filters as $k => $v) {
      $descr_filter.= "$k: $v";
      $descr_filter.= '. ';
    }
    $keywr_filter = '';
    foreach ($ar_filters as $k => $v) {
      $keywr_filter.= "$k ". str_replace(',', '', $v);
      $keywr_filter.= ' ';
    }
    $h1_filter = ' ';
    foreach ($ar_filters as $k => $v) {
      $h1_filter.= "$k: $v";
      $h1_filter.= ', ';
    }
    $h1_filter = rtrim($h1_filter, ", ");
  $aSEOData['title'] = "$h1 в интернет-магазине vmashin.ru$ttl_filter";
  $aSEOData['descr'] = "$h1 в интернет-магазине vmashin.ru.".$descr_filter."Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.";
  $aSEOData['keywr'] = "$h1 $keywr_filter интернет магазин описание характеристики цены фото";
  $aSEOData['h1'] = "$h1, $h1_filter";
  $sContent = str_replace('<h1>', '<h1 class="filtered">', $sContent);
  }
// }








//Собственно вариации для страниц
  switch ($sSEOUrl) {

    case '/tank/household/radioupravljaemyj-tank-cs-rossija-t-90-vladimir-4101-8-household.html': // это ЧПУ
        $aSEOData['title'] = 'Радиоуправляемый танк CS Россия T-90 Владимир 4101-8 HouseHold по доступной цене в интернет-магазине vmashin.ru | Фото, характеристики, цены';
        $aSEOData['descr'] = 'Доступная цена на радиоуправляемый танк CS Россия T-90 Владимир 4101-8 HouseHold в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
      break;

    case '/bike/children-s-bicycles/two-wheeled-bicycles/rock-machine.html': // это ЧПУ
        $aSEOData['title'] = 'Двухколесные велосипеды ROCK MACHINE в интернет-магазине vmashin.ru';
        $aSEOData['descr'] = 'Двухколесные велосипеды ROCK MACHINE по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'двухколесные велосипеды icon интернет магазин описание характеристики цены фото';
        $aSEOData['h1'] = 'ROCK MACHINE по выгодной цене';
      break;

    case '/kak-kupit.html': // это ЧПУ
        $aSEOData['title'] = 'Как купить товары в интернет-магазине радиоуправляемых моделей vmashin.ru';
        $aSEOData['descr'] = 'Как купить товары в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'как купить интернет магазин описание характеристики цены фото';
      break;

    case '/oplata.html': // это ЧПУ
        $aSEOData['title'] = 'Оплата товара в интернет-магазине радиоуправляемых моделей vmashin.ru';
        $aSEOData['descr'] = 'Оплата товара в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Оплата интернет магазин описание характеристики цены фото';
      break;

    case '/dostavka.html': // это ЧПУ
        $aSEOData['title'] = 'Доставка товара в интернет-магазине радиоуправляемых моделей vmashin.ru';
        $aSEOData['descr'] = 'Доставка товара в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Доставка интернет магазин описание характеристики цены фото';
      break;

    case '/service.html': // это ЧПУ
        $aSEOData['title'] = 'Сервис, ремонт и обслуживание радиоуправляемых моделей: машин, вертолетов, самолетов и других видов радиоуправляемой техники в интернет-магазине радиоуправляемых моделей vmashin.ru';
        $aSEOData['descr'] = 'Сервис, ремонт и обслуживание радиоуправляемых моделей: машин, вертолетов, самолетов и других видов радиоуправляемой техники в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Сервис, ремонт и обслуживание радиоуправляемых моделей: машин, вертолетов, самолетов и других видов радиоуправляемой техники интернет магазин описание характеристики цены фото';
      break;

    case '/credit.html': // это ЧПУ
        $aSEOData['title'] = 'Купи радиоуправляемую модель в КРЕДИТ в интернет-магазине радиоуправляемых моделей vmashin.ru';
        $aSEOData['descr'] = 'Купи радиоуправляемую модель в КРЕДИТ в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Купи радиоуправляемую модель в КРЕДИТ интернет магазин описание характеристики цены фото';
      break;

    case '/adresa-magazinov.html':     // Добавлено 28 April 2014 в  13:06
        $aSEOData['title'] = 'Адреса розничных магазинов | Интернет-магазин радиоуправляемых машин vmashin.ru';
        $aSEOData['descr'] = 'Адреса розничных магазинов. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'адреса розничных магазинов интернет магазин описание характеристики цены фото';
      break;

    case '/garantija.html': // это ЧПУ
        $aSEOData['title'] = 'Гарантия на товары | Интернет-магазин радиоуправляемых машин vmashin.ru';
        $aSEOData['descr'] = 'Гарантия на товары интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Гарантия интернет магазин описание характеристики цены фото';
        $aSEOData['h1'] = 'Гарантия';
      break;

    case '/contacts':     // Добавлено 28 April 2014 в  13:08
        $aSEOData['title'] = 'Обратная связь | Интернет-магазин радиоуправляемых машин vmashin.ru';
        $aSEOData['descr'] = 'Обратная связь интернет-магазина vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'Обратная связь интернет магазин описание характеристики цены фото';
      break;

    case '/': // это ЧПУ
        $sContent = preg_replace('#(<div class="nav-cell">[^<]*)(</div>)#siU', '$1<br />Раскрутка сайта в <a href="http://www.demis-promo.ru/">компании Demis</a> Group$2', $sContent);
        $aSEOData['title'] = 'Радиоуправляемые модели в интернет-магазине vmashin.ru';
        $aSEOData['descr'] = 'Радиоуправляемые модели машин в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'радиомодели магазин радиоуправляемые модели авто купить автомобилей радио интернет магазин';
        $sContent = preg_replace('#(<div class="nav-cell">[^<]+</div>)#siU', '$1 <div>Раскрутка сайтов  <a href="http://www.demis-promo.ru">www.demis-promo.ru</a> на приемлемых условиях.</div>', $sContent);
        break;

    case '/helicopter/dynam.html': // это ЧПУ
        $aSEOData['title'] = 'Вертолеты Dynam в интернет-магазине vmashin.ru';
        $aSEOData['descr'] = 'Вертолеты Dynam по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'вертолеты Dynam интернет магазин описание характеристики цены фото';
        $aSEOData['h1'] = 'Dynam';
      break;

    case '/bike/children-s-bicycles/tricycles-with-handle/icon.html': // это ЧПУ
        $aSEOData['title'] = 'Детские велосипеды с ручкой ICON в интернет-магазине vmashin.ru';
        $aSEOData['descr'] = 'Детские велосипеды с ручкой ICON по доступным ценам в интернет-магазине vmashin.ru. Мы реализуем широкий ассортимент радиоуправляемой техники и осуществляем доставку по всей территории России. К услугам клиентов: ремонт и обслуживание радиоуправляемых моделей, а также гарантия на продукцию и возможность покупки в кредит.';
        $aSEOData['keywr'] = 'детские велосипеды с ручкой icon интернет магазин описание характеристики цены фото';
        $aSEOData['h1'] = 'Детские велосипеды с ручкой ICON по выгодной цене';
      break;      
  }  






$sContent = str_replace('<strong>JavaScript seems to be disabled in your browser.</strong>', '<span>JavaScript seems to be disabled in your browser.</span>', $sContent);



//удалить текст с фильтрованных страниц
if(stripos(DUR_REQUEST_URI, '/where/') !== false ){
  $sContent = preg_replace('#<!--cat-descr-->.*<!--/cat-descr-->#siU', '<!-- cat -->', $sContent);

  if($_SERVER['REQUEST_URI']=='/avtomodeli/where/manufacturer/kyosho.html'){
      $sContent = str_replace('<!-- cat -->', '<h2>Где в Москве купить машинки Kyosho?</h2>
<p>Купить по доступной цене <strong>машинку Kyosho</strong>в Москве можно на сайте «Время Машин». Жителям регионов доступна услуга доставки. Знакомство с моделью известной японской марки не оставит вас равнодушным!</p>
<p>Знаменитый японский производитель радует своих поклонников яркими и удобными в управлении автомобилями. Купить Kyosho могут коллекционеры из Москвы и других городов. В нашем каталоге машины этой марки представлены во всем многообразии: от моделей с электромотором до авто на нитродвигателе! Здесь можно купить игрушку для новичка или мощный багги Kyosho для опытного гонщика. Возможна доставка по Москве и в регионы страны проверенной транспортной компанией.</p>
<h2>Хотите купить автомобиль Kyosho в Москве? Обратите внимание на достоинства моделей этой марки:</h2>
<ul>
<li>поразительная точность дизайна, великолепное сходство с реальными авто;</li>
<li>удобный пульт управления;</li>
<li>мощные двигатели для развития высоких скоростей;</li>
<li>ударопрочный корпус;</li>
<li>высокие характеристики.</li>
</ul>
<p>Хотите поразить друзей роскошной радиоуправляемой моделью известной марки? Купите качественный автомобиль Kyosho в Москве, сделав заказ в онлайн-магазине «Время Машин». Знакомьтесь с моделями и заказывайте!</p>', $sContent);
  }
}
// if(preg_match('#<!--manuf-filter-->.*<option[^>]+selected[^>]*>.*<!--/manuf-filter-->#siU', $sContent) || preg_match('#<!--checkbox-filter-->.*<input[^>]+checked="checked"[^>]*>.*<!--/checkbox-filter-->#siU', $sContent)){
//   $sContent = preg_replace('#<!--cat-descr-->.*<!--/cat-descr-->#siU', '', $sContent);
// }












if(stripos($sContent, 'Данная страница не найдена!') ){
  return $sContent;
}
//Обработка
  function changeHeadBlock ($sContent, $sRegExp, $sBlock) {
    if (preg_match($sRegExp, $sContent)) {
      return preg_replace($sRegExp, $sBlock, $sContent);
    }
    else {
      return str_replace('<head>', '<head>' . $sBlock, $sContent);
    }
  }
  if (isset($aSEOData['title']) && !empty($aSEOData['title'])) {
    $aSEOData['title'] = htmlspecialchars($aSEOData['title']);
    $sContent = changeHeadBlock($sContent, '#<title>.*</title>#siU', '<title>' . $aSEOData['title'] . '</title>');
  }
  if (isset($aSEOData['descr']) && !empty($aSEOData['descr'])) {
    $aSEOData['descr'] = htmlspecialchars($aSEOData['descr']);
    $sContent = changeHeadBlock($sContent, '#<meta[^>]+name[^>]{1,7}description[^>]*>#siU', '<meta name="description" content="' . $aSEOData['descr'] . '" />');
  }
  if (isset($aSEOData['keywr']) && !empty($aSEOData['keywr'])) {
    $aSEOData['keywr'] = htmlspecialchars($aSEOData['keywr']);
    $sContent = changeHeadBlock($sContent, '#<meta[^>]+name[^>]{1,7}keywords[^>]*>#siU', '<meta name="keywords" content="' . $aSEOData['keywr'] . '" />');
  }
  if(isset($_SERVER['X_DUSYA']) || isset($_SERVER['HTTP_X_DUSYA'])) {
  $sContent = str_replace('<head>', '<head><!--origUrl="' . $sSEOUrl . '"-->' , $sContent);
  }
  if (isset($aSEOData['h1']) && !empty($aSEOData['h1'])) {
  $sContent = preg_replace('#(<h1[^>]*>).*(</h1>)#siU', '$1'.$aSEOData['h1'].'$2', $sContent);
  }
  if (isset($aSEOData['text']) && !empty($aSEOData['text'])) {
  //$sContent = preg_replace('##siU', '', $sContent);
  }
  if (isset($aSEOData['text_alt']) && !empty($aSEOData['text_alt'])) {
  //$sContent = preg_replace('##siU', '', $sContent);
  }
?>
