<?php 
//error_reporting(E_ALL | E_STRICT);

//ob_start();
include_once ('app/Mage.php');
Mage::run();
//ob_clean();
$_collection = Mage::getModel('catalog/product')->getCollection();


/*
// Load the Magento core
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
 
// Load the product collection
 
$_collection = Mage::getModel('catalog/product')
 ->getCollection();
// ->addAttributeToSelect('*') //Select everything from the table
// ->addUrlRewrite(); //Generate nice URLs
*/

$file_path = "var/export/super_export.csv";

//class CSV
$mage_csv = new Varien_File_Csv();
$products_row = array(); 

foreach($_collection as $_prod){
        $prod_id = $_prod->getId(); 
        $product = Mage:: getModel('catalog/product')->load($prod_id);
        $_categories = $product->getCategoryIds();

      $cat_id = end($_categories);

            $_category = Mage::getModel('catalog/category')->load($cat_id);
            $url = 'http://vmashin.ru/'.$product->getUrlPath($_category);

        $data = array();         
          $data['name'] = $product->getName();    
          $data['sku'] = $product->getSku();
          $data['stock'] = (!$product->getIsInStock()) ? 'нет' : 'есть';    
          $data['status'] = ($product->getStatus() == 2) ? 'выкл' : 'вкл';
          $data['url'] = $url; 
          $products_row[] = $data;        
}

$name_arr = array('НАЗВАНИЕ', 'АРТИКУЛ', 'НАЛИЧИЕ', 'СТАТУС', 'УРЛ');
array_unshift($products_row, $name_arr);

//записываем в CSV файл
$mage_csv->saveData($file_path, $products_row);
?>
