<?php 
error_reporting(E_ALL);
ob_start();
include_once ('app/Mage.php');
Mage::run();
ob_clean();

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

 $category = Mage::getModel('catalog/category');
$tree = $category->getTreeModel();
$tree->load();
$ids = $tree->getCollection()->getAllIds(); 
$categories_row = array();
foreach($ids as $cat){
  //  $category_id = $cat->getId();
    $category = Mage::getModel('catalog/category')->load($cat); //returns a category object
    $data = array("name" => "","h1" => "", "page_title" => "", "is_active" => "", "full_url" => "");
    $data['name'] = $category['name'];
    if($data['name'] == 'Root Catalog' or $data['name'] == 'Default Category') continue;
    $data['h1'] = $category['h1'];
    $data['page_title'] = $category['meta_title'];
    $data['is_active'] = ($category['is_active']) ? 'ДА' : 'НЕТ'; 
    $data['full_url'] = 'http://vmashin.ru/'.$category['url_path'];
    $categories_row[] = $data;
}
$name_arr = array('НАЗВАНИЕ', 'ЗАГОЛОВОК H1', 'PAGE TITLE', 'СТАТУС (ВКЛ/ВЫКЛ)', 'ПОЛНЫЙ УРЛ');
array_unshift($categories_row, $name_arr);
$file_path = "var/export/export_category.csv";

//class CSV
$mage_csv = new Varien_File_Csv();
//записываем в CSV файл
if($mage_csv->saveData($file_path, $categories_row)){
    $alert = "Данные успешно сохранены";
}else{
    $alert = "Данные не сохранены, попробуйте повторить попытку.";
}?>
    <script type ="text/javascript">
        alert('<?=$alert;?>');
    </script><?php
//header("Location: http://vmashin.ru");
?>
