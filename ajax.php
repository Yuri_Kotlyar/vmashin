<?php
//error_reporting(E_ALL);
    ob_start();
    include_once ('app/Mage.php');
    Mage::run();
    ob_clean();
    
    $_cartQty = strip_zero(Mage::getModel('checkout/cart')->getQuote()->getItemsQty());
    $_getSubtotal = strip_zero(Mage::getModel('checkout/cart')->getQuote()->getGrandTotal());;

?>
<div class="mod-cart-info">
                <?php if ($_cartQty==1): ?>
                        <u>1</u> товар = <u><?php echo $_getSubtotal; ?></u> руб.
                <?php else: ?>
                        <u><?php echo $_cartQty; ?></u> товаров = <u><?php echo $_getSubtotal; ?></u> руб.
                <?php endif ?>
        <button type="button" class="button" onclick="location.href='/checkout/cart/'">Перейти в корзину</button>
</div>
<?php
    function strip_zero($number,$count = 0){
        $point_pos = strpos($number, '.');
        $final_char = $point_pos + $count;
        if($count > 0) $final_char++;
        return substr($number, 0, $final_char);
    }
?>