<?php 
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
set_time_limit(35000);
ini_set('memory_limit', '1024M');

include_once ('app/Mage.php');
Mage::app();

include_once ('app/code/local/SLab/LongProductUrl/Model/Observer.php');

$obs = new SLab_LongProductUrl_Model_Observer();

$obs->reindexRewriteUrlToLong();
echo 'Reindex finished';

?>