/**
 * @category    Mana
 * @package     Mana_Admin
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
// the following function wraps code block that is executed once this javascript file is parsed. Lierally, this 
// notation says: here we define some anonymous function and call it once during file parsing. THis function has
// one parameter which is initialized with global jQuery object. Why use such complex notation: 
// 		a. 	all variables defined inside of the function belong to function's local scope, that is these variables
//			would not interfere with other global variables.
//		b.	we use jQuery $ notation in not conflicting way (along with prototype, ext, etc.)
(function(window, $) {
	// the following function is executed when DOM ir ready. If not use this wrapper, code inside could fail if
	// executed when referenced DOM elements are still being loaded.
	$(function() {
		// UI logic for "use default value" checkboxes
		$('input.m-default').click(function() {
			var fieldId = this.id.substring(0, this.id.length - '_default'.length);
			if ($('#'+fieldId).length) {
				if (this.checked) {
					$('#'+fieldId).attr('disabled', true).addClass('disabled');
				}
				else {
					$('#'+fieldId).removeAttr('disabled').removeClass('disabled').focus();
				}
			}
			else {
				throw 'Field for editing not found!';
			}
		});
		
		// UI logic for standard buttons
		$('button.m-close-action').click(function() {
			window.location.href = $.options('button.m-close-action').redirect_to;
		});
		$('button.m-save-action').click(function() {
			var request = [];
			if ($.options('edit-form') && $.options('edit-form').subforms) {
				$.each($.options('edit-form').subforms, function(index, formId) {
					$.extend(request, $(formId).serializeArray());
				});
			}
			//window.Varien.showLoading();
			$('#loading-mask').show();
			$.post($.options('button.m-save-action').action, request)
				.done(function (response) {
					try {
						response = $.parseJSON(response);
						$.dynamicUpdate(response.update);
					}
					catch (error) {
						$.errorUpdate($.options('edit-form').messagesSelector, error);
					}
				})
				.fail(function (error) {
					$.errorUpdate($.options('edit-form').messagesSelector, error);
				})
				.complete(function() {
					//window.Varien.hideLoading();
					$('#loading-mask').hide();
				});
		});
		$('button.m-save-and-close-action').click(function() {
			var request = [];
			if ($.options('edit-form') && $.options('edit-form').subforms) {
				$.each($.options('edit-form').subforms, function(index, formId) {
					$.extend(request, $(formId).serializeArray());
				});
			}
			//window.Varien.showLoading();
			$('#loading-mask').show();
			$.post($.options('button.m-save-and-close-action').action, request)
				.done(function (response) {
					try {
						response = $.parseJSON(response);
						$.dynamicUpdate(response.update);
						window.location.href = $.options('button.m-save-and-close-action').redirect_to;
					}
					catch (error) {
						$.errorUpdate($.options('edit-form').messagesSelector, error);
					}
				})
				.fail(function (error) {
					$.errorUpdate($.options('edit-form').messagesSelector, error);
				})
				.complete(function() {
					//window.Varien.hideLoading();
					$('#loading-mask').hide();
				});
		});
	});
})(window, jQuery);
