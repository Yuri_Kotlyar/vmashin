var j_ready_safe_load = true;
jQuery(document).ready(function(){	
	if(j_ready_safe_load)
		{
			//защита от дублирования ready
			initStart();
			initValidateEvents();
			initEvents();
			createAutocomplite('postcode');	
			j_ready_safe_load = false;
		}
});

function initTab(tab) {
	var init_obj = onsRequiredField[tab];
	jQuery('li .for-required').removeClass('required-entry required');
	jQuery.each(init_obj, function(indx, element){
		  if(element == '2')
			  { 
			  	jQuery('li.ons-' + indx + ' .for-required').addClass('required-entry required');
			  }
	});
	if(init_obj['pickup'] == '1')
		{
			jQuery("#pickup-field").show();
		} else {
			jQuery("#pickup-field").hide();
		}
	checkPickup();
}

function checkPickup()
{
	if(jQuery("#customer-pickup").is(':checked') && jQuery("#customer-pickup").is(':visible'))
	{
		jQuery('li.address-no-pickup').hide();
	} else { 
		jQuery('li.address-no-pickup.'+ jQuery('#customer-region-select').val()).show();
	}	
}

function createAutocomplite(id) {

	if(autoComplete == 0) return ;
	
	jQuery('#'+id).autocomplete({
		source: autoCompleteUrl,
		minLength: 4,
		select: function( event, ui ) {
				setPostcode( ui.item ?
						ui.item.index :
						this.value 
						);
				setCity( ui.item ?
					ui.item.city :
					this.value 
					);
				setRegion( ui.item ?
						ui.item.region :
						this.value 
						);
				focusInId("textarea[name='billing[street][]']");
				reloadShippingMethod();
				return false;
			},
			change: function( event, ui ) {
				setPostcode( ui.item ?
						ui.item.index :
						this.value 
						);
				setCity( ui.item ?
					ui.item.city :
					null 
					);
				setRegion( ui.item ?
						ui.item.region :
						null 
						);
				reloadShippingMethod();
				return false;
			},
	});
}
function setCity( message ) {
	jQuery( "input[name='billing[city]']").val(message); 
}
function setRegion( message ) {
	if(jQuery("input[name='billing[region]']").is(':visible'))
		{
			jQuery("input[name='billing[region]']").val(message);
		}	
}
function setPostcode( message ) {
	jQuery( "input[name='billing[postcode]']").val(message);
}
function focusInId(id) {
	jQuery(id).focus();
}

function initStart() {
	jQuery("#customer-region-select [value='" + init_tab + "']").attr("selected", "selected");
	jQuery('li.address-no-pickup').hide();
	
	if(init_tab == '')
		{
			jQuery("#pickup-field").hide();
		} else {
			jQuery('li.'+ init_tab).show();
			initTab(init_tab);
		}
	
	if(jQuery("#billing-new-address-form-register").length )
		{
			if(jQuery("#billing-new-address-form-register input:radio:checked").val() != '')
				{
					addressRadioAction();
				}
		}	
	
	paymentShowHelper();
	
	//показываем отмену купона
	if(jQuery('#fast-sertificat input').val())
	{
		jQuery('#fast-cancel-coupon').show();
	}
}

function prepareRegion()
{
	jQuery('li.address-no-pickup').hide();
	if(jQuery("#customer-region-select").val() == '')
		{
			jQuery("#pickup-field").hide();	
		} else {
			jQuery('li.'+jQuery("#customer-region-select").val()).show();
			initTab(jQuery("#customer-region-select").val());
			jQuery('li.address-no-pickup .input-text').val('');
			reloadShippingMethod();
		}
}

function initEvents()
{ 
	jQuery("#customer-region-select").bind('keydown', function()
			{
				jQuery("#customer-region-select").data('beforetab', jQuery("#customer-region-select").val());
			} 
	);
	jQuery("#customer-region-select").bind('keyup', function()
			{
				if(jQuery("#customer-region-select").data('beforetab') == jQuery("#customer-region-select").val()) return ; //false key
				prepareRegion();
			} 
	);
	jQuery("#customer-region-select").bind('change', function()
			{
				prepareRegion();
			} 
	);
	
	jQuery("#customer-pickup").bind("click", function(){
			checkPickup();
			reloadShippingMethod();
		});
	jQuery("#billing-new-address-form select.for-required:visible").live("change keyup", function(){
			reloadShippingMethod(); //reload shipping
	});

	//if no autocomplete postcode
	jQuery("#postcode").live("keyup", function(){ 
		if(jQuery('#postcode').val().length > 5  && jQuery('#postcode').val().length < 7)
		{			
			reloadShippingMethod();						
		}
	});
	
	jQuery("#fast-shipping-loader").delegate("input:radio","click", function(){
		saveShippingMethod();
	});
	
	jQuery("#fast-payment-loader").delegate("input:radio","click", function(){ 
		savePaymentMethod();
	});
	
	//событие на нажатие на логин
	jQuery("#fast-login div.block-title").bind("click", function(){
			jQuery('#fast-login div.block-content').slideToggle("normal", function() {
				jQuery("#mini-login").focus();
			  });
		});
	
	//событие на отправку авторизации
	jQuery("#fast-login button").bind("click", function(){	
		loginAction();		
	});
	jQuery("#fast-login input[name='login[password]']").keyup(function(e) {
        if(e.keyCode == 13){
        	loginAction();
        }
      });
	
	jQuery("#fast-add-coupon").bind("click", function(){
		couponAction(0);
	});
	jQuery("#fast-sertificat .input-text").keyup(function(e) {
        if(e.keyCode == 13){
        	couponAction(0);
        }
      });
	jQuery("#fast-cancel-coupon").bind("click", function(){
		couponAction(1);
	});
	
	jQuery("#billing-new-address-form-register input:radio").bind("click", function(){
		addressRadioAction();
	});
	
	jQuery("#billing-buttons-container button").live("click", function(){
		if(validateBeforeSave())
			{
				jQuery('#co-billing-form').submit();
			}
		return false;
	});
	
	jQuery('#checkout-agreements input:checkbox').live("click", function(){
		if(jQuery(this).is(':checked'))
		{
			jQuery(this).parent().removeClass("fast-error-field");
		}		
	});
	
//	jQuery("#billing-new-address-form textarea").live("keyup", function(){
//		reloadShippingMethod(); //reload shipping
//});
	
	//add this custom events
}

function initValidateEvents() {
	//validate required field
	jQuery("#billing-new-address-form .required-entry:visible").live("focusout", function(){			
			if(jQuery(this).val() == "" )
			{
				jQuery(this).addClass("fast-error-field");
			} else {
				jQuery(this).removeClass("fast-error-field");
			}
	});
	//validate email
	jQuery("#billing-new-address-form .validate-email:visible").live("focusout", function(){		
			if(isEmail(jQuery(this).val()))
			{
				jQuery(this).removeClass("fast-error-field");
			} else {					
				jQuery(this).addClass("fast-error-field");
			}			
	});
	//validate postcode
	jQuery("#billing-new-address-form .validate-zip-international.required-entry:visible").live("focusout", function(){		
		if(jQuery(this).val().length > 5  && jQuery(this).val().length < 7 && jQuery(this).val() > 0)	
			{
				jQuery(this).removeClass("fast-error-field");
			} else {					
				jQuery(this).addClass("fast-error-field");
			}	
	});
	//validate select focusout
	jQuery("#billing-new-address-form select.for-required.required-entry.validate-select:visible").live("focusout", function(){
		if(jQuery(this).val() == "")
		{
			jQuery(this).addClass("fast-error-field");
		} else {
			jQuery(this).removeClass("fast-error-field");
		}	
	});
	//validate select change keyup
	jQuery("#billing-new-address-form select.for-required.required-entry.validate-select:visible").live("change keyup", function(){
		if(jQuery(this).val() == "")
		{
			jQuery(this).addClass("fast-error-field");
		} else {
			jQuery(this).removeClass("fast-error-field");
		}	
	});
}

function reloadShippingMethod()
{
	if(goajax) return;
		
	goajax = 1;
	showLoaderShipping();
	showLoaderPayment();
	showLoaderTotal();
	
	jQuery.getJSON(saveaddressurl, agregateFormData('#fast-adress'),			
			function(data) {
				if(data.error == 1)
				{
					showTopError(data.message);
				} else {
					showTopError('');
					jQuery('#fast-shipping').html(data.shipping);
					jQuery('#fast-payment-methods').html(data.payment);	
					jQuery('#fast-total').html(data.totals);
					paymentShowHelper();
					}
				}
			).complete(function() { 
				hideLoaderShipping();
				hideLoaderPayment();
				hideLoaderTotal();
				goajax = 0;}
			);
	return ;
}

function saveShippingMethod()
{
	if(goajax) return;
	
	jQuery('#fast-shipping-methods .active-radio').removeClass('active-radio');
	jQuery(this).parent().addClass('active-radio');	
		
	showLoaderPayment();
	showLoaderTotal();
	goajax = 1;
	
	jQuery.getJSON(saveshipurl, agregateFormData('#fast-shipping'),
			function(data) {
				if(data.error == 1)
				{
					showTopError(data.message);
				} else {
					showTopError('');
					jQuery('#fast-payment-methods').html(data.payment);	
					jQuery('#fast-total').html(data.totals);
					paymentShowHelper();
					}	
		
			}).complete(function() { 
				hideLoaderPayment();
				hideLoaderTotal();
				jQuery("#payment_form_"+jQuery("#checkout-payment-method-load input:radio:checked").val()).show();			
				goajax = 0;
				}
		);
}

function savePaymentMethod()
{
	if(goajax) return;	
 
	jQuery("#checkout-payment-method-load .form-list").hide();
	jQuery("#checkout-payment-method-load div.fast-method-color").removeClass("fast-method-color-true");
	jQuery("#"+jQuery("#checkout-payment-method-load input:radio:checked").val()+"-method").addClass("fast-method-color-true");
	jQuery("#payment_form_"+jQuery("#checkout-payment-method-load input:radio:checked").val()).show();
	showLoaderTotal();
	goajax = 1;
	jQuery.getJSON(savepayurl, /*agregateFormData('#fast-payment')*/{"payment[method]": jQuery("#fast-payment-methods input:radio:checked").val()},		
		function(data) {
				if(data.error == 1)
				{
					showTopError(data.message);
				} else {
					showTopError('');	
					jQuery('#fast-total').html(data.totals);				
				}	
	}).complete(function() { 
		hideLoaderTotal();		
		goajax = 0;
		}
	);				
}

function loginAction()
{
	if(goajax) return;
	goajax = 1;
	jQuery("#fast-login input[name='login[password]']").addClass('ui-autocomplete-loading');
	jQuery.getJSON(loginaddressurl , 
			{
				"login[username]": jQuery("#fast-login input[name='login[username]']").val(),
				"login[password]": jQuery("#fast-login input[name='login[password]']").val(),
			},
			function(data) {
				if(data.error == 1)
				{
					showTopError(data.message);
				} else {
					showTopError('');
					location.reload();
				}
			}
			).complete(function() {
				goajax = 0;	
				jQuery("#fast-login input[name='login[password]']").removeClass('ui-autocomplete-loading');
			});
}

function couponAction(coupon_a)
{
	if(!jQuery("#fast-sertificat input").val())
	{
		jQuery("#fast-sertificat input").addClass("fast-error-field");
		return ;
	}

	showLoaderTotal();
	if(goajax) return;
	goajax = 1;
	jQuery.getJSON(couponurl , 
			{
				"coupon_code": jQuery("#fast-sertificat input").val(),
				"remove": coupon_a
			},

			function(data) {
				if(data.error == 1)
				{
					showTopError(data.message);
					jQuery("#fast-sertificat input").removeClass("fast-success-field");
					jQuery("#fast-sertificat input").addClass("fast-error-field");
				} else {
					showTopError('');
					jQuery('#fast-total').html(data.totals);
					
					if(coupon_a == 0)
						{
							if(data.success == 1)
							{
								jQuery("#fast-sertificat input").removeClass("fast-error-field");
								jQuery("#fast-sertificat input").addClass("fast-success-field");
								jQuery("#fast-cancel-coupon").show();
								showTopInfo(data.message, 'alert-success');
							} else {
								jQuery("#fast-sertificat input").removeClass("fast-success-field");
							}
						}
					
					if(coupon_a == 1)
						{
							if(data.cancel == 1)
							{
							jQuery("#fast-sertificat input").removeClass("fast-error-field");
							jQuery("#fast-sertificat input").removeClass("fast-success-field");
							jQuery("#fast-cancel-coupon").hide();
							jQuery("#fast-sertificat input").val('');
							showTopInfo(data.message, 'alert-info');
							} else {
								jQuery("#fast-sertificat input").removeClass("fast-success-field");
							}
						}
					}				
			}
			).complete(function() {
				hideLoaderTotal();
				goajax = 0;						
			});	
}

function addressRadioAction()
{
	if(jQuery("#billing-new-address-form-register input:radio:checked").val() == '')
	{
		jQuery('#billing-new-address-form').show();
		jQuery("#billing-new-address-form-register input:radio").parent().removeClass('fast-method-color-true');
		jQuery("#fast-shipping-methods input:radio:checked").removeAttr('checked');
	} else {
		jQuery('#billing-new-address-form').hide();
		jQuery("#billing-new-address-form-register input:radio").parent().removeClass('fast-method-color-true');
		jQuery(this).parent().addClass('fast-method-color-true');		
	}
	reloadShippingMethod();
}

function agregateFormData(id)
{
	var filterData = new Object(); //объект с параметрами запроса
	
	jQuery(id + ' input.input-text:visible').each(function(){
		filterData[jQuery(this).attr('name')] = jQuery(this).val();
	});
	jQuery(id + ' select:visible').each(function(){
		filterData[jQuery(this).attr('name')] = jQuery(this).val();
	});
	jQuery(id + ' textarea:visible').each(function(){
		filterData[jQuery(this).attr('name')] = jQuery(this).val();
	});	
	jQuery(id + ' input.checkbox:visible:checked').each(function(){
		filterData[jQuery(this).attr('name')] = '1';
	});
	jQuery(id + " input[type='hidden']").each(function(){
		filterData[jQuery(this).attr('name')] = jQuery(this).val();
	});
	jQuery(id + " input.radio:checked").each(function(){
		filterData[jQuery(this).attr('name')] = jQuery(this).val();
	});
	return filterData;
}

function validateBeforeSave()
{
	var e_obj = new Array();

	//check address
	jQuery('#co-billing-form .required-entry:visible').each(function(){
		if(jQuery(this).val() == "" && !jQuery(this).hasClass('validate-email'))
			{
				jQuery(this).addClass("fast-error-field");
				if(jQuery(this).attr('data-error'))
					{
						e_obj.push(jQuery(this).attr('data-error'));
					}
			}
		if(jQuery(this).hasClass('validate-email') && !isEmail(jQuery(this).val()))
			{
				jQuery(this).addClass("fast-error-field");
				if(jQuery(this).attr('data-error'))
				{
					e_obj.push(jQuery(this).attr('data-error'));
				}
			}		
	});
	
	if(jQuery("#billing-new-address-form-register").length )
	{
		if(!jQuery("#billing-new-address-form-register input:radio:checked").length)
		{
			e_obj.push(onsWarningMsg['check_address']);
		} 
	}
	
	if(jQuery("#fast-shipping-methods").length && !jQuery("#fast-shipping-methods input:radio:checked").length)
	{
		e_obj.push(onsWarningMsg['check_shipping_method']);
	} 
	
	jQuery('#checkout-agreements input:checkbox').each(function(){
		if(!jQuery(this).is(':checked'))
			{
				e_obj.push(onsWarningMsg['check_agree']);
				jQuery('#checkout-agreements .agree').addClass("fast-error-field");
			}
	});
	
	if(!jQuery("#fast-payment input:radio:checked").length)
	{
		e_obj.push(onsWarningMsg['check_payment_method']);
	} 
	
	/**************************pickpoint*******************************/
	if(jQuery("#fast-shipping-methods input:radio:checked").val() == 'pickpoint_pickpoint' && !jQuery("#pickpoint-id").val().length)
		{
			e_obj.push('Пожалуйста, выберите Постамат!'); 
		}	
	
	return showError(e_obj);
}

function showError(e_obj)
{
	if(e_obj.length)
		{
			jQuery('#fast-error').html('');
			jQuery(e_obj).each(function(indx, element){
				jQuery('#fast-error').show();
				jQuery('#fast-error').append('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+element+'</p></div>');
			});
			jQuery('#fast-error .alert').alert();
			return false;
		} else {
			jQuery('#fast-error .alert').alert('close');
			jQuery('#fast-error').html('').hide();			
			jQuery('#co-billing-form .fast-error-field').removeClass('fast-error-field');
		}
	return true;
}

function showTopError(et_html)
{
	if(et_html.length)
	{
		jQuery('#fast-error-top').html('');
		jQuery('#fast-error-top').show();
		jQuery('#fast-error-top').append('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+et_html+'</p></div>');
		jQuery('#fast-error-top .alert').alert();
		return false;
	} else {		
		jQuery('#fast-error-top .alert').alert('close');
		jQuery('#fast-error-top').html('').hide();
	}
	return true;
}

function showTopInfo(ei_html, ei_class)
{
	if(ei_html.length)
	{
		jQuery('#fast-error-top').html('');
		jQuery('#fast-error-top').show();
		jQuery('#fast-error-top').append('<div class="alert alert-block '+ ei_class +' fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+ei_html+'</p></div>');
		jQuery('#fast-error-top .alert').alert();
		return false;
	} else {		
		jQuery('#fast-error-top .alert').alert('close');
		jQuery('#fast-error-top').html('').hide();
	}
	return true;
}

function paymentShowHelper()
{
	jQuery("#checkout-payment-method-load .form-list").hide();
	jQuery("#payment_form_"+jQuery("#checkout-payment-method-load input:radio:checked").val()).show();
	jQuery("#"+jQuery("#checkout-payment-method-load input:radio:checked").val()+"-method").addClass("fast-method-color-true");
}

function implode( glue, pieces ) {	// Join array elements with a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: _argos

	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}


function isEmail( text )
{
	reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
	if (text.match(reg))
		{
			return true;
		}
	return false;
}
function showLoaderShipping()
{
	jQuery('#fast-shipping-loader').addClass('fast-loader');
}
function hideLoaderShipping()
{
	jQuery('#fast-shipping-loader').removeClass('fast-loader');
}
function showLoaderPayment()
{
	jQuery('#fast-payment-loader').addClass('fast-loader');
}
function hideLoaderPayment()
{
	jQuery('#fast-payment-loader').removeClass('fast-loader');
}
function showLoaderTotal()
{
	jQuery('#fast-total-loader').addClass('fast-loader');
}
function hideLoaderTotal()
{
	jQuery('#fast-total-loader').removeClass('fast-loader');
}
function showLoaderAddress()
{
	jQuery('#fast-adress-loader').addClass('fast-loader');
}
function hideLoaderAddress()
{
	jQuery('#fast-adress-loader').removeClass('fast-loader');
}