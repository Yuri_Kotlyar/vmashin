;
(function($) {
    var methods = {
        init : function(options) {
            //add index action
            $(this).find('i').each(function(indx, element) {
                    $(element).html(indx+1);
                });
            $(document).data('onestepcheckout_options', options);
            initStart();
            initValidateEvents();
            initEvents();
            createAutocomplite('postcode');
        }
    };

    var goajax = 0;

    function initStart() {
        jQuery('li.address-no-pickup').hide();

        if(_getData('init_tab') == '') {
            jQuery("#pickup-field").hide();
        } else {
            jQuery('li.'+ _getData('init_tab')).show();
            initTab(_getData('init_tab'));
        }

        if(jQuery("#billing-new-address-form-register").length) {
                if(jQuery("#billing-new-address-form-register input:radio:checked").val() != '') {
                        addressRadioAction();
                    }
            }
        shippingShowHelper();
        paymentShowHelper();
        regionUpdate();
        isHideCity();

        if(_getData('phoneMaskEnabled') == '1') {
            jQuery("#billing-telephone").inputmask("mask", {"mask": _getData('phoneMask')});
        }

        //показываем отмену купона
        if(jQuery('#onestepcheckout-sert').val()) {
            jQuery('#onestepcheckout-cancel-coupon').show();
        }
    }

    //function
    function initEvents()
    {
         jQuery(document).on("change keyup", "select[name='billing[country_id]']", function(){
             regionUpdate();
             reloadShippingMethod();
         });

         jQuery(document).on("change keyup", "select[name='billing[region_id]']", function(){
             prepareRegion();
             reloadShippingMethod();
         });

         jQuery("#customer-pickup").on("click", function(){
             reloadShippingMethod();
             checkPickup();
             isHideCity();
         });

         jQuery("#billing-new-address-form select.for-required").on("change keyup", function(){
             reloadShippingMethod(); //reload shipping
         });

       //if no autocomplete postcode
         jQuery("#postcode").on("keyup", function(){
             if(jQuery('#postcode').val().length > 5  && jQuery('#postcode').val().length < 7) {
                 reloadShippingMethod();
             }
         });

         jQuery(document).on("click",".onestepcheckout-shipping-container input:radio", function(){
             saveShippingMethod();
         });

         jQuery(document).on("click",".onestepcheckout-payment-container input:radio", function(){
             savePaymentMethod();
         });

         jQuery("#onestepcheckout-add-coupon").on("click", function(){
             couponAction(0);
         });

         jQuery("#onestepcheckout-sertificat .input-text").keyup(function(e) {
             if(e.keyCode == 13){
                 couponAction(0);
             }
           });

         jQuery("#onestepcheckout-cancel-coupon").on("click", function(){
             couponAction(1);
         });

         jQuery(".onestepcheckout-checout-button-container button").on("click", function(){
             if(validateBeforeSave()) {
                     jQuery('#co-billing-form').submit();
                 }
             return false;
         });

         jQuery(".onestepcheckout-login > a").on("click", function(){
             jQuery('.onestepcheckout-login div.onestepcheckout-login-content').slideToggle("fast", function() {
                 jQuery("#mini-login").focus();
               });
         });

         jQuery("#billing-new-address-form-register input:radio").on("click", function(){
             addressRadioAction();
         });

         jQuery('#checkout-agreements input:checkbox').on("click", function(){
             if(jQuery(this).is(':checked')) {
                 jQuery(this).parent().removeClass("onestepcheckout-error-field");
             }
         });

         jQuery(".onestepcheckout-login .button-login").on("click", function(){
             loginAction();
         });

         jQuery(".onestepcheckout-login input[name='login[password]']").keyup(function(e) {
             if(e.keyCode == 13){
                 loginAction();
             }
           });

         if(_getData('city_event') == '1') {
             jQuery(document).on("keyup", "#billing-city", function(){
                 reloadShippingMethod();
              });
         }

         if(_getData('city_event') == '2') {
             jQuery(document).on("focusout", "#billing-city", function(){
                 reloadShippingMethod();
              });
         }

         if(_getData('city_event') == '3') {
             jQuery(document).on("keyup focusout", "#billing-city", function(){
                 reloadShippingMethod();
              });
         }
        //add this custom events
    }

    function regionUpdate()
    {
        region_current_value = countryRegions[jQuery("#billing-new-address-form select[name='billing[country_id]']").val()];
        if(region_current_value) {
                __region_select = jQuery("#billing-new-address-form select[name='billing[region_id]']");
                jQuery("#billing-new-address-form input[name='billing[region]']").hide();

                //reset all value
                __region_select.empty();
                if(jQuery("#billing-new-address-form select[name='billing[country_id]']").val() == 'RU') {
                        __region_select.append(countryRegionsRU);
                    } else {
                        jQuery.each(region_current_value, function(indx, element){
                            __region_select.append('<option value="'+ indx +'">'+ element.name +'</option>');
                        });
                    }

                //add null element
                __region_select.prepend('<option value=""></option>');
//                __region_select.find("option[value='']").attr("selected", "selected");
                //set current value
                __region_select.find("option[value='"+ _getData('currentRegion') +"']").attr("selected", "selected");

                if(_getData('isChosenEnabled') == '1') {
                        __region_select.chosen({no_results_text: "Нет результатов"}).trigger("liszt:updated");
                        jQuery('.chzn-container-single .chzn-drop').css('width', jQuery('#ul-onestepcheckout-address').width() - 20);
                        jQuery('.chzn-container-single .chzn-search input').css('width', jQuery('#ul-onestepcheckout-address').width() - 47);
                        jQuery("#billing_region_id_chzn").show();
                    } else {
                        __region_select.show();
                    }
            } else {
                jQuery("#billing-new-address-form select[name='billing[region_id]']").hide();
                jQuery("#billing_region_id_chzn").hide();
                jQuery("#billing-new-address-form input[name='billing[region]']").show();
            }
    }

    function reloadShippingMethod()
    {
        if(goajax) return;

        goajax = 1;
        disableRadioSelect();
        showLoaderShipping();
        showLoaderPayment();
        showLoaderTotal();

        jQuery.getJSON(_getData('saveaddressurl'), agregateFormData('#ul-onestepcheckout-address'),
                function(data) {
                    if(data.error == 1) {
                        showTopError(data.message);
                    } else {
                        showTopError('');
                        jQuery('#onestepcheckout-shipping-methods').html(data.shipping);
                        jQuery('#onestepcheckout-payment-methods').html(data.payment);
                        jQuery('.onestepcheckout-totals-container').html(data.totals);
                        paymentShowHelper();
                        shippingShowHelper();
                        }
                    }
                ).complete(function() {
                    hideLoaderShipping();
                    hideLoaderPayment();
                    hideLoaderTotal();
                    enableRadioSelect();
                    goajax = 0;}
                );
        return ;
    }

    function saveShippingMethod()
    {
        if(goajax) return;

        goajax = 1;
        disableRadioSelect();
        showLoaderPayment();
        showLoaderTotal();
        shippingShowHelper();

        jQuery.getJSON(_getData('saveshipurl'), agregateFormData('#onestepcheckout-shipping-methods'),
                function(data) {
                    if(data.error == 1) {
                        showTopError(data.message);
                    } else {
                        showTopError('');
                        jQuery('#onestepcheckout-payment-methods').html(data.payment);
                        jQuery('.onestepcheckout-totals-container').html(data.totals);
                        paymentShowHelper();
                        }

                }).complete(function() {
                    hideLoaderPayment();
                    hideLoaderTotal();
                    enableRadioSelect();
                    goajax = 0;
                    }
            );
    }

    function savePaymentMethod()
    {
        if(goajax) return;

        goajax = 1;
        disableRadioSelect();
        paymentShowHelper();
        showLoaderTotal();

        jQuery.getJSON(_getData('savepayurl'), /*agregateFormData('#fast-payment')*/{"payment[method]": jQuery("#onestepcheckout-payment-methods input:radio:checked").val()},
            function(data) {
                    if(data.error == 1) {
                        showTopError(data.message);
                    } else {
                        showTopError('');
                        jQuery('.onestepcheckout-totals-container').html(data.totals);
                    }
        }).complete(function() {
            hideLoaderTotal();
            enableRadioSelect();
            goajax = 0;
            }
        );
    }

    function addressRadioAction()
    {
        if(jQuery("#billing-new-address-form-register input:radio:checked").val() == '') {
            jQuery('#billing-new-address-form').show();
            jQuery("#billing-new-address-form-register input:radio").parent().removeClass('onestepcheckout-method-color-true');
            jQuery("#onestepcheckout-shipping-container input:radio:checked").removeAttr('checked');
        } else {
            jQuery('#billing-new-address-form').hide();
            jQuery("#billing-new-address-form-register input:radio").parent().removeClass('onestepcheckout-method-color-true');
            jQuery(this).parent().addClass('onestepcheckout-method-color-true');
        }
        reloadShippingMethod();
    }

    function loginAction()
    {
        if(goajax) return;
        goajax = 1;
        jQuery(".onestepcheckout-login-content input[name='login[password]']").addClass('ui-autocomplete-loading');
        jQuery.getJSON(_getData('loginaddressurl') ,
                {
                    "login[username]": jQuery(".onestepcheckout-login-content input[name='login[username]']").val(),
                    "login[password]": jQuery(".onestepcheckout-login-content input[name='login[password]']").val()
                },
                function(data) {
                    if(data.error == 1) {
                        showTopError(data.message);
                    } else {
                        showTopError('');
                        location.reload();
                    }
                }
                ).complete(function() {
                    goajax = 0;
                    jQuery(".onestepcheckout-login-content input[name='login[password]']").removeClass('ui-autocomplete-loading');
                });
    }

    function showTopError(et_html)
    {
        if(et_html.length)
        {
            jQuery('#onestepcheckout-error-top').html('');
            jQuery('#onestepcheckout-error-top').show();
            jQuery('#onestepcheckout-error-top').append('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+et_html+'</p></div>');
            jQuery('#onestepcheckout-error-top .alert').alert();
            return false;
        } else {
            jQuery('#onestepcheckout-error-top .alert').alert('close');
            jQuery('#onestepcheckout-error-top').html('').hide();
        }
        return true;
    }

    function showError(e_obj)
    {
        if(e_obj.length) {
                jQuery('#onestepcheckout-error').html('');
                jQuery(e_obj).each(function(indx, element){
                    jQuery('#onestepcheckout-error').show();
                    jQuery('#onestepcheckout-error').append('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+element+'</p></div>');
                });
                jQuery('#onestepcheckout-error .alert').alert();
                return false;
            } else {
                jQuery('#onestepcheckout-error .alert').alert('close');
                jQuery('#onestepcheckout-error').html('').hide();
                jQuery('#co-billing-form .onestepcheckout-error-field').removeClass('onestepcheckout-error-field');
            }
        return true;
    }

    function showTopInfo(ei_html, ei_class)
    {
        if(ei_html.length) {
            jQuery('#onestepcheckout-error-top').html('');
            jQuery('#onestepcheckout-error-top').show();
            jQuery('#onestepcheckout-error-top').append('<div class="alert alert-block '+ ei_class +' fade in"><a class="close" data-dismiss="alert" href="#">&times;</a><p>'+ei_html+'</p></div>');
            jQuery('#onestepcheckout-error-top .alert').alert();
            return false;
        } else {
            jQuery('#onestepcheckout-error-top .alert').alert('close');
            jQuery('#onestepcheckout-error-top').html('').hide();
        }
        return true;
    }

    function couponAction(coupon_a)
    {
        if(!jQuery("#onestepcheckout-sert").val()) {
            jQuery("#onestepcheckout-sert").addClass("onestepcheckout-error-field");
            return ;
        }

        showLoaderTotal();
        if(goajax) return;
        goajax = 1;
        jQuery.getJSON(_getData('couponurl'),
                {
                    "coupon_code": jQuery("#onestepcheckout-sert").val(),
                    "remove": coupon_a
                },

                function(data) {
                    if(data.error == 1) {
                        showTopError(data.message);
                        jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-success-field");
                        jQuery("#onestepcheckout-sert").addClass("onestepcheckout-error-field");
                    } else {
                        showTopError('');
                        jQuery('.onestepcheckout-totals-container').html(data.totals);

                        if(coupon_a == 0) {
                                if(data.success == 1) {
                                    jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-error-field");
                                    jQuery("#onestepcheckout-sert").addClass("onestepcheckout-success-field");
                                    jQuery("#onestepcheckout-cancel-coupon").show();
                                    showTopInfo(data.message, 'alert-success');
                                } else {
                                    jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-success-field");
                                }
                            }

                        if(coupon_a == 1) {
                                if(data.cancel == 1) {
                                jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-error-field");
                                jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-success-field");
                                jQuery("#fast-cancel-coupon").hide();
                                jQuery("#onestepcheckout-sert").val('');
                                showTopInfo(data.message, 'alert-info');
                                jQuery("#onestepcheckout-cancel-coupon").hide();
                                } else {
                                    jQuery("#onestepcheckout-sert").removeClass("onestepcheckout-success-field");
                                }
                            }
                        }
                }
                ).complete(function() {
                    hideLoaderTotal();
                    goajax = 0;
                });
    }


    function paymentShowHelper()
    {
        jQuery("#checkout-payment-method-load div.onestepcheckout-method-color-true").removeClass("onestepcheckout-method-color-true");
        jQuery("#checkout-payment-method-load .form-list").hide();
        jQuery("#payment_form_"+jQuery("#checkout-payment-method-load input:radio:checked").val()).show();
        jQuery("#"+jQuery("#checkout-payment-method-load input:radio:checked").val()+"-method").addClass("onestepcheckout-method-color-true");
    }

    function shippingShowHelper()
    {
        jQuery('.onestepcheckout-shipping-container .onestepcheckout-method-color-true').removeClass('onestepcheckout-method-color-true');
        jQuery('.onestepcheckout-shipping-container input:radio:checked').parent().addClass('onestepcheckout-method-color-true');
    }

    function agregateFormData(id)
    {
        var filterData = new Object(); //объект с параметрами запроса

        jQuery(id + ' input.input-text:visible').each(function(){
            filterData[jQuery(this).attr('name')] = jQuery(this).val();
        });
        jQuery(id + ' select').each(function(){
            filterData[jQuery(this).attr('name')] = jQuery(this).val();
        });
        jQuery(id + ' textarea:visible').each(function(){
            filterData[jQuery(this).attr('name')] = jQuery(this).val();
        });
        jQuery(id + ' input.checkbox:visible:checked').each(function(){
            filterData[jQuery(this).attr('name')] = '1';
        });
        jQuery(id + " input[type='hidden']").each(function(){
            filterData[jQuery(this).attr('name')] = jQuery(this).val();
        });
        jQuery(id + " input.radio:checked").each(function(){
            filterData[jQuery(this).attr('name')] = jQuery(this).val();
        });
        return filterData;
    }

    function prepareRegion()
    {
        jQuery('li.address-no-pickup').hide();
        if(jQuery("#billing-new-address-form select[name='billing[region_id]']").val() == '') {
                jQuery("#pickup-field").hide();
            } else {
                jQuery('li.' + getCurrentSelectedTab()).show();
                initTab(getCurrentSelectedTab());
                jQuery('li.address-no-pickup .input-text').val('');
                isHideCity();
                reloadShippingMethod();
            }
    }

    function initTab(tab) {
        $data = _getData('tab_data');
        init_obj = $data[tab];
        jQuery('li .for-required').removeClass('required-entry required');
        jQuery.each(init_obj, function(indx, element){
              if(element == '2') {
                      jQuery('li.ons-' + indx + ' .for-required').addClass('required-entry required');
                  }
        });
        if(init_obj['pickup'] == '1') {
                jQuery("#pickup-field").show();
            } else {
                jQuery("#pickup-field").hide();
            }
        updateMetro(init_obj['metro_data']);
        checkPickup();
    }

    function updateMetro($metro)
    {
        if(typeof $metro != "undefined") {
            jQuery("select[name='billing[metro]']").empty();
            if($metro.length > 1 && _getData($metro).length) {
                jQuery("select[name='billing[metro]']").append(_getData($metro));
            }
        }
    }

    function checkPickup()
    {
        if(jQuery("#customer-pickup").is(':checked') && jQuery("#customer-pickup").is(':visible')) {
            jQuery('li.address-no-pickup').hide();
        } else {
            jQuery('li.address-no-pickup.'+ getCurrentSelectedTab()).show();
        }
    }

    function initValidateEvents() {
        //validate required field
        jQuery("#billing-new-address-form .required-entry:visible").on("focusout", function(){
                if(jQuery(this).val() == "" ) {
                    jQuery(this).addClass("onestepcheckout-error-field");
                } else {
                    jQuery(this).removeClass("onestepcheckout-error-field");
                }
        });
        //validate email
        jQuery("#billing-new-address-form .validate-email:visible").on("focusout", function(){
                if(isEmail(jQuery(this).val())) {
                    jQuery(this).removeClass("onestepcheckout-error-field");
                } else {
                    jQuery(this).addClass("onestepcheckout-error-field");
                }
        });
        //validate postcode
        jQuery("#billing-new-address-form .validate-zip-international.required-entry:visible").on("focusout", function(){
            if(jQuery(this).val().length > 5  && jQuery(this).val().length < 7 && jQuery(this).val() > 0) {
                    jQuery(this).removeClass("onestepcheckout-error-field");
                } else {
                    jQuery(this).addClass("onestepcheckout-error-field");
                }
        });
        //validate select focusout
        jQuery("#billing-new-address-form select.for-required.required-entry.validate-select:visible").on("focusout", function(){
            if(jQuery(this).val() == "") {
                jQuery(this).addClass("onestepcheckout-error-field");
            } else {
                jQuery(this).removeClass("onestepcheckout-error-field");
            }
        });
        //validate select change keyup
        jQuery("#billing-new-address-form select.for-required.required-entry.validate-select:visible").on("change keyup", function(){
            if(jQuery(this).val() == "") {
                jQuery(this).addClass("onestepcheckout-error-field");
            } else {
                jQuery(this).removeClass("onestepcheckout-error-field");
            }
        });

        jQuery("#billing-new-address-form select[name='billing[region_id]']").on("change", function(){
            if(jQuery(this).val() == "") {
                jQuery('#billing_region_id_chzn').addClass("onestepcheckout-error-field");
            } else {
                jQuery('#billing_region_id_chzn').removeClass("onestepcheckout-error-field");
            }
        });
    }

    function validateBeforeSave()
    {
        e_obj = new Array();
        onsWarningMsg = _getData('onsWarningMsg');
        //check address
        jQuery('#co-billing-form .required-entry:visible').each(function(){
            if(jQuery(this).val() == "" && !jQuery(this).hasClass('validate-email')) {
                    jQuery(this).addClass("onestepcheckout-error-field");
                    if(jQuery(this).attr('data-error')) {
                            e_obj.push(jQuery(this).attr('data-error'));
                        }
                }
            if(jQuery(this).hasClass('validate-email') && !isEmail(jQuery(this).val())) {
                    jQuery(this).addClass("onestepcheckout-error-field");
                    if(jQuery(this).attr('data-error')) {
                        e_obj.push(jQuery(this).attr('data-error'));
                    }
                }
        });

        if(jQuery('#billing_region_id_chzn').length && jQuery('#billing_region_id_chzn').is(':visible') &&
                jQuery("#billing-new-address-form select[name='billing[region_id]']").hasClass('required-entry') && !jQuery("#billing-new-address-form select[name='billing[region_id]']").val().length)
            {
                e_obj.push(jQuery("#billing-new-address-form select[name='billing[region_id]']").attr('data-error'));
                jQuery('#billing_region_id_chzn').addClass("onestepcheckout-error-field");
            }

        if(jQuery("#billing-new-address-form-register").length ) {
            if(!jQuery("#billing-new-address-form-register input:radio:checked").length) {
                e_obj.push(onsWarningMsg['check_address']);
            }
        }

        if(jQuery("#onestepcheckout-shipping-methods").length && !jQuery("#onestepcheckout-shipping-methods input:radio:checked").length) {
            e_obj.push(onsWarningMsg['check_shipping_method']);
        }

        jQuery('#checkout-agreements input:checkbox').each(function(){
            if(!jQuery(this).is(':checked')) {
                    e_obj.push(onsWarningMsg['check_agree']);
                    jQuery('#checkout-agreements .agree').addClass("onestepcheckout-error-field");
                }
        });

        if(!jQuery("#onestepcheckout-payment-methods input:radio:checked").length) {
            e_obj.push(onsWarningMsg['check_payment_method']);
        }

        /**************************pickpoint*******************************/
        if(typeof validatePostamat == 'function') {
            if(!validatePostamat()) {
                e_obj.push('Пожалуйста, выберите Постамат!');
            }
        }
        return showError(e_obj);
    }

    function createAutocomplite(id) {

        if(_getData('autoComplete') == 0) return ;

        jQuery('#'+id).autocomplete({
            source: _getData('autoCompleteUrl'),
            minLength: 4,
            select: function( event, ui ) {
                    setPostcode( ui.item ?
                            ui.item.index :
                            this.value
                            );
                    setCity( ui.item ?
                        ui.item.city :
                        this.value
                        );
                    setRegion( ui.item ?
                            ui.item.region :
                            this.value, ui.item.ems_region
                            );
                    focusInId("textarea[name='billing[street][]']");
                    reloadShippingMethod();
                    return false;
                },
                change: function( event, ui ) {
                    setPostcode( ui.item ?
                            ui.item.index :
                            this.value
                            );
                    setCity( ui.item ?
                        ui.item.city :
                        null
                        );
                    setRegion( ui.item ?
                            ui.item.region :
                            null, ui.item.ems_region
                            );
                    reloadShippingMethod();
                    return false;
                }
        });
    }

    function setCity(message) {
        if(_getData('city_autocomlete') == '1') {
            jQuery( "input[name='billing[city]']").val(message).removeClass("onestepcheckout-error-field");
        }
    }

    function setRegion(message, __ems_region) {
        if(_getData('region_autocomlete') == '1') {
            if(jQuery("input[name='billing[region]']").is(':visible')) {
                jQuery("input[name='billing[region]']").val(message);
            } else {
                jQuery("#billing-new-address-form select[name='billing[region_id]']").find("option[ems_location_code='" + __ems_region + "']").attr("selected", "selected").trigger("liszt:updated");
            }
        }
    }

    function setPostcode(message) {
        jQuery( "input[name='billing[postcode]']").val(message);
    }

    function focusInId(id) {
        jQuery(id).focus();
    }

    function getCurrentSelectedTab()
    {
        tab = jQuery("#billing-region_id  option:selected").attr('data-region-tab');
        if(!!!tab) {
            return _getData('init_tab');
        }
        return tab;
    }

    function showLoaderShipping()
    {
        jQuery('#onestepcheckout-shipping-methods .sp-methods').mask(_getData('loadingmsg'));
    }

    function hideLoaderShipping()
    {
        jQuery('#onestepcheckout-shipping-methods .sp-methods').unmask();
    }

    function showLoaderPayment()
    {
        jQuery('#checkout-payment-method-load').mask(_getData('loadingmsg'));
    }

    function hideLoaderPayment()
    {
        jQuery('#checkout-payment-method-load').unmask();
    }

    function showLoaderTotal()
    {
        jQuery('.onestepcheckout-totals-container').mask(_getData('loadingmsg'));
    }

    function hideLoaderTotal()
    {
        jQuery('.onestepcheckout-totals-container').unmask();
    }

    function disableRadioSelect()
    {
        jQuery('.onestepcheckout select').attr('disabled', 'disabled');
        jQuery('.onestepcheckout .checkbox').attr('disabled', 'disabled');
        jQuery('.onestepcheckout .radio').attr('disabled', 'disabled');
        jQuery('.chzn-select').trigger('liszt:updated');
    }

    function enableRadioSelect()
    {
        jQuery('.onestepcheckout select').removeAttr('disabled');
        jQuery('.onestepcheckout .checkbox').removeAttr('disabled');
        jQuery('.onestepcheckout .radio').removeAttr('disabled');
        jQuery('.chzn-select').trigger('liszt:updated');
    }

    function isHideCity()
    {
        if(jQuery("#billing-region_id  option:selected").attr('data-city') == '1') {
            jQuery('#billing-city').val(jQuery("#billing-new-address-form select[name='billing[region_id]'] :selected").text());
            jQuery('.ons-city').hide();
        }
    }

    function implode( glue, pieces ) {	// Join array elements with a string
        //
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: _argos

        return ((pieces instanceof Array )?pieces.join(glue):pieces);
    }

    function isEmail(text)
    {
        reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if (text.match(reg)) {
                return true;
            }
        return false;
    }

    function _getData(param)
    {
        $this = $(document).data('onestepcheckout_options');
        return $this[param];
    }

    $.fn.onestepcheckout = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' не существует');
        }
    };
})(jQuery);