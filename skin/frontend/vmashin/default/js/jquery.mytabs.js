(function($) {
    $.fn.mytabs = function() {
        this.prepend('<div class="mytabs"></div>');
        var dv = this.find('.mytabs');
        var i = 0;
        this.find(".mytab").each(function(){
            if ( i == 0 ) {
                $( this ).addClass( 'active' );
            }
            dv.append($(this).clone());
            $(this).remove();
            i++;
        });
        var i = 0;
        this.find(".mypane").each(function(){
            if ( i == 0 ) {
                $( this ).show();
            } else {
                $( this).hide();
            }
            i++;
        });
        this.find(".mytab").bind(
            'click',
            function ( e ) {
                var id = $( this ).attr( 'data-href' );
                $( '.mypane').hide();
                $( '.mytab').removeClass( 'active' );
                $( e.target).addClass( 'active' );
                $( '#'+id).show();
                e.preventDefault();
            }
        );

    };
})(jQuery);
