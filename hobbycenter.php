<?php 
error_reporting(E_ALL);
require_once("app/Mage.php");
Mage::app();
$attr_code = 'provider';
$attr_value = '305';
$storeId = Mage::app()->getStore()->getId();
$_collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter($attr_code, $attr_value);

//настройки доступа к апи hobbycenter.ru

$login = 'FGTGGAPEDI'; // логин указанный в настройках API

$passAPI = 'UTzGvNBVpl'; // пароль указанный в настройках API

/**
 *   @ $code - сумма запрашиваемой информации
 *   пример: code=24 – ответ: галерея картинок+инфо по брендам, где:
 *
 *   1 — название КТ (полное и сокр.)
 *   2 — описание КТ (полное и сокр., мета-данные)
 *   4 — ссылки к описанию (видео, pdf инструкции)
 *   8 — галерея картинок (картинка шапка и галерея)
 *   16 — бренд (id и имя)
 *   32 — категории (id'шники и имена)
 *   64 — количество (кол. на складе, кол. в коробке, вес)
 *   128 — цены для каждого из видов дилерства
 *
 */
$code =  128 + 64;



$n = 1;
foreach($_collection as $product){
    $prod_id = $product->getId(); 
    $_product = Mage::getModel('catalog/product')->load($prod_id);

    /**
     * артикул опрашиваемого товара (получение списков смотрите выше)
     */
    $article = $_product->getSku();

    /**
     * ключ сверки доступа к API
     * пример: $key = md5($login.$passAPI.$article.$code), где:
     *
     * $login - логин указанный в запросе
     * $passAPI – пароль указанный в настройках API
     * $article – артикул указанный в запросе
     * $code — код указанный в запросе
     * key — ключ сверки доступа к API, подробнее:
     */
    $key = md5($login.$passAPI.$article.$code);

    /**
     * url по которому стучимся к hobbycenter.ru
     * 
     * тестовый урл:
     * $url = 'http://www.hobbycenter.ru/API/list.php?login=FGTGGAPEDI&type=xml&code=brands&key=eeade30a61b7aa2c67dcb09941d5192a';
     */
    $url = "http://www.hobbycenter.ru/API/product.php?login=$login&article=$article&code=$code&key=$key";

    //стучимся к апи
    if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '');
        curl_setopt($curl, CURLOPT_, true);
        $out = curl_exec($curl);
        curl_close($curl);

        try{

            if( ! $out){
                throw new Exception('Неверные данные');
            }

            $first = strrpos($out, 'Array');
            $last = strripos($out, ')');
            $array = substr($out, $first, $last-$first+1);
            $data = print_r_reverse($array);
            if($data['error']){
                throw new Exception($data['error']);
            }

            $price = $data['price_retail'];
            $in_stock = ($data['qty_status'] === 'нет') ? false : true;

            $_product->setData('price', $price);
            $_product->setData('name', $_product->getName());
            if( ! $in_stock){
                $_product->setStockData(
                   array( 
                          'is_in_stock' => 0, 
                          'qty' => 0,
                          'manage_stock' => 1,
                          'use_config_notify_stock_qty' => 1
                   )
                 );
            }
            if($_product->save()){
                echo "Цена для товара $article обновлена до $price, количество - {$data['qty_status']}<br>";
            }

        }catch(Exception $e){
            echo $e->getMessage() . '<br>';
        }
    }
}

function print_r_reverse($in) {
    $lines = explode("\n", trim($in));
    if (trim($lines[0]) != 'Array') {
        // bottomed out to something that isn't an array
        return $in;
    } else {
        // this is an array, lets parse it
        if (preg_match("/(\s{5,})\(/", $lines[1], $match)) {
            // this is a tested array/recursive call to this function
            // take a set of spaces off the beginning
            $spaces = $match[1];
            $spaces_length = strlen($spaces);
            $lines_total = count($lines);
            for ($i = 0; $i < $lines_total; $i++) {
                if (substr($lines[$i], 0, $spaces_length) == $spaces) {
                    $lines[$i] = substr($lines[$i], $spaces_length); 
                }
            }
        }
        array_shift($lines); // Array 
        array_shift($lines); // ( 
        array_pop($lines); // ) 
        $in = implode("\n", $lines); 
        // make sure we only match stuff with 4 preceding spaces (stuff for this array and not a nested one) 
        preg_match_all("/^\s{4}\[(.+?)\] \=\> /m", $in, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER); 
        $pos = array(); 
        $previous_key = ''; 
        $in_length = strlen($in); 
        // store the following in $pos: 
        // array with key = key of the parsed array's item 
        // value = array(start position in $in, $end position in $in) 
        foreach ($matches as $match) { 
            $key = $match[1][0]; 
            $start = $match[0][1] + strlen($match[0][0]); 
            $pos[$key] = array($start, $in_length); 
            if ($previous_key != '') $pos[$previous_key][1] = $match[0][1] - 1; 
            $previous_key = $key; 
        } 
        $ret = array(); 
        foreach ($pos as $key => $where) { 
            // recursively see if the parsed out value is an array too 
            $ret[$key] = print_r_reverse(substr($in, $where[0], $where[1] - $where[0])); 
        } 
        return $ret; 
    } 
}
?>