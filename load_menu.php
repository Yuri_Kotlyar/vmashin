<?php
//error_reporting(E_ALL);
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    ob_start();
    include_once ('app/Mage.php');
    Mage::run();
    require_once 'app/code/local/Mage/Catalog/Block/Navigation.php';
    ob_clean();
    $parent_id = $_POST['parent_id'];
    $expand = $_POST['expand_class'].'-';
    $level = $_POST['level'];
    /*GET CHILDREN*/
    $category = Mage::getModel('catalog/category')->load($parent_id);
    $children = $category->getChildrenCategories();
    $activeCategories = array();
    foreach($children as $child){
        if($child['is_active']){
            $activeCategories[]=$child;
        }
    }
    /*GET HTML*/
    $parent_level = $level-1;
    $html = "<ul class = \"level{$parent_level}\">";
    $q = 0;
    $count = count($activeCategories);
    $childLevel = ++$level;
    $childLevel = "level{$ChildLevel}";
    foreach($activeCategories as $child){
        $q++;
        $expandClass = $expand . $q;
        $class = "$childLevel $expandClass";
        if ($q == 1){
            $class .= " first";
        }
        if ($q == $count){
            $class .= " last";
        }
        $category_id = $child['entity_id'];
        if($child->hasChildren()){
            $class .= " parent";
        }else{
            $category_id = 0;
        }
        $href = 'http://vmashin.ru/'.$child['request_path'];
        $name = $child['name'];
        $html .= "<li class=\"$class\" onclick=\"javascript:expand('$expandClass', $category_id, this)\">";
        $html .= "<a href=\"$href\"><span>$name</span></a>";
        $html .= '</li>';
    }
    $html .= '</ul>';
    echo $html;
}
?>