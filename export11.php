<?php
 
// Load the Magento core
 
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
 
// Load the product collection
 
$collection = Mage::getModel('catalog/product')
 ->getCollection()
 ->addAttributeToSelect('*') //Select everything from the table
 ->addUrlRewrite(); //Generate nice URLs
/*
 For this example I am generating a CSV file,
 but you can change this to suit your needs.
*/
 
echo "NAME, SKU,ID,URL\n";
 
foreach($collection as $product) {
 //Load the product categories
 $categories = $product->getCategoryIds();
 //Select the last category in the list
 $categoryId = end($categories);
 //Load that category
 $category = Mage::getModel('catalog/category')->load($categoryId);
  
 echo '"'.$product->getName().'","'.
  $product->getSku().'",'.
  $product->getprice().',"'.
  //This will the proper URL, the base url is optional, though make sure you remove the trailing export.php (or whatever you name this file)
  str_replace('export.php/','',Mage::getBaseUrl()).$product->getUrlPath($category).'"'.
  "\n";
}
 
 
?>